package com.trinitcore.mearstaidear.modules.notes

import com.trinitcore.sqlv2.queryUtils.module.*

open class NoteTable : DataModule() {

    var metaColumns: DataModules<NoteTable.MetaColumn> by Attribute()
    var rows: DataModules<NoteTable.Row> by Attribute()
    var title: String by Attribute()
    var caption: String by Attribute()

    open class Proposal : DataModule() {

        var metaColumns: List<NoteTable.MetaColumn.Proposal>? by NullableAttribute()
        var rows: List<NoteTable.Row.Proposal>? by NullableAttribute()
        var title: String? by NullableAttribute()
        var caption: String? by NullableAttribute()

        open class JSON : JSONModule() {

            var metaColumns: List<NoteTable.MetaColumn.Proposal.JSON>? by NullableAttribute()
            var rows: List<NoteTable.Row.Proposal.JSON>? by NullableAttribute()
            var title: String? by NullableAttribute()
            var caption: String? by NullableAttribute()

        }

    }

    open class MetaColumn : DataModule() {

        var title: String by Attribute()
        var order: Int by Attribute()

        open class Proposal : DataModule() {
            open class JSON : JSONModule() {

                var deletion: Boolean? by NullableAttribute()
                var modification: Boolean? by NullableAttribute()
                var addition: Boolean? by NullableAttribute()

                var title: String? by NullableAttribute()
                var order: Int? by NullableAttribute()
            }
        }

        class DeletionProposal : Proposal() {
            var elementID: Int by Attribute()
        }

        open class ConstructiveOperationProposal : Proposal() {

            var title: String? by NullableAttribute()
            var order: Int? by NullableAttribute()

        }

        class ModificationProposal : ConstructiveOperationProposal() {

            var elementID: Int by Attribute()

        }

        class AdditionProposal : ConstructiveOperationProposal() {

        }

    }

    open class Column : DataModule() {

        var paragraphTextID: Int? by NullableAttribute()
        var imageID: Int? by NullableAttribute()

        var paragraphText: NoteParagraphText? by NullableAttribute()
        var image: NoteImage? by NullableAttribute()

        var order: Int by Attribute()

        open class Proposal : DataModule() {
            open class JSON : JSONModule() {
                var deletion: Boolean? by NullableAttribute()
                var modification: Boolean? by NullableAttribute()
                var addition: Boolean? by NullableAttribute()

                var elementID: Int? by NullableAttribute()

                var paragraphText: NoteParagraphText.JSON? by NullableAttribute()
                var image: NoteImage.JSON? by NullableAttribute()
            }
        }

        class DeletionProposal : Proposal() {
            var elementID: Int by Attribute()

        }

        open class ConstructiveOperationProposal : Proposal() {

            var paragraphTextID: Int? by NullableAttribute()
            var imageID: Int? by NullableAttribute()

            var paragraphText: NoteParagraphText? by NullableAttribute()
            var image: NoteImage? by NullableAttribute()

            var order: Int by Attribute()

        }

        class ModificationProposal : ConstructiveOperationProposal() {

            var elementID: Int by Attribute()

        }

        class AdditionProposal : ConstructiveOperationProposal() {

        }

        class JSON : JSONModule() {

            var ID: Int? by NullableAttribute()

            var paragraphTextID: Int? by NullableAttribute()
            var imageID: Int? by NullableAttribute()

            var paragraphText: NoteParagraphText.JSON? by NullableAttribute()
            var image: NoteImage.JSON? by NullableAttribute()

        }

    }

    open class Row : DataModule() {

        var order: Int by Attribute()

        var columns: DataModules<Column> by Attribute()

        open class Proposal : DataModule() {
            open class JSON : JSONModule() {
                var elementID: Int? by NullableAttribute()

                var order: Int? by NullableAttribute()

                var columns: List<Column.Proposal.JSON>? by NullableAttribute()
            }
        }

        open class ConstructiveOperationProposal : Proposal() {

            var order: Int? by NullableAttribute()

            var columns: DataModules<Column.Proposal> by Attribute()

        }

        class ModificationProposal : ConstructiveOperationProposal() {

            var elementID: Int by Attribute()

        }

        class AdditionProposal : ConstructiveOperationProposal() {

        }

        class DeletionProposal : NoteTable.Row.Proposal()

    }

}