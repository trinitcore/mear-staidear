package com.trinitcore.mearstaidear.modules.notes

import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.module.JSONModule
import com.trinitcore.sqlv2.queryUtils.module.NullableAttribute

open class NoteParagraphText : DataModule() {

    var title: String? by NullableAttribute()
    var data: String by Attribute()

    class JSON : JSONModule() {

        var title: String? by NullableAttribute()
        var data: String? by NullableAttribute()

    }

}