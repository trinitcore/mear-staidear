package com.trinitcore.mearstaidear.modules.notes

import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.module.JSONModule
import com.trinitcore.sqlv2.queryUtils.module.NullableAttribute

class NoteImage : DataModule() {
    var URL: String by Attribute()
    var subtitle: String? by NullableAttribute()

    class JSON : JSONModule() {

        var URL: String? by NullableAttribute()
        var subtitle: String? by NullableAttribute()

    }

}