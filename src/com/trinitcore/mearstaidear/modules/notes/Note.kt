package com.trinitcore.mearstaidear.modules.notes

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.mearstaidear.users.student.Student
import com.trinitcore.sqlv2.commonUtils.MultiAssociatingQMap
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.queryObjects.ModuleTable
import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.module.DataModules

class Note : DataModule() {
    var topicID: Int by Attribute()
    var elements: DataModules<NoteElement> by Attribute()
    var currentVersionNumber: Int by Attribute()
    var currentProposalNumber: Int by Attribute()

    @Web
    fun default() : StatusResult {
        return OKStatus(mapOf("note" to this.toJSONObject()))
    }

    @Web
    fun submitProposal(
            user: Student,
            deletedElements: List<NoteElement.Proposal.JSON>,
            newElements: List<NoteElement.Proposal.JSON>,
            modifiedElements: List<NoteElement.Proposal.JSON>
    ) : StatusResult {
        val noteselementsproposals: ModuleTable<NoteElement.Proposal> = (user.userType.findTable("noteselementsproposals")
                .permanentTransactionParameters(QMap("noteID", getID()), QMap("versionNumber", currentVersionNumber + 1), QMap("proposalNumber", currentProposalNumber)) as ModuleTable<NoteElement.Proposal>)

        noteselementsproposals.multiInsert(*MultiAssociatingQMap("", modifiedElements).value as Array<out Array<QMap>>)
        noteselementsproposals.multiInsert(*MultiAssociatingQMap("", newElements).value as Array<out Array<QMap>>)
        noteselementsproposals.multiInsert(*MultiAssociatingQMap("", deletedElements).value as Array<out Array<QMap>>)
        currentProposalNumber++

        return OKFormStatus("Cuireadh na hathruithe faoi bhráid an eagarthóra. Cuirfear i bhfeidhm na hathruithe nuair a fhaobhfaidh ár meitheal iad.")
    }

}