package com.trinitcore.mearstaidear.modules.notes

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.module.JSONModule
import com.trinitcore.sqlv2.queryUtils.module.NullableAttribute

open class NoteElement : DataModule() {

    var order: Int by Attribute()
    var width: Int by Attribute()

    var paragraphTextID: Int? by NullableAttribute()
    var imageID: Int? by NullableAttribute()
    var tableID: Int? by NullableAttribute()

    var paragraphText: NoteParagraphText? by NullableAttribute()
    var image: NoteImage? by NullableAttribute()
    var table: NoteTable? by NullableAttribute()

    var firstVersionNumber: Int by Attribute()
    var lastVersionNumber: Int by Attribute()


    /* Le cur isteacch
        topic
        dateModified
     */

    open class Proposal : DataModule() {

        class JSON : JSONModule() {
            var elementID: Int? by Attribute()
            var modification: Boolean? by Attribute()
            var addition: Boolean? by Attribute()
            var deletion: Boolean? by Attribute()

            var order: Int? by NullableAttribute()
            var width: Int? by NullableAttribute()

            var paragraphText: NoteParagraphText.JSON? by NullableAttribute()
            var image: NoteImage.JSON? by NullableAttribute()
            var table: NoteTable.Proposal.JSON? by NullableAttribute()
        }

        var versionNumber: Int by Attribute()
        var proposalNumber: Int by Attribute()

    }


    open class ConstructiveOperationProposal : Proposal() {

        var order: Int? by NullableAttribute()
        var width: Int? by NullableAttribute()

        var paragraphText: NoteParagraphText? by NullableAttribute()
        var image: NoteImage? by NullableAttribute()
        var table: NoteTable.Proposal? by NullableAttribute()

    }

    class AdditionProposal : ConstructiveOperationProposal();

    class ModificationProposal : ConstructiveOperationProposal() {

        var elementID: Int by Attribute()

    }

    class DeletionProposal: Proposal() {

        var elementID: Int by Attribute()

    }

    @Web
    fun default() : StatusResult {
        return OKStatus(mapOf("note" to this.toJSONObject()))
    }

}