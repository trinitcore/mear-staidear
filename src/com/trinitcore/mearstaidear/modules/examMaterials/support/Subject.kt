package com.trinitcore.mearstaidear.modules.examMaterials.support

import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule

class Subject : DataModule() {

    var titleGA: String by Attribute()
    var titleEN: String by Attribute()

}