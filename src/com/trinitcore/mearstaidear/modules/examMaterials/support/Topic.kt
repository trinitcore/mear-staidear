package com.trinitcore.mearstaidear.modules.examMaterials.support

import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule

class Topic : DataModule() {
    var subjectID: Int by Attribute()

    var titleGA: String by Attribute()
    var titleEN: String by Attribute()
}