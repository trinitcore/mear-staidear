package com.trinitcore.mearstaidear.modules.examMaterials.exam

import com.trinitcore.mearstaidear.modules.examMaterials.support.Subject
import com.trinitcore.mearstaidear.modules.examMaterials.support.Topic
import com.trinitcore.sqlv2.queryUtils.module.*

class ExamSubQuestion : DataModule() {

    var solutionID: Int? by NullableAttribute()
    var orderNumber: Int by Attribute()
    var questionID: Int by Attribute()

    var startPosition: Double by Attribute()
    var endPosition: Double by Attribute()

    var markingSchemes: DataModules<ExamMarkingScheme> by Attribute()

    var ignored: Boolean by Attribute()
    var floatSide: Boolean by Attribute()

    class JSON : JSONModule() {
        var ID: Int? by NullableAttribute()

        var solutionID: Int? by NullableAttribute()
        var orderNumber: Int by Attribute()
        var questionID: Int by Attribute()

        var startPosition: Double by Attribute()
        var endPosition: Double by Attribute()

        var markingScheme: DataModules<ExamMarkingScheme>? by NullableAttribute()

        var ignored: Boolean by Attribute()
        var floatSide: Boolean by Attribute()
    }

}