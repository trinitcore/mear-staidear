package com.trinitcore.mearstaidear.modules.examMaterials.exam

import com.trinitcore.asd.Configuration
import com.trinitcore.asd.annotations.SubWebServlet
import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.requestTools.UploadedFile
import com.trinitcore.asd.resourceTools.fileStorage.InternalStorageConfiguration
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.ErrorStatus
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.mearstaidear.rest.admin.ExamResourcesREST
import com.trinitcore.sqlv2.commonUtils.AssociatingQMap
import com.trinitcore.sqlv2.commonUtils.MultiAssociatingQMap
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.module.DataModules
import com.trinitcore.sqlv2.queryUtils.module.JSONModule
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.rendering.ImageType
import org.apache.pdfbox.rendering.PDFRenderer
import org.apache.pdfbox.tools.imageio.ImageIOUtil
import java.awt.image.BufferedImage
import java.io.File
import javax.servlet.annotation.MultipartConfig

class ExamPaper : DataModule() {

    var year: Int by Attribute()
    var subjectID: Int by Attribute()
    var lang: Int by Attribute()
    var level: Int by Attribute()
    var remoteExamPaperReference: String by Attribute()
    var remoteMarkingSchemeReference: String by Attribute()

    var remoteExamPaperWidth: Int by Attribute()
    var remoteMarkingSchemeWidth: Int by Attribute()

    @SubWebServlet var questions: DataModules<ExamQuestion> by Attribute()

    @Web
    fun changeScaleOfMarkingSchemeContainers(ratio: Double) : StatusResult {
        for (question in questions.moduleValues()) {
            for (subQuestion in question.subQuestions.moduleValues()) {
                for (markingScheme in subQuestion.markingSchemes.moduleValues()) {
                    markingScheme.startPosition = (markingScheme.startPosition / ratio)
                    markingScheme.endPosition = (markingScheme.endPosition / ratio)
                }
            }
        }
        return OKFormStatus("Coibhneas curtha i bhfeidhm.")
    }

    @Web
    fun adjustMarkingSchemeContainers(offset: Int) : StatusResult {
        for (question in questions.moduleValues()) {
            for (subQuestion in question.subQuestions.moduleValues()) {
                for (markingScheme in subQuestion.markingSchemes.moduleValues()) {
                    markingScheme.startPosition = markingScheme.startPosition + (offset)
                    markingScheme.endPosition = markingScheme.endPosition  + (offset)
                }
            }
        }
        return OKFormStatus("Seach-curtha.")
    }

    @Web
    fun applyNewMarkingScheme(pdf: UploadedFile,
                              lowerPageToProcess: Int, upperPageToProcess: Int,
                              changeInPages: Int
    ) : StatusResult {
        val markingSchemeFileSave = pdf.saveFileWithAlternativeConfiguration(InternalStorageConfiguration("/app/userdata")) as UploadedFile.Local

        try {
            val remoteMarkingScheme = ExamResourcesREST.convert("MS",markingSchemeFileSave, lowerPageToProcess, upperPageToProcess, year, subjectID, lang, level)
            val heightPerPage = remoteMarkingScheme["heightPerPage"] as Int

            val newRemoteMarkingSchemeReference = remoteMarkingScheme["reference"] as String
            val newRemoteMarkingSchemeWidth = remoteMarkingScheme["width"] as Int

            val markingSchemeRatio = remoteMarkingSchemeWidth / newRemoteMarkingSchemeWidth.toDouble()

            for (question in questions.moduleValues()) {
                for (subQuestion in question.subQuestions.moduleValues()) {
                    for (markingScheme in subQuestion.markingSchemes.moduleValues()) {
                        markingScheme.startPosition = (markingScheme.startPosition / markingSchemeRatio) + (heightPerPage*changeInPages)
                        markingScheme.endPosition = (markingScheme.endPosition / markingSchemeRatio) + (heightPerPage*changeInPages)
                    }
                }
            }

            update(
                    QMap("remoteMarkingSchemeReference", newRemoteMarkingSchemeReference),
                    QMap("remoteMarkingSchemeWidth", newRemoteMarkingSchemeWidth)
            )
        } catch (e: Exception) {
            e.printStackTrace()
            ErrorStatus("Níor éirigh leis an athchóiriú. Tá an fhadhb á deisiú againn faoi láthair.")
        }

        return OKFormStatus("D'éirigh leis an athchóiriú")
    }

    @Web
    fun applyNewExamPaper(pdf: UploadedFile,
                          lowerPageToProcess: Int, upperPageToProcess: Int,
                          changeInPages: Int
    ) : StatusResult {
        val examPaperFileSave = pdf.saveFileWithAlternativeConfiguration(InternalStorageConfiguration("/app/userdata")) as UploadedFile.Local

        try {
            val remoteExamPaper = ExamResourcesREST.convert("EP",examPaperFileSave, lowerPageToProcess, upperPageToProcess, year, subjectID, lang, level)
            val heightPerPage = remoteExamPaper["heightPerPage"] as Int

            val newRemoteExamPaperReference = remoteExamPaper["reference"] as String
            val newRemoteExamPaperWidth = remoteExamPaper["width"] as Int

            val examPaperRatio = remoteExamPaperWidth.toDouble() / newRemoteExamPaperWidth.toDouble()

            for (question in questions.moduleValues()) {
                for (subQuestion in question.subQuestions.moduleValues()) {
                    subQuestion.startPosition = ((subQuestion.startPosition) / examPaperRatio) + (heightPerPage*changeInPages)
                    subQuestion.endPosition = ((subQuestion.endPosition) / examPaperRatio) + (heightPerPage*changeInPages)
                }
            }

            update(
                    QMap("remoteExamPaperReference", newRemoteExamPaperReference),
                    QMap("remoteExamPaperWidth", newRemoteExamPaperWidth)
            )
        } catch (e: Exception) {
            e.printStackTrace()
            ErrorStatus("Níor éirigh leis an athchóiriú. Tá an fhadhb á deisiú againn faoi láthair.")
        }

        return OKFormStatus("D'éirigh leis an athchóiriú")
    }

    @Web
    fun save(loadedQuestions: List<ExamQuestion.JSON>,
             newQuestions: List<ExamQuestion.JSON>,
             loadedAnswers: List<ExamMarkingScheme.JSON>,
             newAnswers: List<ExamMarkingScheme.JSON>,
             deletedQuestions: List<Long>,
             deletedSubQuestions: List<Long>,
             deletedAnswers: List<Long>) : StatusResult {
        val time1 = System.currentTimeMillis()

        val answersToInsert = mutableListOf<ExamMarkingScheme.JSON>()
        if (loadedAnswers.isNotEmpty()) { answersToInsert.addAll(loadedAnswers) }
        if (newAnswers.isNotEmpty()) { answersToInsert.addAll(newAnswers) }
        if (answersToInsert.isNotEmpty() || deletedAnswers.isNotEmpty() || deletedSubQuestions.isNotEmpty()) {
            for (question in questions.values) {
                if (question is ExamQuestion) {
                    for (subQuestion in question.subQuestions.moduleValues()) {
                        val b = deletedSubQuestions.find { it.toInt() == subQuestion.getID() }
                        if (b != null) {
                            question.subQuestions.delete(Where().value("ID", b))
                        } else {
                            val a = answersToInsert.filter {
                                val r = it.subQuestionID == subQuestion.getID()
                                if (r) it.mappedValues.remove("subQuestionID")
                                r
                            }
                                // a.mappedValues.remove("subQuestionID")
                            if (a.isNotEmpty()) {
                                val params = MultiAssociatingQMap("markingSchemes", a)
                                subQuestion.update(params)
                            }
                                // answersToInsert.remove(a)

                                for (markingScheme in subQuestion.markingSchemes.moduleValues()) {
                                    deletedAnswers.find { it.toInt() == markingScheme.getID() }?.let {
                                        subQuestion.markingSchemes.delete(Where().value("ID", it.toInt()))
                                    }
                                }

                                /*
                                deletedAnswers.find { it.toInt() == subQuestion.markingSchemes?.getID() }?.let {
                                    subQuestion.markingSchemes = null
                                }
                                */
                        }
                    }
                }
            }
        }

        if (deletedQuestions.isNotEmpty()) {
            val deleteWhere = Where()
            deletedQuestions.forEach {
                deleteWhere.orEqualValue("ID",it)
            }
            questions.delete(deleteWhere)
        }

        if (loadedQuestions.isNotEmpty()) {
            val a = MultiAssociatingQMap("questions", loadedQuestions)
            update(a)
        }
        if (newQuestions.isNotEmpty()) {
            val b = MultiAssociatingQMap("", newQuestions)
            questions.multiValueInsert(b.value as Array<out Array<QMap>>)
        }

        val time2 = System.currentTimeMillis()
        println("Am glactha: " + (time2 - time1))

        return OKFormStatus("D'éirigh leis an tsábháil")
    }

    @Web
    fun default() : StatusResult {
        return OKStatus(mapOf("examPaper" to this.toJSONObject()))
    }

    class JSON : JSONModule() {

    }

}