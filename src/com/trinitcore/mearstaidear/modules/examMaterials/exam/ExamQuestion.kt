package com.trinitcore.mearstaidear.modules.examMaterials.exam

import com.trinitcore.asd.annotations.SubWebServlet
import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.mearstaidear.modules.examMaterials.support.Topic
import com.trinitcore.sqlv2.commonUtils.AssociatingQMap
import com.trinitcore.sqlv2.queryUtils.module.*

class ExamQuestion : DataModule() {

    var examPaperID: Int by Attribute()
    var topicID: Int? by Attribute()
    var questionNumber: Double by Attribute()

    @SubWebServlet var subQuestions: DataModules<ExamSubQuestion> by Attribute()

    /*

    */

    @Web
    fun default() : StatusResult {
        return OKStatus(mapOf("question" to this.toJSONObject()))
    }

    class JSON : JSONModule() {
        var ID: Int? by NullableAttribute()
        var examPaperID: Int? by NullableAttribute()
        var topicID: Int? by NullableAttribute()
        var questionNumber: Double? by NullableAttribute()

        var subQuestions: List<ExamSubQuestion.JSON> by Attribute()
    }
}