package com.trinitcore.mearstaidear.modules.examMaterials.exam

import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.module.JSONModule
import com.trinitcore.sqlv2.queryUtils.module.NullableAttribute

class ExamMarkingScheme : DataModule() {

    var startPosition: Double by Attribute()
    var endPosition: Double by Attribute()
    var subQuestionID: Int by Attribute()

    class JSON : JSONModule() {
        var ID: Int? by NullableAttribute()

        var startPosition: Double by Attribute()
        var endPosition: Double by Attribute()

        var subQuestionID: Int? by NullableAttribute()
    }

}