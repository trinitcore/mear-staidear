package com.trinitcore.mearstaidear.users.student

import com.trinitcore.asd.resourceTools.email.contents.EmailContent
import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.mearstaidear.rest.adminStudent.CommonREST
import com.trinitcore.mearstaidear.rest.student.ExamPapersREST
import com.trinitcore.mearstaidear.rest.student.NotesREST
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.ModuleTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder

class StudentType : UserType<Student>(ExamPapersREST::class, NotesREST::class, ID = 0) {

    override val reactJSAllowedPages: Array<String>
        get() = arrayOf("/student")
    override val reactJSLoginRedirectPage: String
        get() = "/student/dashboard"
    override fun tables(): MutableList<GenericTableBuilder>
        = mutableListOf(

    )

    override fun overrideTables(): Map<String, (tableBuilder: GenericTableBuilder) -> Unit> = mapOf("exampapers" to {it -> (it as ModuleTableBuilder<*>).readOnly()})

    override fun configureUserTable(userTableBuilder: TableBuilder) {

    }

    override fun confirmationEmailContent(userData: User): EmailContent? {
        return null
    }
}