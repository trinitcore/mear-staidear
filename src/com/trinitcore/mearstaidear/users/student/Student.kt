package com.trinitcore.mearstaidear.users.student

import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.sqlv2.queryUtils.module.Attribute

class Student(userType: UserType<*>) : User(userType) {

    var email: String by Attribute()

}