package com.trinitcore.mearstaidear.users

import com.trinitcore.asd.servlet.AdvancedServlet
import com.trinitcore.asd.user.CommonUserType
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.configurations.Column
import com.trinitcore.mearstaidear.modules.examMaterials.exam.ExamMarkingScheme
import com.trinitcore.mearstaidear.modules.examMaterials.exam.ExamPaper
import com.trinitcore.mearstaidear.modules.examMaterials.exam.ExamQuestion
import com.trinitcore.mearstaidear.modules.examMaterials.exam.ExamSubQuestion
import com.trinitcore.mearstaidear.modules.notes.*
import com.trinitcore.mearstaidear.rest.adminStudent.CommonREST
import com.trinitcore.mearstaidear.users.admin.AdminType
import com.trinitcore.mearstaidear.users.student.StudentType
import com.trinitcore.sqlv2.commonUtils.row.Row
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.ModuleTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.sqlv2.queryUtils.associationV2.Associating
import com.trinitcore.sqlv2.queryUtils.associationV2.table.RowAssociation
import com.trinitcore.sqlv2.queryUtils.associationV2.table.RowsAssociation
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import kotlin.reflect.KClass

class AdminStudentTeacherUserType : CommonUserType(AdminType::class, StudentType::class) {

    override fun servletsForTableSharing(): Array<KClass<out AdvancedServlet>> = arrayOf(
            CommonREST::class
    )

    override fun configureUserTable(userTableBuilder: TableBuilder): Boolean {
        return false
    }

    private fun proposalTypes(it: Row, deletionProposal: () -> DataModule, modificationProposal: () -> DataModule, additionProposal: () -> DataModule) : DataModule {
        return if (it["deletion"] == true) deletionProposal() else if (it["modification"] == true) modificationProposal() else if (it["addition"] == true) additionProposal() else throw Exception("Níor frítheadh an réad ceart mar gheall nach raibh réad-cineál sainaithinte.")
    }

    override fun tables(): Array<GenericTableBuilder> = arrayOf(
            ModuleTableBuilder("noteselemmentsproposals") { proposalTypes(it, { NoteElement.DeletionProposal() }, { NoteElement.ModificationProposal() }, { NoteElement.AdditionProposal() }) }
                    .addAssociation(
                            RowAssociation("notestablesproposals", Associating("tableID", "table", "ID")).module { NoteTable.Proposal() }
                                    .addAssociation(
                                            RowsAssociation("notestablesrowsproposals", Associating("ID", "rows", "tableID")).module { proposalTypes(it, { NoteTable.Row.DeletionProposal() }, { NoteTable.Row.ModificationProposal() }, { NoteTable.Row.AdditionProposal() }) }
                                                    .addAssociation(
                                                            RowsAssociation("notestablescolumnsproposals", Associating("ID", "columns", "rowID").blankRowsIfMatchNotFound()).module { proposalTypes(it, { NoteTable.Column.DeletionProposal() }, { NoteTable.Column.ModificationProposal() }, { NoteTable.Column.AdditionProposal() }) }
                                                                    .addAssociation(
                                                                            RowAssociation("notestexts", Associating("paragraphTextID", "paragraphText", "ID")).module { NoteParagraphText() }
                                                                    )
                                                    )

                                    )
                                    .addAssociation(
                                            RowsAssociation("notestablesmetacolumnsproposals", Associating("ID","metaColumns","tableID")).module { proposalTypes(it, { NoteTable.MetaColumn.DeletionProposal() }, { NoteTable.MetaColumn.ModificationProposal() }, { NoteTable.MetaColumn.AdditionProposal() }) }
                                    )

                    )
                    .addAssociation(
                            RowAssociation("notestexts", Associating("paragraphTextID", "paragraphText", "ID")).module { NoteParagraphText() }
                    )
                    .addAssociation(
                            RowAssociation("notesimages", Associating("imageID", "image", "ID")).module { NoteImage() }
                    ),

            TableBuilder("topics"),
            TableBuilder("subjects"),
            ModuleTableBuilder("exampapers") { ExamPaper() }
                    .addAssociation(
                            RowsAssociation("exampapersquestions",
                                    Associating("ID","questions","examPaperID").blankRowsIfMatchNotFound()
                            ).module { ExamQuestion() }
                                    .addAssociation(
                                            RowsAssociation("exampaperssubquestions",
                                                    Associating("ID","subQuestions","questionID").blankRowsIfMatchNotFound()
                                            ).module { ExamSubQuestion() }
                                                    .addAssociation(
                                                            RowsAssociation("exampapersmarkingscheme",
                                                                    Associating("ID","markingSchemes","subQuestionID").blankRowsIfMatchNotFound()
                                                            ).module { ExamMarkingScheme() }
                                                    )
                                    )
                    ),
            ModuleTableBuilder("noteselementsproposals") { NoteElement.Proposal() }
                    .addAssociation(
                            RowAssociation("notestablesproposals", Associating("tableID", "table", "ID")).module { NoteTable.Proposal() }
                                    .addAssociation(
                                            RowsAssociation("notestablesrowsproposals", Associating("ID", "rows", "tableID")).module { NoteTable.Row.Proposal() }
                                                    .addAssociation(
                                                            RowsAssociation("notestablescolumnsproposals", Associating("ID", "columns", "rowID").blankRowsIfMatchNotFound()).module { NoteTable.Column.Proposal() }
                                                                    .addAssociation(
                                                                            RowAssociation("notestexts", Associating("paragraphTextID", "paragraphText", "ID")).module { NoteParagraphText() }
                                                                    )
                                                    )

                                    )
                                    .addAssociation(
                                            RowsAssociation("notestablesmetacolumnsproposals", Associating("ID","metaColumns","tableID")).module { NoteTable.MetaColumn.Proposal() }
                                    )

                    ),
            ModuleTableBuilder("notes") { Note() }
                    .addAssociation(
                            RowsAssociation("noteselements", Associating("ID","elements", "noteID")).module { NoteElement() }
                                    .addAssociation(
                                            RowAssociation("notestables", Associating("tableID", "table", "ID")).module { NoteTable() }
                                                    .addAssociation(
                                                            RowsAssociation("notestablesrows", Associating("ID", "rows", "tableID")).module { NoteTable.Row() }
                                                                    .addAssociation(
                                                                            RowsAssociation("notestablescolumns", Associating("ID", "columns", "rowID").blankRowsIfMatchNotFound()).module { NoteTable.Column() }
                                                                                    .addAssociation(
                                                                                            RowAssociation("notestexts", Associating("paragraphTextID", "paragraphText", "ID")).module { NoteParagraphText() }
                                                                                    )
                                                                    )

                                                    )
                                                    .addAssociation(
                                                            RowsAssociation("notestablesmetacolumns", Associating("ID","metaColumns","tableID")).module { NoteTable.MetaColumn() }
                                                    )

                                    )
                                    .addAssociation(
                                            RowAssociation("notestexts", Associating("paragraphTextID", "paragraphText", "ID")).module { NoteParagraphText() }
                                    )
                                    .addAssociation(
                                            RowAssociation("notesimages", Associating("imageID", "image", "ID")).module { NoteImage() }
                                    )
                    )
    )

    override fun userColumns(): Array<Column>? = null

}