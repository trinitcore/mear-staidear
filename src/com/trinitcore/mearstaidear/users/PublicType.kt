package com.trinitcore.mearstaidear.users

import com.trinitcore.asd.user.PublicUserType
import com.trinitcore.mearstaidear.html.MainReactJSServlet
import com.trinitcore.mearstaidear.html.MainUploadedWebResourcesServlet
import com.trinitcore.mearstaidear.html.MainWebResourcesServlet
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder

class PublicType : PublicUserType(MainReactJSServlet::class, MainUploadedWebResourcesServlet::class, MainWebResourcesServlet::class) {

    override fun publicTables(): MutableList<GenericTableBuilder> = mutableListOf(

    )
}