package com.trinitcore.mearstaidear.users.admin

import com.trinitcore.asd.resourceTools.email.contents.EmailContent
import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.mearstaidear.rest.admin.ExamEditorREST
import com.trinitcore.mearstaidear.rest.admin.ExamResourcesREST
import com.trinitcore.mearstaidear.rest.admin.NotesREST
import com.trinitcore.mearstaidear.rest.adminStudent.CommonREST
import com.trinitcore.sqlv2.queryObjects.Table
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder

class AdminType : UserType<Admin>(ExamResourcesREST::class, ExamEditorREST::class, NotesREST::class, ID = -1) {

    override val reactJSAllowedPages: Array<String>
        get() = arrayOf("/admin")
    override val reactJSLoginRedirectPage: String
        get() = "/admin/dashboard"
    override fun tables(): MutableList<GenericTableBuilder> = mutableListOf(
                TableBuilder("topics"),
                TableBuilder("exampapersmarkingscheme")
    )

    override fun configureUserTable(userTableBuilder: TableBuilder) {

    }

    override fun confirmationEmailContent(userData: User): EmailContent? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}