package com.trinitcore.mearstaidear.users.admin

import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.sqlv2.queryUtils.module.Attribute

class Admin(userType: UserType<*>) : User(userType) {
    var email: String by Attribute()
}