package com.trinitcore.mearstaidear.rest.admin

import com.trinitcore.asd.requestTools.Parameters
import com.trinitcore.asd.requestTools.SessionAttributes
import com.trinitcore.asd.responseTools.status.json.ErrorStatus
import com.trinitcore.asd.servlet.ModuleServlet
import com.trinitcore.mearstaidear.modules.notes.Note
import com.trinitcore.mearstaidear.modules.notes.NoteElement
import com.trinitcore.sqlv2.queryObjects.ModuleTable
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import javax.servlet.annotation.WebServlet

@WebServlet("/REST/admin/notesREST/*")
class NotesREST : ModuleServlet() {
    override val pathStructure: Array<String>
        get() = arrayOf("subjectID", "topicID")

    lateinit var notes: ModuleTable<Note>

    override fun moduleCreation(pathParameters: Parameters, sessionAttributes: SessionAttributes): DataModule? {
        return notes.findModule(
                Where().value("topicID", pathParameters.getInt("topicID")).value("subjectID", pathParameters.getInt("subjectID"))
        ).let {
            it
        }
    }

    override fun moduleNotFoundError(pathParameters: Parameters, sessionAttributes: SessionAttributes): ErrorStatus {
        return ErrorStatus("Níor aimsíodh nótaí le haghaidh na topaice")
    }

}