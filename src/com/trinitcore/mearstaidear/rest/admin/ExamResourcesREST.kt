package com.trinitcore.mearstaidear.rest.admin

import com.trinitcore.asd.Configuration
import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.requestTools.UploadedFile
import com.trinitcore.asd.resourceTools.fileStorage.InternalStorageConfiguration
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.ErrorStatus
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.mearstaidear.modules.examMaterials.exam.ExamPaper
import com.trinitcore.mearstaidear.rest.adminStudent.CommonREST
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.queryObjects.ModuleTable
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import java.awt.image.BufferedImage
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import java.io.File
import javax.servlet.annotation.MultipartConfig
import javax.servlet.annotation.WebServlet


@WebServlet("/REST/admin/examResources/*")
@MultipartConfig
class ExamResourcesREST : CommonREST() {

    lateinit var exampapers: ModuleTable<ExamPaper>

    /*
    @Web
    fun convertToImage(pdf: UploadedFile) : StatusResult {
        val fileSave = pdf.saveFileWithAlternativeConfiguration(InternalStorageConfiguration("/app/userdata")) as UploadedFile.Local

        val uploadedPath = "C:\\Users\\Cormac\\Documents\\Projects\\MearStaidéar\\uploaded\\"
        try {
            PDDocument.load(File(fileSave.file.path)).use { document ->
                val pdfRenderer = PDFRenderer(document)
                val bufferedImage = BufferedImage(2480, 3508*(document.numberOfPages-1), BufferedImage.TYPE_INT_RGB)
                val g = bufferedImage.graphics

                for (page in 1 until document.numberOfPages) {
                    val bim = pdfRenderer.renderImageWithDPI(page, 300f, ImageType.RGB)
                    g.drawImage(bim,0,(page-1)*3508,null)
                }
                val fileName = uploadedPath + "image.png"
                ImageIOUtil.writeImage(bufferedImage, fileName, 300)

                val fileToUpload = Configuration.getConfiguration().defineFileStorageConfiguration().generateHandler(File(fileName))
                val paperReference = (fileToUpload.write() as UploadedFile.Remote).fileRemoteReference

                document.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return OKFormStatus("The conversion is being processed.")
    }
*/

    companion object {
        fun convert(type: String, fileSave: UploadedFile.Local, lowerPageToProcess: Int, upperPageToProcess: Int,
                    year: Int, subjectID: Int, lang: Int, level: Int,
                    requestedWidth: Int? = null, requestedHeightPerPage: Int? = null, requestedTotalHeight: Int? = null, requestedDPI: Float? = null, requestedQuality: Float = 0.8f): Map<String, Any> {
            PDDocument.load(File(fileSave.file.path)).use { document ->
                val uploadedPath = "C:\\Users\\Cormac\\Documents\\Projects\\MearStaidear\\uploaded\\"

                val pdfRenderer = PDFRenderer(document)

                // val maxHeight = 55500.0
                val maxHeight = 65500.0

                var width = requestedWidth ?: 2480
                var heightPerPage = requestedHeightPerPage ?: 3508
                var totalHeight = requestedTotalHeight ?: heightPerPage*(upperPageToProcess - lowerPageToProcess + 1)
                var dpi = requestedDPI ?: 300f
                fun recalculateDimensions(height: Double) {
                    heightPerPage = Math.round(height / (upperPageToProcess - lowerPageToProcess + 1)).toInt()
                    totalHeight = height.toInt()
                    dpi = (300f / 3508f) * (height.toFloat() / (upperPageToProcess - lowerPageToProcess + 1).toFloat())
                    width = (2480 / 300) * (dpi.toInt())
                }

                if (totalHeight > maxHeight) {
                    recalculateDimensions(maxHeight)
                }

                val bufferedImage = BufferedImage(width, totalHeight, BufferedImage.TYPE_INT_RGB)
                val g = bufferedImage.graphics

                for (page in (lowerPageToProcess) until (upperPageToProcess + 1)) {
                    val bim = pdfRenderer.renderImageWithDPI(page - 1, dpi, ImageType.RGB)
                    g.drawImage(bim, 0, (page - lowerPageToProcess) * heightPerPage, null)
                }
                val fileName = "$uploadedPath$type$year$subjectID$lang$level.jpeg"

                ImageIOUtil.writeImage(bufferedImage, fileName, dpi.toInt(), requestedQuality)
                val file = File(fileName)
                val maxFileSize = 10485750
                val fileSize = file.length()
                if (fileSize > maxFileSize) {
                    return if (requestedQuality == 0.8f) {
                        convert(type, fileSave, lowerPageToProcess, upperPageToProcess,
                                year, subjectID, lang, level,
                                requestedWidth, requestedHeightPerPage, requestedTotalHeight, requestedDPI,0.5f)
                    } else {
                        val constantOfProportion = file.length() / totalHeight.toDouble()
                        val suggestedHeight = maxFileSize / constantOfProportion

                        recalculateDimensions(suggestedHeight)

                        convert(type, fileSave, lowerPageToProcess, upperPageToProcess,
                                year, subjectID, lang, level,
                                width, heightPerPage, totalHeight, dpi, requestedQuality)
                    }
                }

                val fileToUpload = Configuration.getConfiguration().defineFileStorageConfiguration().generateHandler(file)

                document.close()
                return mapOf(
                        "reference" to (fileToUpload.write() as UploadedFile.Remote).fileRemoteReference,
                        "width" to width,
                        "heightPerPage" to heightPerPage
                )
            }
        }
    }

    @Web
    fun convertToImage(examPaperPDF: UploadedFile, markingSchemePDF: UploadedFile, year: Int, subjectID: Int, lang: Int, level: Int,
                       examLowerPageToProcess: Int, examUpperPageToProcess: Int,
                       markingLowerPageToProcess: Int, markingSchemeUpperPageToProcess: Int
    ) : StatusResult {
        val examPaperFileSave = examPaperPDF.saveFileWithAlternativeConfiguration(InternalStorageConfiguration("/app/userdata")) as UploadedFile.Local
        val markingSchemeFileSave = markingSchemePDF.saveFileWithAlternativeConfiguration(InternalStorageConfiguration("/app/userdata")) as UploadedFile.Local

        try {
            val remoteMarkingScheme = convert("MS",markingSchemeFileSave, markingLowerPageToProcess, markingSchemeUpperPageToProcess, year, subjectID, lang, level)
            val remoteExamPaper = convert("EP",examPaperFileSave, examLowerPageToProcess, examUpperPageToProcess, year, subjectID, lang, level)

            val remoteMarkingSchemeReference = remoteMarkingScheme["reference"] as String
            val remoteExamPaperReference = remoteExamPaper["reference"] as String

            val remoteMarkingSchemeWidth = remoteMarkingScheme["width"] as Int
            val remoteExamPaperWidth = remoteExamPaper["width"] as Int

            exampapers.insert(
                    QMap("year",year),
                    QMap("subjectID",subjectID),
                    QMap("lang",lang),
                    QMap("level",level),
                    QMap("remoteExamPaperReference", remoteExamPaperReference),
                    QMap("remoteMarkingSchemeReference", remoteMarkingSchemeReference),
                    QMap("remoteMarkingSchemeWidth",remoteMarkingSchemeWidth),
                    QMap("remoteExamPaperWidth",remoteExamPaperWidth)
            )

        } catch (e: Exception) {
            e.printStackTrace()
            ErrorStatus("Níor éirigh leis an athchóiriú. Tá an fhadhb á deisiú againn faoi láthair.")
        }

        return OKFormStatus("D'éirigh leis an athchóiriú")
    }

    @Web
    fun findExamPaper(year: Int, subjectID: Int, lang: Int, level: Int) : StatusResult {
        val a = exampapers.find(Where()
                .value("year",year)
                .value("subjectID",subjectID)
                .value("lang",lang)
                .value("level",level)
        )
        return OKStatus(mapOf(
                "examPapers" to a.toJSONArray()
        ))
    }

    @Web
    fun default() : StatusResult {
        val a = Configuration.getConfiguration().userTypes[-1]?.tables
        return OKStatus(mapOf(
                "subjects" to subjects.find().toJSONArray()
        ))
    }


}