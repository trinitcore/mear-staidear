package com.trinitcore.mearstaidear.rest.admin

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.requestTools.Parameters
import com.trinitcore.asd.requestTools.SessionAttributes
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.ErrorStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.asd.servlet.ModuleServlet
import com.trinitcore.asd.servlet.RESTServlet
import com.trinitcore.mearstaidear.modules.examMaterials.exam.ExamPaper
import com.trinitcore.sqlv2.queryObjects.ModuleTable
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import javax.servlet.annotation.MultipartConfig
import javax.servlet.annotation.WebServlet

@MultipartConfig
@WebServlet("/REST/admin/examResources/examEditor/*")
class ExamEditorREST : ModuleServlet() {

    override val cacheModules: Boolean
        get() = true
    
    override val pathStructure: Array<String>
        get() = arrayOf("ID")

    lateinit var exampapers: ModuleTable<ExamPaper>

    override fun moduleCreation(pathParameters: Parameters, sessionAttributes: SessionAttributes): DataModule? {
        val examPaperID = pathParameters.getInt("ID")
        return exampapers.findModuleByID(examPaperID)
    }

    override fun moduleNotFoundError(pathParameters: Parameters, sessionAttributes: SessionAttributes): ErrorStatus {
        return ErrorStatus("Níor aimsíodh an samhail")
    }

    // applyChanges

}