package com.trinitcore.mearstaidear.rest.adminStudent

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.asd.servlet.RESTServlet
import com.trinitcore.sqlv2.queryObjects.Table
import org.json.simple.JSONArray
import javax.servlet.annotation.WebServlet

@WebServlet("/REST/adminStudent/common/*")
open class CommonREST : RESTServlet() {

    lateinit var subjects: Table
    lateinit var topics: Table

    @Web
    fun subjects() : StatusResult {
        return OKStatus(mapOf(
                "subjects" to subjects.find().toJSONArray()
        ))
    }

    @Web
    fun staticData(subjects: Boolean = false,
                   topics: Boolean = false) : StatusResult {
        val output = HashMap<String, JSONArray>()
        if (subjects) {
            output["subjects"] = this.subjects.find().toJSONArray()
        }
        if (topics) {
            output["topics"] = this.topics.find().toJSONArray()
        }

        return OKStatus(output)
    }

}