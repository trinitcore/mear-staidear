package com.trinitcore.mearstaidear.rest.student

import com.trinitcore.asd.requestTools.Parameters
import com.trinitcore.asd.requestTools.SessionAttributes
import com.trinitcore.asd.responseTools.status.json.ErrorStatus
import com.trinitcore.asd.servlet.ModuleServlet
import com.trinitcore.mearstaidear.modules.examMaterials.exam.ExamPaper
import com.trinitcore.sqlv2.queryObjects.ModuleTable
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import javax.servlet.annotation.WebServlet

@WebServlet("/REST/student/examPapers/*")
class ExamPapersREST : ModuleServlet() {
    override val pathStructure: Array<String>
        get() = arrayOf("subjectID","year")

    lateinit var exampapers: ModuleTable<ExamPaper>

    override fun moduleCreation(pathParameters: Parameters, sessionAttributes: SessionAttributes): DataModule? {
        return exampapers.findModule(Where().value("lang",0).value("subjectID", pathParameters.getInt("subjectID")).value("year", pathParameters.getInt("year")))
    }

    override fun moduleNotFoundError(pathParameters: Parameters, sessionAttributes: SessionAttributes): ErrorStatus {
        return ErrorStatus("Níor aimsíodh an pápéar scrúdaithe")
    }
}