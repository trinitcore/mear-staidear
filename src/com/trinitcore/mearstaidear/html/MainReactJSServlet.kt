package com.trinitcore.mearstaidear.html

import com.trinitcore.asd.responseTools.status.html.subNodes.ScriptNode
import com.trinitcore.asd.responseTools.status.html.subNodes.StylesheetNode
import com.trinitcore.asd.servlet.ReactJSServlet
import com.trinitcore.asd.servlet.UploadedWebResourcesServlet
import com.trinitcore.asd.servlet.WebResourcesServlet
import javax.servlet.annotation.WebServlet
import kotlin.reflect.KClass

@WebServlet("/*")
class MainReactJSServlet : ReactJSServlet() {
    override fun additionalScripts(): Array<ScriptNode> = arrayOf(
            ScriptNode(scriptURL = "jquery-3.4.1.min.js"),
            ScriptNode(scriptURL = "jquery.scrollify.js"),
            ScriptNode(scriptURL = "image-to-slices.js")
    )

    override fun additionalStylesheets(): Array<StylesheetNode> = arrayOf(StylesheetNode("bootstrap.min.css"), StylesheetNode("https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400&display=swap"))

    override fun uploadedResourceServlet(): KClass<out UploadedWebResourcesServlet> = MainUploadedWebResourcesServlet::class

    override fun webResourceServlet(): KClass<out WebResourcesServlet> = MainWebResourcesServlet::class
}