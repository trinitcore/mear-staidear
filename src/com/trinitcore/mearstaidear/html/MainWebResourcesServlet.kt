package com.trinitcore.mearstaidear.html

import com.trinitcore.asd.servlet.WebResourcesServlet
import javax.servlet.annotation.WebServlet

@WebServlet("/mainWebResources/*")
class MainWebResourcesServlet : WebResourcesServlet() {
    override fun subWebResourcePath(): String = "main"
}
