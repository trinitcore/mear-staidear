package com.trinitcore.mearstaidear

import com.trinitcore.asd.Configuration
import com.trinitcore.asd.resourceTools.email.configurations.GoogleMailEmailConfiguration
import com.trinitcore.asd.resourceTools.email.configurations.SMTPEmailConfiguration
import com.trinitcore.asd.resourceTools.fileStorage.CloudinaryStorageConfiguration
import com.trinitcore.asd.resourceTools.fileStorage.FileStorageConfiguration
import com.trinitcore.asd.resourceTools.fileStorage.InternalStorageConfiguration
import com.trinitcore.asd.user.CommonUserType
import com.trinitcore.asd.user.PublicUserType
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.configurations.ColumnType
import com.trinitcore.asd.user.configurations.predefined.EmailColumn
import com.trinitcore.asd.user.configurations.predefined.PasswordColumn
import com.trinitcore.asd.user.configurations.predefined.UsernameColumn
import com.trinitcore.mearstaidear.users.AdminStudentTeacherUserType
import com.trinitcore.mearstaidear.users.PublicType
import com.trinitcore.mearstaidear.users.admin.AdminType
import com.trinitcore.mearstaidear.users.student.StudentType
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.sqlv2.queryUtils.connection.ConnectionManager
import com.trinitcore.sqlv2.queryUtils.connection.PostgresConnectionManager
import javax.servlet.annotation.WebListener
import javax.servlet.http.HttpServlet

@WebListener
class ConfigurationServlet : HttpServlet() {
    init {
        Configuration.setupConfiguration(C())
    }

    class C : Configuration() {
        override fun UTCTimeOffset(): Int = 0

        override fun configureLoginUserTable(userTableBuilder: TableBuilder) {

        }

        override fun databaseConnection(): ConnectionManager = PostgresConnectionManager(
/*
                "ec2-176-34-184-174.eu-west-1.compute.amazonaws.com",
                "d9n9osrctoqr1l",
                "jdfenudbjwqwqv",
                "dfe748cd993683f2c99ed8a3690af8298d769ab4c92565e1619c1372de5dba83"
*/

                "localhost",
                "mearstaidear",
                "postgres",
                "@C[]4m9c17",false
        )


        override fun defineCommonUserTypes(): Array<CommonUserType> = arrayOf(
            AdminStudentTeacherUserType()
        )

        override fun defineEmailConfiguration(): SMTPEmailConfiguration = GoogleMailEmailConfiguration("","","","email_schedule")

        override fun defineFileStorageConfiguration(): FileStorageConfiguration =
                CloudinaryStorageConfiguration(
                        "dm4hr38iy",
                        932618742551287,
                        "Y8IZMVNoXkB33s3C9CsYsEAgL-U"
                        )

        override fun defineLoginColumns(): Array<ColumnType> = arrayOf(
                EmailColumn(), PasswordColumn()
        )

        override fun definePublicUserType(): PublicUserType = PublicType()

        override fun defineResourcesDirectory(): String = "resources"

        override fun defineUserTypes(): Array<UserType<*>> = arrayOf(
                StudentType(),
                AdminType()
        )

        override fun projectTitle(): String = "Mear Staidear"

        override fun projectVersion(): Int = 2

        override fun resetPasswordLinkSuffix(resetKey: String, userID: Int?): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun websiteLinkPrefix(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }

}