import React from "react";
import SessionFrame from "trinreact/layouts/SessionFrame"
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {s} from "../../App";
import {Link} from "react-router-dom";
import ExamEditor from "./examResources/ExamEditor";
import NoteRender from "../../uiComponents/NoteRender";

class Notes extends SessionFrame {

    constructor(props) {
        super(props, {
            didLoadMetaDataSubjects: false
        });
    }

    componentDidMount() {
        this.request(link("adminStudent.CommonREST","staticData"), {subjects: true, topics: true})
            .success(data => this.setState({subjects: data.subjects, topics: data.topics, didMetaDataSubjects: true})).performRequest();
        if (this.props.match.params.subjectID != null && this.props.match.params.year != null) {
            // this.load();
        }
        this.load();
    }

    load() {
        this.request(link("admin.NotesREST","1/1"))
            .success(data => {
                this.setState({noteData: data.note, didMetaDataSubjects: true})
            }).performRequest();
    }

    render() {
        return (
            <div>
                {(this.state.didMetaDataSubjects) ? <NoteRender
                    /*
                    subjectID={this.props.match.params.subjectID}
                    topicID={this.props.match.params.subjectID}
                    */
                    subjectID={1}
                    topicID={1}

                    subjects={this.state.subjects}
                    topics={this.state.topics}


                    noteDataRequest={(subjectID, topicID, versionNumber, complete) => {
                        this.request(link("admin.NotesREST","1/1"))
                            .success(data => {
                                complete(data.note);
                                this.setState({noteData: data.note})
                            }).performRequest();
                    }}
                /> : null}
            </div>
        )
    };

}
Notes.package = "admin.Notes";

export default Notes;