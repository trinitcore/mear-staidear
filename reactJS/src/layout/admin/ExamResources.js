import React from "react";
import {ProgressSessionFrame} from "trinreact"
import {Form, TrinUploadField} from "trinreact"
import FormInterface from "../../uiComponents/FormInterface";
import Button from "../../uiComponents/Button";
import TextInput from "../../uiComponents/TextInput";
import {doSignOut} from "trinreact";
import SelectField from "../../uiComponents/SelectField";
import {getAllAvailableYears} from "../../App";
import {Link} from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
    resourceCard: {
        maxWidth: 345,
        textAlign: "center",
        margin: "auto"
    },
    actionArea: {

    }
};

const langs = [
    {ID: 0, label:"Gaeilge"},
    {ID: 1, label:"Béarla"}
];

const levels = [
    {ID: 0, label:"Ard"},
    {ID: 1, label:"Gnáth"},
    {ID: 2, label:"Bun"},
    {ID: 3, label:"Comónta"}
];

class ExamResources extends ProgressSessionFrame {

    constructor(props,state) {
        super(props,Object.assign({
            searchData: []
        },state));
        this.requestWithProgress(link("admin.ExamResourcesREST","default")).performRequest()
    }

    successRender(data,status) {
        const { classes } = this.props;

        return (
            <div className={"container"}>
                <div className={"row"} style={{marginBottom:16}}>
                    <h2>Cuir scrúdpháipéar leis</h2>
                    <Form URL={link("admin.ExamResourcesREST","convertToImage")}>
                        <SelectField name={"year"} label={"Bliain"} values={
                                getAllAvailableYears().map(year => {
                                    return {ID: year, label: year};
                                })
                            }/>
                        <SelectField name={"subjectID"} values={data.subjects.map(subject => {
                                return {ID: subject.ID, label: subject.titleGA}
                            })} label={"Ábhar"}/>
                        <SelectField name={"lang"} values={langs} label={"Teanga"}/>
                        <SelectField name={"level"} values={levels} label={"Leibhéal"}/>

                        <h5>Socruithe scrúdpháipéir</h5>
                        <TextInput name={"examLowerPageToProcess"} label={"L. Iosta le próiseáil"}/>
                        <TextInput name={"examUpperPageToProcess"} label={"L. Uasta le próiseáil"}/>

                        <h5>Socruithe scéim márcála</h5>
                        <TextInput name={"markingLowerPageToProcess"} label={"L. Iosta le próiseáil"}/>
                        <TextInput name={"markingSchemeUpperPageToProcess"} label={"L. Uasta le próiseáil"}/>

                        <h5>PDF an pháipéir scrúdaithe</h5>
                        <TrinUploadField name={"examPaperPDF"} />

                        <h5>PDF na scéime marcála</h5>
                        <TrinUploadField name={"markingSchemePDF"} />

                        <FormInterface>
                            <Button submit={true} label={"Uaslódáil"} />
                        </FormInterface>
                    </Form>
                </div>
                <div className={"row"} style={{marginTop:32}}>
                    <h2>Cuardaigh scrúdpháipéar lena chur in eagar</h2>
                    <Form URL={link("admin.ExamResourcesREST","findExamPaper")} postRequestHandler={(success,message,formInterface,_,data) => {
                        if (success) {
                            this.setState({searchData: data.examPapers});
                            formInterface.setNotInProgress(status,data.message);
                            return false
                        } else {
                            return true
                        }
                    }}>
                        <SelectField name={"year"} label={"Bliain"} values={
                            getAllAvailableYears().map(year => {
                                return {ID: year, label: year};
                            })
                        }/>
                        <SelectField name={"subjectID"} values={data.subjects.map(subject => {
                            return {ID: subject.ID, label: subject.titleGA}
                        })} label={"Ábhar"}/>
                        <SelectField name={"lang"} values={langs} label={"Teanga"}/>
                        <SelectField name={"level"} values={levels} label={"Leibhéal"}/>
                        <FormInterface>
                            <Button submit={true} label={"Cuardaigh"} />
                        </FormInterface>
                    </Form>
                    {this.state.searchData.map( item => {
                        return (
                            <div>
                                <Link to={"/admin/examResources/examEditor/"+item.ID}><Card className={classes.resourceCard}>
                                    <CardActionArea className={classes.actionArea}>
                                        <CardContent>
                                            <div className={"row"}>
                                                <div className={"col-md-3"}><h4>{data.subjects.find(subject => {return (subject.ID === item.subjectID)}).titleGA}</h4></div>
                                                <div className={"col-md-3"}><h4>{item.year}</h4></div>
                                                <div className={"col-md-3"}><h4>{langs.find(lang => {return (lang.ID === item.lang)}).label}</h4></div>
                                                <div className={"col-md-3"}><h4>{levels.find(level => {return (level.ID === item.lang)}).label}</h4></div>
                                            </div>
                                        </CardContent>
                                    </CardActionArea>
                                </Card></Link>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}
ExamResources.package = "admin.ExamResources";

export default withStyles(styles)(ExamResources);