import React, {Component} from "react";
import SessionFrame from "trinreact/layouts/SessionFrame"
import {ProgressSessionFrame, Form, TrinUploadField, TrinFormInterface as FormInterface} from "trinreact";
import "./ExamEditor.css";
import Button from "../../../uiComponents/Button";
import TextInput from "../../../uiComponents/TextInput";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import interact from "interactjs";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import Typography from "@material-ui/core/es/Typography/Typography";

class ExamEditor extends ProgressSessionFrame {

    /*
    Sonraaí nua/nuashonraithe
    Ceist
    {
        ID : null,
        questionNumber : null
        subQuestions; [

        ]
    }

    Fócheist
    {
        ID : null,
        startPosition: null,
        endPosition: null,
        solutionID: null,
        markingSchemeAnswerID: null,
        orderNumber: null,
        questionID: null
    }
     */

    constructor(props,state) {
        super(props,Object.assign({
            questionContainers: [],
            answerContainers: [],

            questionOptionsDialogVisible: false,
            settingsVisible: false
        },state));

        const request = this.requestWithProgress(link("admin.ExamEditorREST",this.props.match.params.ID),{

        });
        request.performRequest();
        this.deletedQuestions = [];
        this.deletedSubQuestions = [];
        this.deletedAnswers = [];

        this.questionNumber = React.createRef();
        this.modifiedQuestionNumber = React.createRef();

        this.loadedQuestionsContainers = [];

        this.selectedSubContainerID = null;

        this.examImageLoaded = true;
        this.markingSchemeImageLoaded = true;

        this.imageLoadedCalled = false;

        this.questionContainerOffsets = 11+3+3;
    }

    componentWillMount() {
        this.props.appController.setNavigationInstructions([
            {
                label: "Faic",
                link: "#div1",
                selectedDeterminator: (selectedDeterminator) => {
                    // Cuir fios ar an Navigation selectedDeterminator()
                }
            }
        ]);
    }

    setQuestionContainer(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(elmnt.id + "-header")) {
            /* if present, the header is where you move the DIV from:*/
            document.getElementById(elmnt.id + "-header").onmousedown = dragMouseDown;
        } else {
            /* otherwise, move the DIV from anywhere inside the DIV:*/
            elmnt.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";

            if (!elmnt.className.includes("question-modified")) {
                elmnt.className += " " + "question-modified";
            }
        }

        function closeDragElement() {
            /* stop moving when mouse button is released:*/
            document.onmouseup = null;
            document.onmousemove = null;
        }

        console.log(elmnt.className);
        interact('#' + elmnt.id.replace(".","\\.") + ' > .' + "question-content")
            .draggable({
                onmove: window.dragMoveListener,
                modifiers: [
                    interact.modifiers.restrictRect({
                        restriction: 'parent'
                    })
                ]
            })
            .resizable({
                // resize from all edges and corners
                edges: { bottom: true, },

                modifiers: [
                    // keep the edges inside the parent
                    interact.modifiers.restrictEdges({
                        outer: 'parent',
                        endOnly: true
                    }),

                    // minimum size
                    interact.modifiers.restrictSize({
                        min: { width: 100, height: 50 }
                    })
                ],

                // inertia: true
            })
            .on('resizemove', function (event) {
                const target = event.target
                event.target.style.cursor = 'context-menu'
                // update the element's style
                target.style.width = event.rect.width + 'px'
                target.style.height = event.rect.height + 'px';

                if (!elmnt.className.includes("question-modified")) {
                    elmnt.className += " " + "question-modified";
                }
            }).styleCursor(false)
    }

    setAnswerContainer(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(elmnt.id + "-header")) {
            /* if present, the header is where you move the DIV from:*/
            document.getElementById(elmnt.id + "-header").onmousedown = dragMouseDown;
        } else {
            /* otherwise, move the DIV from anywhere inside the DIV:*/
            elmnt.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";

            if (!elmnt.className.includes("answer-modified")) {
                elmnt.className += " " + "answer-modified";
            }
            if (1 <= 2) {

            }
        }

        function closeDragElement() {
            /* stop moving when mouse button is released:*/
            document.onmouseup = null;
            document.onmousemove = null;
        }

        var resizer = document.createElement('div');
        resizer.className = 'resizer';
        resizer.style.width = '10px';
        resizer.style.height = '10px';
        resizer.style.background = 'red';
        resizer.style.position = 'absolute';
        resizer.style.right = 0;
        resizer.style.bottom = 0;
        resizer.style.cursor = 'se-resize';
        elmnt.appendChild(resizer);
        resizer.addEventListener('mousedown', initResize, false);

        function initResize(e) {
            window.addEventListener('mousemove', Resize, false);
            window.addEventListener('mouseup', stopResize, false);
        }
        function Resize(e) {
            //console.log(e);
            //console.log(elmnt);
            const scrollTop = document.getElementById("answer-editor-scroll").scrollTop - 48;
            // elmnt.style.height = (e.clientY + scrollTop - elmnt.offsetTop) + 'px';
            elmnt.getElementsByClassName("answer-content")[0].style.height = (e.clientY + scrollTop - elmnt.offsetTop) + 'px';

            if (!elmnt.className.includes("answer-modified")) {
                elmnt.className += " " + "answer-modified";
            }
        }
        function stopResize(e) {
            window.removeEventListener('mousemove', Resize, false);
            window.removeEventListener('mouseup', stopResize, false);
        }

    }

    setSubQuestionContainer = (id) => {
        const elmnt = document.getElementById("split-"+id);
        const header = document.getElementById("split-" + id + "-header");

        elmnt.onclick = () => {
            this.setAnswerContainerVisibility(id);
        };

        var pos2 = 0, pos4 = 0;

        header.onmousedown = dragMouseDown;

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        const elementDrag = (e) => {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos2 = pos4 - e.clientY;
            pos4 = e.clientY;

            const newPosition = (elmnt.offsetTop - (pos2));

            // set the element's new position:
            elmnt.style.top = (newPosition-3) + "px";

            if (elmnt.className.includes("split-unmodified")) {
                elmnt.className = elmnt.className.replace("split-unmodified","");
                elmnt.className += " " + "split-modified";
            }
        };

        function closeDragElement() {
            /* stop moving when mouse button is released:*/
            document.onmouseup = null;
            document.onmousemove = null;
        }
    };

    onDragStart(e) {
        e.preventDefault();
    }

    geMarkingSchemeRatio = () => {
        const examImg = document.getElementById("admin-exam-editor-img");
        // const examImg = document.getElementById("admin-marking-scheme-editor-img");
        const remoteMarkingSchemeWidth = this.state.data.examPaper.remoteMarkingSchemeWidth;
        return (remoteMarkingSchemeWidth / examImg.offsetWidth);
    };

    getExamPaperRatio = () => {
        // const examImg = document.getElementById("admin-marking-scheme-editor-img");
        const examImg = document.getElementById("admin-exam-editor-img");

        const remoteExamPaperWidth = this.state.data.examPaper.remoteExamPaperWidth;
        console.log(examImg.offsetWidth);
        return (remoteExamPaperWidth / examImg.offsetWidth);
    };

    getExamPaperActualPosition = (virtualVerticalPosition) => {
        return ((virtualVerticalPosition)/this.getExamPaperRatio());
    };

    getExamPaperVirtualPosition = (actualVerticalPosition, deviation) => {
        return (actualVerticalPosition+(-deviation))*this.getExamPaperRatio();
    };

    getMarkingSchemeActualPosition = (virtualVerticalPosition) => {
        return ((virtualVerticalPosition)/this.geMarkingSchemeRatio());
    };

    getMarkingSchemeVirtualPosition = (actualVerticalPosition, deviation) => {
        return (actualVerticalPosition+(-deviation))*this.geMarkingSchemeRatio();
    };

    setAnswerContainerVisibility = (divisionID) => {
        function setVisibility(ID,hidden) {
            console.log(ID);
            const split = document.getElementById("split-" + ID);
            const header = document.getElementById("split-" + ID + "-header");
            header.style.backgroundColor = hidden ? "red" : "green";
            if (split != null) {
                split.style.zIndex = hidden ? "0" : "100";
            }
            header.style.zIndex = hidden ? "0" : "100";

            const divisions = Array.from(document.getElementsByClassName("answer-splitID-" + ID));
            divisions.forEach((division) => {
                division.style.display = hidden ? "none" : "block";
            });
        }
        setVisibility.bind(this);

        if (this.selectedSubContainerID != null) {
            console.log("selectedSubContainerID");
            setVisibility(this.selectedSubContainerID, true);
        }

        console.log("divisionID");
        setVisibility(divisionID, false);
        this.selectedSubContainerID = divisionID;
    };

    deleteAnswer = (splitID, answerID, ignoreRegistry) => {
        let aa;
        if (answerID == null) aa = Array.from(document.getElementsByClassName("answer-splitID-" + splitID));
        else aa = [document.getElementById("answer-" + answerID + "-" + splitID)];
        aa.forEach((a) => {
            if (a.className.includes("answer-loaded") && ignoreRegistry == null) {
                console.log("Scriosadh sa database", parseInt(a.className.split(' ')[1].replace("answer-ID-", "")));
                this.deletedAnswers.push(parseInt(a.className.split(' ')[1].replace("answer-ID-", "")));
            }
            this.selectedSubContainerID = null;
            a.remove();
        });
    };

    questionContainerDeviation = -3-11;
    generateQuestionContainer = (newQuestion, questionNumber, requestedHeight, requestedVerticalPosition, givenLowerSubQuestionID, questionID, floatSide) => {
        const height = (this.getExamPaperActualPosition(requestedHeight) || 200);
        const verticalPosition = this.getExamPaperActualPosition(requestedVerticalPosition)+this.questionContainerDeviation || document.getElementById("exam-editor-scroll").scrollTop + 20;
        const lowerSubQuestionID = givenLowerSubQuestionID || Math.random();

        const ref = React.createRef();
        this.loadedQuestionsContainers.push(ref);
        const refPosition = parseInt((this.loadedQuestionsContainers.length - 1).toString());

        return () => {
            return (
            <div ref={ref} id={"question-"+questionNumber} className={("lower-sub-ID-"+lowerSubQuestionID) + ((questionID != null) ? (" question-ID-" + questionID) : "" ) + " question-div " + (newQuestion ? "question-new" : "question-loaded")} style={{top: verticalPosition}}>
                <div id={"question-"+questionNumber+"-header"} className="question-header"><h6>Gabhdán ceiste - Brúigh agus Tarraing anseo chun a suíomh a athrú</h6></div>
                <div className={"question-content"} style={{height:height}}>
                    {this.state.questionContainers.find(container => {return container.questionNumber === questionNumber}).splits}
                        <span id={"split-" + lowerSubQuestionID + "-header"} className={"split-lowest"} style={{width: "100%", bottom: -5, left: 0, height: 5, backgroundColor: "red", zIndex: 10, position: "absolute"}}
                        onClick={() => {
                            this.setAnswerContainerVisibility(lowerSubQuestionID);
                        }}/>
                        <div className={"question-options-left"}>
                            <Button style={{fontSize: 6}} label={"Roghanna"} onClick={() => {
                                const accessRefQuestionNumber = parseFloat(this.loadedQuestionsContainers[refPosition].current.id.replace("question-",""));
                                this.handleClickOpen(accessRefQuestionNumber, questionID)
                            }}/>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        defaultChecked={floatSide}
                                        onChange={event => {
                                            const subElmnt = document.getElementById("question-"+questionNumber);
                                            if (event.target.checked) {
                                                subElmnt.className += " " + "split-floatSide";
                                            } else {
                                                subElmnt.className = subElmnt.className.replace(" split-floatSide","")
                                            }

                                            const elmnt = document.getElementById("question-"+questionNumber);
                                            if (!elmnt.className.includes("question-modified")) {
                                                elmnt.className += " " + "question-modified";
                                            }
                                        }}
                                    />
                                }
                                label={<Typography style={{fontSize: 11}}>Snámh ar chlé</Typography>}
                            />
                        </div>
                        <div className={"question-options-right"}>
                            <Button onClick={() => this.addAnswerContainer(lowerSubQuestionID)} style={{fontSize: 6}} label={"Nasc cuid scéime márcála leis"}/>
                        </div>
                </div>
            </div>)};
    };

    addQuestionContainer = (givenNumber) => {
        const questionNumber = givenNumber || this.questionNumber.current.state.value;

        const questionContainer = this.generateQuestionContainer(true,questionNumber);

        this.setState(prevState => ({
            questionContainers: [...prevState.questionContainers, {questionNumber: questionNumber, html:questionContainer, splits:[]}]
        }), () => {
            // this.newData.push({

            // });
            this.setQuestionContainer(document.getElementById("question-" + questionNumber));
        });
    };

    splitContainerDeviation = -6;
    generateSplitToQuestionContainer = (newSplit, id, questionNumber, requestedPosition, ignore, floatSide) => {
        const position = this.getExamPaperActualPosition(requestedPosition)+this.splitContainerDeviation || 16;
        if (ignore) {
            console.log("Tabhair neamhaird",id);
        }

        return <div id={"split-" + id} className={"split-unmodified container-split " + (newSplit ? "split-new" : "split-loaded") + " " + (ignore ? "split-ignored" : "")}
                            style={{position: "absolute", left: 0, width: "100%", textAlign: "right", top: position+this.splitContainerDeviation}}>
            <FormControlLabel
                style={{
                    position: "absolute",
                    left: 6,
                    bottom: -15
                }}
                control={
                    <Checkbox
                        defaultChecked={ignore}
                        onChange={event => {
                            const subElmnt = document.getElementById("split-"+id);
                            if (event.target.checked) {
                                subElmnt.className += " " + "split-ignored";
                            } else {
                                subElmnt.className = subElmnt.className.replace(" split-ignored","")
                            }

                            const elmnt = document.getElementById("question-"+questionNumber);
                            if (!elmnt.className.includes("question-modified")) {
                                elmnt.className += " " + "question-modified";
                            }
                        }}
                    />
                }
                label={<Typography style={{fontSize: 11}}>T. Neamhaird</Typography>}
            />
            <FormControlLabel
                style={{
                    position: "absolute",
                    left: 106,
                    bottom: -15
                }}
                control={
                    <Checkbox
                        defaultChecked={floatSide}
                        onChange={event => {
                            const subElmnt = document.getElementById("split-"+id);
                            if (event.target.checked) {
                                subElmnt.className += " " + "split-floatSide";
                            } else {
                                subElmnt.className = subElmnt.className.replace(" split-floatSide","")
                            }

                            const elmnt = document.getElementById("question-"+questionNumber);
                            if (!elmnt.className.includes("question-modified")) {
                                elmnt.className += " " + "question-modified";
                            }
                        }}
                    />
                }
                label={<Typography style={{fontSize: 11}}>Snámh ar chlé</Typography>}
            />
            <Button color={"secondary"} onClick={() => {
                const elmnt = document.getElementById("question-"+questionNumber);
                if (!elmnt.className.includes("question-modified")) {
                    elmnt.className += " " + "question-modified";
                }
                document.getElementById("split-" + id).remove();

                this.selectedSubContainerID = null;
                this.deletedSubQuestions.push(id);
                this.deleteAnswer(id);
            }} style={{fontSize: 6}} label={"Scrios"} />
            {newSplit ? null : <Button onClick={() => {
                this.addAnswerContainer(id)
            }} style={{fontSize: 6}}
                            label={"Nasc cuid scéime márcála leis"}/>}
                    <span id={"split-" + id + "-header"} style={{
                        backgroundColor: "red",
                        width: "100%",
                        height: "5px",
                        position: "absolute",
                        left: 0,
                        marginTop: 24,
                        zIndex: 6
                    }}/>
                </div>;
    };

    addSplitToQuestionContainer = (questionNumber, questionPosition) => {
        console.log(document.getElementById("exam-editor-scroll").scrollTop);
        console.log(questionPosition);

        const id = Math.random();
        let pos = this.getExamPaperVirtualPosition(document.getElementById("exam-editor-scroll").scrollTop - questionPosition,0);
        if (pos < 0) pos = null;
        const questionContainer = this.generateSplitToQuestionContainer(true, id, questionNumber, pos, false, false);

        const state = {...this.state};
        const data = state.questionContainers;
        const index = data.findIndex(obj => {
            return obj.questionNumber.toString() === questionNumber.toString()
        });
        const splits = data[index].splits;
        data[index].splits = [...splits, questionContainer];

        this.setState({state}, () => {
            this.setSubQuestionContainer(id);
        });
    };

    answerContainerDeviation = -3-11;
        //123;
    generateAnswerContainer = (divisionID, ID, loaded, requestedHeight, requestedPosition) => {
        const position = this.getMarkingSchemeActualPosition(requestedPosition)+this.answerContainerDeviation || document.getElementById("answer-editor-scroll").scrollTop + 20;
        const height = this.getMarkingSchemeActualPosition(requestedHeight) || 50;

        return () => {
            return (
                <div id={"answer-" + ID + "-" + divisionID} className={"answer-div " + (loaded ? ("answer-ID-"+ID + " answer-loaded") : "answer-new") + " answer-splitID-" + divisionID} style={{display: "none", top: position}}>
                    <div id={"answer-" + ID + "-" + divisionID + "-header"} className="answer-header"><h6>Gabhdán freagra - Brúigh agus Tarraing anseo chun a suíomh a athrú</h6></div>
                    <div className={"answer-content"} style={{height: height}}/>
                    <div className={"answer-options-right"}>
                        <Button color={"secondary"} style={{fontSize: 6}} onClick={() => {this.deleteAnswer(divisionID, ID)}} label={"Scrios"}/>
                    </div>
                </div>
            )
        };
    };

    addAnswerContainer = (divisionID) => {
        // if (this.state.answerContainers.find((answer) => {return answer.questionNumber === divisionID}) == null) {
            const ID = Math.random();
            const answerContainer = this.generateAnswerContainer(divisionID, ID, false);

            this.setState(prevState => ({
                answerContainers: [...prevState.answerContainers, {
                    questionNumber: divisionID,
                    html: answerContainer
                }]
            }), () => {
                console.log(divisionID);
                this.setAnswerContainer(document.getElementById("answer-" + ID + "-" + divisionID));
                this.setAnswerContainerVisibility(divisionID, true);
            });
        // }
    };

    imagesLoaded = (data) => {
        if (this.imageLoadedCalled) { return }
        this.imageLoadedCalled = true;
        document.getElementById("admin-marking-scheme-editor-canvas").scrollTop = 2461;

        // const ratio = this.getExamPaperRatio();

        const questionContainersToAdd = [];
        const answerContainersToAdd = [];

        data.examPaper.questions.forEach(question => {
            let firstPosition = 0;
            let lastPosition = 0;

            const splits = [];

            const subQuestions = question.subQuestions.sort((a, b) => (a.startPosition > b.startPosition) ? 1 : -1);

            subQuestions.forEach(subQuestion => {
                if (firstPosition === 0) {
                    firstPosition = subQuestion.startPosition;
                }
                if (firstPosition > subQuestion.startPosition) {
                    firstPosition = subQuestion.startPosition;
                }
                if (lastPosition < subQuestion.endPosition) {
                    lastPosition = subQuestion.endPosition;
                }
            });


            subQuestions.forEach((subQuestion,i) => {
                if (subQuestion.markingSchemes != null) {
                    subQuestion.markingSchemes.forEach((markingScheme) => {
                        const answerHeight = markingScheme.endPosition - markingScheme.startPosition;
                        answerContainersToAdd.push({
                            answerNumber: markingScheme.ID,
                            questionNumber: subQuestion.ID,
                            html: this.generateAnswerContainer(subQuestion.ID, markingScheme.ID, true, answerHeight, markingScheme.startPosition)
                        });
                    });
                }
                if (i + 1 !== question.subQuestions.length) {
                    splits.push(
                        this.generateSplitToQuestionContainer(false, subQuestion.ID, question.questionNumber, subQuestion.endPosition - firstPosition, subQuestion.ignored, subQuestion.floatSide)
                    );
                }
            });

            let height = (lastPosition - firstPosition);

            questionContainersToAdd.push(
                {
                    ID: question.ID,
                    questionNumber: question.questionNumber,
                    html: this.generateQuestionContainer(false,question.questionNumber, height, firstPosition, question.subQuestions[question.subQuestions.length - 1].ID, question.ID, question.subQuestions[question.subQuestions.length - 1].floatSide),
                    splits: splits
                }
            );
        });

                /*
                this.setAnswerContainer(document.getElementById("answer-" + divisionID));
                this.setAnswerContainerVisibility(divisionID, true);
                */

        this.setState(prevState => ({
            questionContainers: [...prevState.questionContainers, ...questionContainersToAdd],
            answerContainers: [...prevState.answerContainers, ...answerContainersToAdd]
        }), () => {
            console.log(this.state);
            questionContainersToAdd.forEach(questionContainerToAdd => {
                this.setQuestionContainer(document.getElementById("question-" + questionContainerToAdd.questionNumber));
                questionContainerToAdd.splits.forEach(split => {
                    this.setSubQuestionContainer(split.props.id.replace("split-",""));
                });
            });
            answerContainersToAdd.forEach(questionContainerToAdd => {
                this.setAnswerContainer(document.getElementById("answer-" + questionContainerToAdd.answerNumber + "-" + questionContainerToAdd.questionNumber));
            });
        });
    };

    handleRenderedQuestionData = (questionLoaded) => {
        let emptyModifications = false;

        let loadedData = Array.from(document.getElementsByClassName(
            (questionLoaded ? "question-loaded" : "question-new")
        )).map(loadedQuestion => {
            console.log(loadedQuestion);
            if (!loadedQuestion.className.includes("question-modified")) {
                if (loadedQuestion.getElementsByClassName("split-modified").length === 0) {
                    return null;
                }
            } else if (loadedQuestion.getElementsByClassName("split-modified").length === 0) {
                if (!loadedQuestion.className.includes("question-modified")) {
                    return null;
                }
            }

            const subQuestionsHTML = loadedQuestion.getElementsByClassName(
                "container-split "// + (subQuestionLoaded ? "split-loaded " : "split-new ")
            );
            const orderedLoadedSubQuestions = Array.from(subQuestionsHTML).sort((a, b) => (parseFloat(a.style.top) > parseFloat(b.style.top)) ? 1 : -1);

            let startPosition = this.getExamPaperVirtualPosition(parseFloat(loadedQuestion.style.top), this.questionContainerDeviation);
            let lastIndex = 0;
            const loadedSubQuestions = orderedLoadedSubQuestions.map((subQuestion, subQuestionIndex) => {
                lastIndex = subQuestionIndex;

                const endPosition = this.getExamPaperVirtualPosition(parseFloat(subQuestion.style.top), this.splitContainerDeviation)
                    + this.getExamPaperVirtualPosition(parseFloat(loadedQuestion.style.top), this.questionContainerDeviation-6);

                if (isNaN(startPosition) || isNaN(endPosition)) {
                    console.log(subQuestion);
                }

                try {
                    const subNewQuestion = subQuestion.className.includes("split-new");
                    return {
                            ID: subNewQuestion ? null : parseInt(subQuestion.id.replace("split-", "")),
                            startPosition: startPosition,
                            endPosition: endPosition,
                            orderNumber: (subQuestionIndex + 1),
                            ignored: subQuestion.className.includes("split-ignored"),
                            floatSide: subQuestion.className.includes("split-floatSide")
                    }
                } finally {
                    startPosition = endPosition
                }
            }).concat(
            [{
                ID: questionLoaded ?  parseInt(loadedQuestion.className.split(' ')[0].replace("lower-sub-ID-","")) : null,
                startPosition: startPosition,
                endPosition: this.getExamPaperVirtualPosition(parseFloat(loadedQuestion.style.top), this.questionContainerDeviation)
                + this.getExamPaperVirtualPosition(parseFloat(loadedQuestion.getElementsByClassName("question-content")[0].style.height), 0),
                orderNumber: (lastIndex + 2),
                ignored: false,
                floatSide: loadedQuestion.className.includes("split-floatSide")
            }]).filter((obj)=> {return obj !== null} );

            if (loadedSubQuestions.length === 0) {
                emptyModifications = true;
                return null;
            } else {
                emptyModifications = false;
                return {
                    ID: questionLoaded ? parseInt(loadedQuestion.className.split(' ')[1].replace("question-ID-","")) : null,
                    questionNumber: parseFloat(loadedQuestion.id.replace("question-","")),
                    subQuestions: loadedSubQuestions
                }
            }
        });

        return emptyModifications ? [] : loadedData.filter((obj)=> {return obj !== null} );
    };

    handleRenderedAnswerData = (answerLoaded) => {
        return Array.from(document.getElementsByClassName(
            (answerLoaded ? "answer-modified answer-loaded" : "answer-new")
        )).map(loadedAnswer => {
            console.log((parseFloat(loadedAnswer.style.top) - this.answerContainerDeviation)-(parseFloat(loadedAnswer.style.top) + parseFloat(loadedAnswer.getElementsByClassName("answer-content")[0].style.height) - this.answerContainerDeviation));
            let startPosition = this.getMarkingSchemeVirtualPosition(parseFloat(loadedAnswer.style.top), this.answerContainerDeviation);
            const endPosition = this.getMarkingSchemeVirtualPosition(parseFloat(loadedAnswer.style.top) + parseFloat(loadedAnswer.getElementsByClassName("answer-content")[0].style.height), this.answerContainerDeviation);

            const subQuestionID = parseInt(loadedAnswer.id.replace("answer-","").split("-")[1]);

            const data = {
                ID: answerLoaded ? parseInt(loadedAnswer.className.split(' ')[1].replace("answer-ID-","")) : null,
                startPosition: startPosition,
                endPosition: endPosition,
                subQuestionID: (subQuestionID === 0) ? null : subQuestionID
            };
            // if (answerLoaded) data.ID =
            return data;
        });
    };

    save = () => {
        const loadedQuestions = this.handleRenderedQuestionData(true);
        const newQuestions = this.handleRenderedQuestionData(false);

        console.log("Sonraí ceiste lódáilte", loadedQuestions);
        console.log("Sonraí ceiste nua", newQuestions);

        const loadedAnswers = this.handleRenderedAnswerData(true);
        const newAnswers = this.handleRenderedAnswerData(false);

        console.log("Sonraí freagra lódáilte", loadedAnswers);
        console.log("Sonraí freagra nua", newAnswers);

        console.log("Ceisteanna scriosta", this.deletedQuestions);
        console.log("Freagraí scriosta", this.deletedAnswers);


        this.request(link("admin.ExamEditorREST",this.props.match.params.ID+"/save"), {
            loadedQuestions: loadedQuestions,
            newQuestions: newQuestions,
            loadedAnswers: loadedAnswers,
            newAnswers: newAnswers,
            deletedQuestions: this.deletedQuestions,
            deletedAnswers: this.deletedAnswers,
            deletedSubQuestions: this.deletedSubQuestions
        }).success((data) => {
                window.location.reload(false)
            }
        ).performRequest();
    };

    handleClickOpen = (selectedQuestionNumber, selectedQuestionID) => {
        this.setState({questionOptionsDialogVisible: true, questionOptionNumber: selectedQuestionNumber, questionOptionID: selectedQuestionID});
    };

    handleClose = () => {
        this.setState({questionOptionsDialogVisible: false});
    };

    successRender = (data,status) => {
            return (
                <div className={"container-fluid"} style={{height: "94vh"}}>
                    <Button label={"Socruithe"} style={{position: "fixed", top: 50, left: 20, zIndex: 20}} onClick={() => {
                        this.setState({settingsVisible: !this.state.settingsVisible})
                    }} />
                    <Dialog
                        open={this.state.questionOptionsDialogVisible}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description">
                        <DialogTitle id="alert-dialog-title">{"Roghanna ceiste"}</DialogTitle>
                        <DialogActions>
                            <Button color={"secondary"} onClick={() => {
                                const questionNumber = this.state.questionOptionNumber;
                                const questionID = this.state.questionOptionID;

                                const elmnt = document.getElementById("question-" + questionNumber);
                                Array.from(elmnt.getElementsByClassName("container-split")).forEach(split => {
                                    if (split.className.includes("split-loaded")) {
                                        this.deleteAnswer(split.id.replace("split-", ""), null, true);
                                    }
                                });

                                elmnt.remove();

                                if (questionID != null)
                                    this.deletedQuestions.push(questionID);

                                this.handleClose();
                            }} label={"Scrios"}/>
                            <Button onClick={() => {
                                const questionNumber = this.state.questionOptionNumber;

                                const elmnt = document.getElementById("question-" + questionNumber);

                                this.addSplitToQuestionContainer(questionNumber, parseInt(elmnt.style.top));
                                this.handleClose();
                            }} color={"primary"} label={"Cuir scoilt leis"} />
                            <TextInput ref={this.modifiedQuestionNumber} label={"Uimhir na ceiste"} value={this.state.questionOptionNumber}/>
                            <Button label={"Athraigh an uimhir"} onClick={() => {
                                const questionNumber = parseFloat(this.modifiedQuestionNumber.current.getValue());
                                const questionID = this.state.questionOptionID;

                                const elmnt = document.getElementById("question-" + this.state.questionOptionNumber);
                                elmnt.id = "question-" + questionNumber;
                                document.getElementById("question-" + this.state.questionOptionNumber + "-header").id = elmnt.id + "-header";

                                this.setQuestionContainer(elmnt);

                                if (questionID != null) {
                                    this.request(link("admin.ExamEditorREST", this.props.match.params.ID + "/questions/" + questionID + "/applyChanges"), {
                                        changes: {
                                            questionNumber: questionNumber
                                        }
                                    }).success(() => {
                                        this.handleClose();
                                    }).performRequest();
                                }
                            }}/>
                        </DialogActions>
                        <DialogActions>
                            <Button style={{zIndex: 20}} onClick={this.handleClose} color="primary" label={"Ceallaigh"}/>
                        </DialogActions>
                    </Dialog>
                    <div className={"row"} style={{height: "100%"}}>
                        <div className={"col-md-2 admin-exam-editor-controls"} style={{overflowY: "auto", height: "100%", position: "fixed",  backgroundColor: "white", display: this.state.settingsVisible ? "block" : "none"}}>
                            <h3>Cuir Gabhdán ceiste leis</h3>
                            <TextInput ref={this.questionNumber} label={"Uimhir na ceiste"} />
                            <Button onClick={this.addQuestionContainer} label={"Cuir leis"} />
                            <Button variant={"secondary"} onClick={this.save} label={"Sábháil"}/>

                            <h5>Athraigh PDF an pháipéir scrúdaithe</h5>
                            <Form URL={link("admin.ExamEditorREST", this.props.match.params.ID + "/applyNewExamPaper")} postRequestHandler={
                                () => {
                                    window.location.reload(false)
                                }
                            }>
                                <TextInput name={"lowerPageToProcess"} label={"L. Iosta le próiseáil"}/>
                                <TextInput name={"upperPageToProcess"} label={"L. Uasta le próiseáil"}/>
                                <TextInput name={"changeInPages"} label={"Athrú i méid na l."}/>
                                <TrinUploadField name={"pdf"} />

                                <FormInterface>
                                    <Button submit={true} label={"Uaslódáil"}/>
                                </FormInterface>
                            </Form>

                            <h5>Athraigh PDF na scéime marcála</h5>
                            <Form URL={link("admin.ExamEditorREST", this.props.match.params.ID + "/applyNewMarkingScheme")} postRequestHandler={
                                () => {
                                    window.location.reload(false)
                                }
                            }>
                                <TextInput name={"lowerPageToProcess"} label={"L. Iosta le próiseáil"}/>
                                <TextInput name={"upperPageToProcess"} label={"L. Uasta le próiseáil"}/>
                                <TextInput name={"changeInPages"} label={"Athrú i méid na l."}/>
                                <TrinUploadField name={"pdf"} />

                                <FormInterface>
                                    <Button submit={true} label={"Uaslódáil"}/>
                                </FormInterface>
                            </Form>

                            <h5>Seach-cur na scéime marcála</h5>
                            <Form URL={link("admin.ExamEditorREST", this.props.match.params.ID + "/adjustMarkingSchemeContainers")} postRequestHandler={
                                () => {
                                    window.location.reload(false)
                                }
                            }>
                                <TextInput name={"offset"} label={"Seach-cur"} numeric={true}/>

                                <FormInterface>
                                    <Button submit={true} label={"Cuir i bhfeidhm"}/>
                                </FormInterface>
                            </Form>

                            <h5>Athraigh scála na ngabhdán sa scéim marcála</h5>
                            <Form URL={link("admin.ExamEditorREST", this.props.match.params.ID + "/changeScaleOfMarkingSchemeContainers")} postRequestHandler={
                                () => {
                                    window.location.reload(false)
                                }
                            }>
                                <TextInput name={"ratio"} label={"Coibhneas"} numeric={true} double={true}/>

                                <FormInterface>
                                    <Button submit={true} label={"Cuir i bhfeidhm"}/>
                                </FormInterface>
                            </Form>
                        </div>
                        <div className={"col-md-6"} id={"exam-editor-scroll"} style={{height: "100%", overflow: "auto"}}>
                            <div id={"admin-exam-editor-canvas"}>
                                {this.state.questionContainers.map((container) => {return container.html()})}
                                <img ref={(input) => {
                                    // onLoad replacement for SSR
                                    if (!input) { return; }
                                    const img = input;

                                    img.onload = () => {
                                        this.examImageLoaded = true;
                                        if (this.markingSchemeImageLoaded) {
                                            this.imagesLoaded(data);
                                        }
                                    };
                                }} id={"admin-exam-editor-img"} draggable="false" onDragStart={this.onDragStart} src={
                                    data.examPaper.remoteExamPaperReference
                                    /* resource("examResources/EP2018500.png") */
                                }/>
                            </div>
                        </div>
                        <div className={"col-md-6"} id={"answer-editor-scroll"} style={{height: "100%", overflow: "auto"}}>
                            <div id={"admin-marking-scheme-editor-canvas"}>
                                {this.state.answerContainers.map((container) => {return container.html()})}
                                <img ref={(input) => {
                                    // onLoad replacement for SSR
                                    if (!input) { return; }
                                    input.onload = () => {
                                        this.markingSchemeImageLoaded = true;
                                        if (this.examImageLoaded) {
                                            this.imagesLoaded(data);
                                        }
                                    };
                                }} id={"admin-marking-scheme-editor-img"} draggable="false" onDragStart={this.onDragStart} src={
                                    data.examPaper.remoteMarkingSchemeReference
                                    /* resource("examResources/MS2018500.png") */
                                }/>
                            </div>
                        </div>
                    </div>
                </div>
            )
    }
}
ExamEditor.package = "admin.examResources.ExamEditor";

export default ExamEditor;