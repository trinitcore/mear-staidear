import React from "react";
import SessionFrame from "trinreact/layouts/SessionFrame"
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {s} from "../../App";
import {Link} from "react-router-dom";
import ExamEditor from "./examResources/ExamEditor";

const styles = {
    resourceCard: {
        maxWidth: 345,
        textAlign: "center",
        margin: "auto"
    },
    actionArea: {
        paddingTop: 32
    },

    notificationCard: {
        paddingLeft: 16,
        paddingRight: 16
    },
    notificationCardTitle: {
        marginBottom: 2
    }
};

class Dashboard extends SessionFrame {

    constructor(props) {
        super(props);
        this.package = "admin.Dashboard";
    }

    resourceCard = (title, imageResource, link) => {
        const { classes } = this.props;
        return <Link to={link}><Card className={classes.resourceCard}>
            <CardActionArea className={classes.actionArea}>
                <img
                    height={64}
                    src={resource(imageResource)}
                />
                <CardContent>
                    <Typography gutterBottom variant="h6" component="h2">
                        {title}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card></Link>
    };

    render() {
        return (
            <div className={"dashboard-body"}>
                <div className={"container"}>
                    <div className={"row dashboard-options"}>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard("Acmhainní Scrúdpháipéir","sample_logo.png","/admin/examResources")}
                        </div>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard("Bainisteoireacht Nótaí","sample_logo.png","/admin/notes")}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
Dashboard.package = "admin.Dashboard";

export default withStyles(styles)(Dashboard);