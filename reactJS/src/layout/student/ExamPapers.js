import React from "react";
import SessionFrame from "trinreact/layouts/SessionFrame";
import {getAllAvailableYears, s} from "../../App";

import {withStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import AnimateHeight from "react-animate-height";
import Slider from '@material-ui/core/Slider';

import "./ExamPapers.css";

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        width: "100%",
    },
    selectEmpty: {},
};

class ExamPapers extends SessionFrame {

    examPaperStyles = {
        4 : {
            questions: {
                1 : {
                    title: <h2>{s().english1.firstSection.title}</h2>,
                    subtitle: <h4 dangerouslySetInnerHTML={{__html: s().english1.firstSection.subtitle}}/>,
                    marks: "100 marc",
                    bottomMarginBetweenDividedQuestions: 32,
                    topMarginBetweenDividedQuestions: 32
                },
                4 : {
                    bottomMarginBetweenDividedQuestions: 32,
                    topMarginBetweenDividedQuestions: 32
                }
            },
            marginBetweenQuestions: 64
        },
        5 : {
            questions: {
                1 : {
                    title: <h2>{s().physics.firstSection.title}</h2>,
                    subtitle: <h4>{s().physics.firstSection.subtitle}</h4>,
                    marks: "40 marc",
                    bottomMarginBetweenDividedQuestions: 32
                },
                5 : {
                    title: <h2>{s().physics.secondSection.title}</h2>,
                    subtitle: <h4>{s().physics.secondSection.subtitle}</h4>,
                    marks: "56 marc",
                    bottomMarginBetweenDividedQuestions: 32
                }
            }
        },
        6 : {
            questions: {
                1 : {
                    title: <h2>{s().biology.firstSection.title}</h2>,
                    subtitle: <h4 dangerouslySetInnerHTML={{__html : s().biology.firstSection.subtitle}}/>,
                    marks: "20 marc",
                    bottomMarginBetweenDividedQuestions: 32
                },
                7 : {
                    title: <h2>{s().biology.secondSection.title}</h2>,
                    subtitle: <h4 dangerouslySetInnerHTML={{__html : s().biology.secondSection.subtitle}}/>,
                    marks: "30 marc",
                    bottomMarginBetweenDividedQuestions: 32
                },
                10 : {
                    title: <h2>{s().biology.thirdSection.title}</h2>,
                    subtitle: <h4 dangerouslySetInnerHTML={{__html : s().biology.thirdSection.subtitle}}/>,
                    marks: "60 marc",
                    bottomMarginBetweenDividedQuestions: 32
                }
            }
        },
        7 : {
            questions: {
                1 : {
                    title: <h2>{s().history.documentSection.title}</h2>,
                    subtitle: <h4 dangerouslySetInnerHTML={{__html : s().history.documentSection.subtitle}}/>,
                    marks: "100 marc",
                    bottomMarginBetweenDividedQuestions: 12
                },
                2 : {
                    title: <h2>{s().history.irishSection.title}</h2>,
                    subtitle: <h4 dangerouslySetInnerHTML={{__html : s().history.irishSection.subtitle}}/>,
                    marks: "100 marc",
                    bottomMarginBetweenDividedQuestions: 12
                }
            }
        },
        8 : {
            questions: {
                1 : {
                    title: <h2>{s().french.firstSection.title}</h2>,
                    subtitle: <h4>{s().french.firstSection.subtitle}</h4>,
                    marks: "60 marc",
                    bottomMarginBetweenDividedQuestions: 32
                },
                3 : {
                    title: <h2>{s().french.secondSection.title}</h2>,
                    subtitle: <h4>{s().french.secondSection.subtitle}</h4>,
                    marks: "40, 30, 30 & 30 marc faoi seach",
                    bottomMarginBetweenDividedQuestions: 12
                }
            }
        },
        10 : {
            questions: {
                1 : {
                    title: <h2>{s().irish2.firstSection.title}</h2>,
                    subtitle: <h4>{s().irish2.firstSection.subtitle}</h4>,
                    marks: "50 marc",
                    bottomMarginBetweenDividedQuestions: 64
                },
                2 : {
                    title: <h2>{s().irish2.secondSection.title}</h2>,
                    subtitle: <h4>{s().irish2.secondSection.subtitle}</h4>,
                    marks: "30 marc",
                    bottomMarginBetweenDividedQuestions: 32
                },
                3 : {
                    title: <h2>{s().irish2.thirdSection.title}</h2>,
                    subtitle: <h4>{s().irish2.thirdSection.subtitle}</h4>,
                    marks: "30 marc",
                    bottomMarginBetweenDividedQuestions: 32
                },
                4 : {
                    title: <h2>{s().irish2.fourthSection.title}</h2>,
                    subtitle: <h4 dangerouslySetInnerHTML={{__html: s().irish2.fourthSection.subtitle}}></h4>,
                    marks: "30 marc"
                }
            }
        },
        11 : {
            questions: {
                1 : {
                    title: <h2>{s().english2.firstSection.title}</h2>,
                    subtitle: <h4>{s().english2.firstSection.subtitle}</h4>,
                    marks: "60 marc",
                    bottomMarginBetweenDividedQuestions: 12
                },
                2 : {
                    title: <h2>{s().english2.secondSection.title}</h2>,
                    subtitle: <h4>{s().english2.secondSection.subtitle}</h4>,
                    marks: "70 marc",
                    bottomMarginBetweenDividedQuestions: 12
                },
                3 : {
                    title: <h2>{s().english2.thirdSection.unseenPoetry.title}</h2>,
                    subtitle: <h4>{s().english2.thirdSection.unseenPoetry.subtitle}</h4>,
                    marks: "20 marc",
                    bottomMarginBetweenDividedQuestions: null
                },
                4 : {
                    title: <h2>{s().english2.thirdSection.prescribedPoetry.title}</h2>,
                    subtitle: <h4>{s().english2.thirdSection.prescribedPoetry.subtitle}</h4>,
                    marks: "50 marc",
                    bottomMarginBetweenDividedQuestions: null
                }
            }
        }
    };

    constructor(props) {
        super(props, {
            subjects: [],
            render: [],
            renderWidth: 80,

            subjectID: props.match.params.subjectID,
            year: props.match.params.year
        });
    }

    load() {
        const currentURL = window.location.pathname.substring(1);
        const currentURLSplit = currentURL.split("/");
        if (currentURLSplit.length === 2) {
            var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname +'/' + this.state.subjectID + '/' + this.state.year;
            window.history.pushState({path: newurl}, '', newurl);
        }
        let thisExamPapers = this;

        this.request(link("student.ExamPapersREST", this.state.subjectID + "/" + this.state.year), {}).success((data, status) => {
            let imageExamPaper = new Image();
            imageExamPaper.crossOrigin = "anonymous";
            imageExamPaper.onload = function () {
                let thisExamPaper = this;
                let images = [];

                var imageMarkingScheme = new Image();
                imageMarkingScheme.crossOrigin = "anonymous";
                imageMarkingScheme.onload = function () {
                    let thisMarkingScheme = this;

                    const style = thisExamPapers.examPaperStyles[data.examPaper.subjectID];

                    console.time();
                    let lastRoundedQuestionNumber = null;
                    let lastSubQuestion = null;

                    data.examPaper.questions
                        .sort((a, b) => (a.questionNumber > b.questionNumber) ? 1 : -1)
                        .forEach(question => {
                            let dividedQuestionStyle = null;
                            let questionStyle = null;
                            let roundedQuestionNumber = null;
                            let questionNumber = null;

                            if (style != null) {
                                questionNumber = question.questionNumber;
                                dividedQuestionStyle = style.questions[questionNumber];

                                roundedQuestionNumber = parseInt(questionNumber);
                                questionStyle = style.questions[roundedQuestionNumber];

                                const isSplitQuestion = (Number(questionNumber) === questionNumber && questionNumber % 1 !== 0);

                                function handleInterfaceStylesLoad() {
                                    if (questionStyle != null) {
                                        if (questionStyle.title != null) {
                                            images.push({
                                                type: "style",
                                                data: questionStyle.title
                                            });
                                        }
                                        if (questionStyle.subtitle != null) {
                                            images.push({
                                                type: "style",
                                                data: questionStyle.subtitle
                                            });
                                        }
                                        if (questionStyle.marks != null) {
                                            images.push({
                                                type: "style",
                                                data: <h5 style={{
                                                    fontWeight: 600,
                                                    paddingBottom: 10
                                                }}>{s().marksWorth.replace("${marks}", questionStyle.marks)}</h5>
                                            })
                                        }
                                    }
                                }

                                if (questionStyle !== null) {
                                    // Láimhsheáil ceisteanna a bhfuil scoilte
                                    if (isSplitQuestion) {
                                        if (questionNumber.toString().split(".")[1] === "1") {
                                            handleInterfaceStylesLoad()
                                        }
                                    }
                                    // Láimhsheáil ceisteanna nach bhfuil scoilte
                                    else {
                                        handleInterfaceStylesLoad()
                                    }
                                }
                            }

                            let restCanFloatLeft = question.subQuestions.find(e => e.floatSide) != null;

                            let floatLeftSubQuestions = [];
                            let floatRightSubQuestions = [];
                            let subQuestions = [];
                            question.subQuestions
                                .sort((a, b) => (a.startPosition > b.startPosition) ? 1 : -1)
                                .forEach((subQuestion, index) => {
                                    if (!subQuestion.ignored) {

                                        let canvasExamPaper = document.createElement("canvas");
                                        canvasExamPaper.width = data.examPaper.remoteExamPaperWidth;
                                        canvasExamPaper.height = subQuestion.endPosition - subQuestion.startPosition;

                                        let contextExamPaper = canvasExamPaper.getContext("2d");
                                        try {
                                            contextExamPaper.drawImage(thisExamPaper, 0, subQuestion.startPosition, data.examPaper.remoteExamPaperWidth, subQuestion.endPosition - subQuestion.startPosition, 0, 0, data.examPaper.remoteExamPaperWidth, subQuestion.endPosition - subQuestion.startPosition);
                                        } catch (e) {
                                            console.log(e)
                                        }

                                        let examDataPaper = {
                                            examPaperData: canvasExamPaper.toDataURL("image/jpeg"),
                                            markingSchemeVisible: false,
                                            markingSchemesData: []
                                        };

                                        if (restCanFloatLeft) {
                                            if (subQuestion.floatSide) {
                                                examDataPaper["floatLeft"] = true;
                                                floatLeftSubQuestions.push(examDataPaper);
                                            } else {
                                                examDataPaper["floatRight"] = true;
                                                floatRightSubQuestions.push(examDataPaper);
                                            }
                                        } else {
                                            subQuestions.push(examDataPaper);
                                        }

                                        subQuestion.markingSchemes
                                            .sort((a, b) => (a.startPosition > b.startPosition) ? 1 : -1)
                                            .forEach((markingScheme => {
                                                let markingSchemeHeight = markingScheme.endPosition - markingScheme.startPosition;
                                                let markingSchemeWidth = data.examPaper.remoteMarkingSchemeWidth;

                                                let canvasMarkingScheme = document.createElement("canvas");
                                                canvasMarkingScheme.width = data.examPaper.remoteMarkingSchemeWidth;
                                                canvasMarkingScheme.height = markingSchemeHeight;

                                                let contextMarkingScheme = canvasMarkingScheme.getContext("2d");

                                                try {
                                                    contextMarkingScheme.drawImage(thisMarkingScheme, 0,
                                                        markingScheme.startPosition, markingSchemeWidth, markingSchemeHeight,
                                                        0, 0,
                                                        markingSchemeWidth, markingSchemeHeight);
                                                } catch (e) {
                                                    console.log(e)
                                                }
                                                examDataPaper.markingSchemesData.push(contextMarkingScheme.canvas.toDataURL("image/jpeg"));
                                            }));

                                        examDataPaper["number"] = questionNumber;
                                        if (question.subQuestions.length - 1 === index) {
                                            examDataPaper["endOfQuestion"] = true;
                                        }
                                        lastSubQuestion = examDataPaper;
                                    } else {
                                        lastSubQuestion = null;
                                    }
                                });

                            let renderQuestion = {
                                type: "examSegment",
                                subQuestions: subQuestions,
                                floatLeftSubQuestions: floatLeftSubQuestions,
                                floatRightSubQuestions: floatRightSubQuestions
                            };

                            if (questionStyle != null) {
                                renderQuestion["marginTop"] = questionStyle.topMarginBetweenDividedQuestions;
                                renderQuestion["marginBottom"] = questionStyle.bottomMarginBetweenDividedQuestions;
                            } else {
                                renderQuestion["marginBottom"] = 32
                            }

                            images.push(renderQuestion)
                        });
                    console.timeEnd();

                    thisExamPapers.setState({render: images});

                    /*
                     for(var i = 0; i < images.length; i++) {
                         let renderElement = document.getElementById("render");
                         renderElement.append("<img style='width: 100%' src='"+images[i].examPaperData+"' />");
                         if (images[i].markingSchemeData != null) {
                             renderElement.append("<img style='width: 100%' src='" + images[i].markingSchemeData + "' />");
                         }
                     }
                     */
                };

                // imageMarkingScheme.src = resource("examResources/MS2018500.png");
                imageMarkingScheme.src = data.examPaper.remoteMarkingSchemeReference;
            };

            // imageExamPaper.src = resource("examResources/EP2018500.png");
            imageExamPaper.src = data.examPaper.remoteExamPaperReference;
        }).performRequest();
    }

    componentDidMount() {
        this.request(link("student.CommonREST","subjects")).success(data => this.setState({subjects: data.subjects})).performRequest();
        if (this.props.match.params.subjectID != null && this.props.match.params.year != null) {
            this.load();
        }
    }

    handleSubjectChange = (event) => {
        this.setState({subjectID: event.target.value}, () => {
            if (this.state.subjectID != null && this.state.year) {
                this.load()
            }
        });
    };

    handleYearChange = (event) => {
        this.setState({year: event.target.value}, () => {
            if (this.state.subjectID != null && this.state.year) {
                this.load()
            }
        });
    };

    subQuestionMarkingSchemeVisibility = (questionIndex, subQuestionIndex, visible) => {
        const state = {...this.state};
        const data = state.render;

        data[questionIndex].subQuestions[subQuestionIndex].markingSchemeVisible = visible;

        this.setState({state});

    };

    floatLeftSubQuestionMarkingSchemeVisibility = (questionIndex, subQuestionIndex, visible) => {
        const state = {...this.state};
        const data = state.render;

        data[questionIndex].floatLeftSubQuestions[subQuestionIndex].markingSchemeVisible = visible;

        this.setState({state});

    };

    floatRightSubQuestionMarkingSchemeVisibility = (questionIndex, subQuestionIndex, visible) => {
        const state = {...this.state};
        const data = state.render;

        data[questionIndex].floatRightSubQuestions[subQuestionIndex].markingSchemeVisible = visible;

        this.setState({state});

    };

    zoomValueText = (value) => {
        return value;
    };

    zoomValueChange = (_,value) => {
        console.log(value);
        this.setState({renderWidth: value});
    };

    render() {
        const {classes} = this.props;

        return (
            <div className={"examPapers-body"}>
                <div className={"container-fluid"}>
                    <div className={"row"}>
                        <div className={"col-md-2 examPapers-selectors"}>
                            <div className={"row"}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>{s().subject}</InputLabel>
                                    <Select
                                        value={this.state.subjectID}
                                        onChange={this.handleSubjectChange}>
                                        {this.state.subjects.map(subject => <MenuItem value={subject.ID}>{subject.titleGA}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            </div>
                            <div className={"row"}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>{s().year}</InputLabel>
                                    <Select
                                        fullWidth
                                        value={this.state.year}
                                        onChange={this.handleYearChange}>
                                        {getAllAvailableYears().map(year => <MenuItem value={year}>{year}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            </div>
                            <div className={"row"}>
                                <h4 className={"student-examPapers-settingTitle"}>{s().zoom}</h4>
                                <Slider
                                    defaultValue={this.state.renderWidth}
                                    getAriaValueText={this.zoomValueText}
                                    aria-labelledby="discrete-slider"
                                    valueLabelDisplay="auto"
                                    step={5}
                                    min={65}
                                    max={100}
                                    onChange={this.zoomValueChange}
                                />
                            </div>
                            <div className={"row student-examPapers-paperOptions"}>
                                <h3>{s().prepareTitle}</h3>
                                <h6>{s().prepareDescription}</h6>
                                <Button>{s().prepareButton}</Button>
                            </div>
                        </div>
                        <div id={"paper-width"} className={"col-md-10"}>
                            <div id='student-examPapers-render'>
                                {this.state.render.map((renderObj,renderIndex) => {
                                    if (renderObj.type === "style") {
                                        return renderObj.data;
                                    } else {
                                        if (renderObj.subQuestions.length !== 0) {
                                            return <div style={{marginTop: renderObj.marginTop, marginBottom: renderObj.marginBottom}}>{renderObj.subQuestions.map((image,imageIndex) =>
                                                <div
                                                     className={"examPapers-parentExamSplit " +
                                                     ((image.endOfQuestion === true) ? "examPapers-endOfQuestion " : "")}>
                                                        <div className={"examPapers-examSplit"} style={{
                                                            marginBottom: image.marginBottom, marginTop: image.marginTop,
                                                            marginLeft: "auto", marginRight: "auto",
                                                            width: this.state.renderWidth + "%"
                                                        }}>
                                                            <img className={"examPapers-general-image"}
                                                                 src={image.examPaperData}/>
                                                            <div className={"examPapers-examSplit-options"}>
                                                                <ButtonGroup variant="contained" size="small">
                                                                    {(image.markingSchemesData.length !== 0) ? <Button
                                                                        style={{fontSize: (1 - (100 - this.state.renderWidth) * .01) + "vw"}}
                                                                        onClick={() => this.subQuestionMarkingSchemeVisibility(renderIndex, imageIndex, !image.markingSchemeVisible)}>
                                                                        {(image.markingSchemeVisible) ? s().hideMarkingScheme : s().showMarkingScheme}
                                                                    </Button> : null}
                                                                </ButtonGroup>
                                                            </div>
                                                        </div>
                                                    <AnimateHeight
                                                        duration={500}
                                                        height={image.markingSchemeVisible ? 'auto' : 0}>
                                                            {(image.markingSchemesData.length !== 0) ?
                                                                image.markingSchemesData.map(markingSchemeData => {
                                                                    return <div className={"examPapers-examSplit"} style={{marginLeft: "auto", marginRight: "auto", width: this.state.renderWidth + "%"}}>
                                                                        <img className={"examPapers-general-image"}
                                                                             src={markingSchemeData}/>
                                                                    </div>
                                                                }) : null}
                                                    </AnimateHeight>
                                                </div>
                                            )}</div>
                                        } else {
                                            return <div style={{overflowX: "auto"}}><div style={{
                                                display: "flex",
                                                marginTop: renderObj.marginTop, marginBottom: renderObj.marginBottom,
                                                marginLeft: "auto", marginRight: "auto",
                                                minWidth: 1107,
                                                width: this.state.renderWidth + "%"}}>
                                            <div style={{float: "left", width: "48%"}}>
                                                {renderObj.floatLeftSubQuestions.map((image,imageIndex) =>
                                                    <div
                                                         className={"examPapers-parentExamSplit " +
                                                         ((image.endOfQuestion === true) ? "examPapers-endOfQuestion " : "")}>
                                                        <div className={"examPapers-examSplit"} style={{
                                                            marginBottom: image.marginBottom, marginTop: image.marginTop
                                                        }}><img className={"examPapers-general-image"}
                                                                 src={image.examPaperData}/>
                                                            <div className={"examPapers-examSplit-options"}>
                                                                <ButtonGroup variant="contained" size="small">
                                                                    {(image.markingSchemesData.length !== 0) ? <Button
                                                                        style={{fontSize: "calc(5px + " + (this.state.renderWidth * .002) + "vw)"}}
                                                                        onClick={() => this.floatLeftSubQuestionMarkingSchemeVisibility(renderIndex, imageIndex, !image.markingSchemeVisible)}>
                                                                        {(image.markingSchemeVisible) ? s().hideMarkingScheme : s().showMarkingScheme}
                                                                    </Button> : null}
                                                                </ButtonGroup>
                                                            </div>
                                                        </div>
                                                        <AnimateHeight
                                                            duration={500}
                                                            height={image.markingSchemeVisible ? 'auto' : 0}>
                                                            {(image.markingSchemesData.length !== 0) ?
                                                                image.markingSchemesData.map(markingSchemeData => {
                                                                    return <div className={"examPapers-examSplit"}>
                                                                        <img className={"examPapers-general-image"}
                                                                             src={markingSchemeData}/>
                                                                    </div>
                                                                }) : null}
                                                        </AnimateHeight>
                                                    </div>)}
                                                    </div>
                                                <div style={{float: "right", width: "48%"}}>
                                                {renderObj.floatRightSubQuestions.map((image,imageIndex) =>
                                                    <div
                                                         className={"examPapers-parentExamSplit " +
                                                         ((image.endOfQuestion === true) ? "examPapers-endOfQuestion " : "")}>
                                                        <div className={"examPapers-examSplit"} style={{
                                                            marginBottom: image.marginBottom,
                                                            marginTop: image.marginTop,
                                                            minWidth: 285
                                                        }}><img className={"examPapers-general-image"}
                                                                 src={image.examPaperData}/>
                                                            <div className={"examPapers-examSplit-options"}>
                                                                <ButtonGroup variant="contained" size="small">
                                                                    {(image.markingSchemesData.length !== 0) ? <Button
                                                                        style={{fontSize: "calc(5px + " + (this.state.renderWidth * .002) + "vw)"}}
                                                                        onClick={() => this.floatRightSubQuestionMarkingSchemeVisibility(renderIndex, imageIndex, !image.markingSchemeVisible)}>
                                                                        {(image.markingSchemeVisible) ? s().hideMarkingScheme : s().showMarkingScheme}
                                                                    </Button> : null}
                                                                </ButtonGroup>
                                                            </div>
                                                        </div>

                                                        <AnimateHeight
                                                            duration={500}
                                                            height={image.markingSchemeVisible ? 'auto' : 0}>
                                                            {(image.markingSchemesData.length !== 0) ?
                                                                image.markingSchemesData.map(markingSchemeData => {
                                                                    return <div className={"examPapers-examSplit"}>
                                                                        <img className={"examPapers-general-image"}
                                                                             src={markingSchemeData}/>
                                                                    </div>
                                                                }) : null}
                                                        </AnimateHeight>
                                                    </div>)}
                                                </div>
                                            </div></div>
                                        }
                                    }
                                })}
                            </div>
                            {/*<div id='render'></div>*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
ExamPapers.package = "student.ExamPapers";

export default withStyles(styles)(ExamPapers);