import React, {Component} from "react";
import SessionFrame from "trinreact/layouts/SessionFrame";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import {withStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from "prop-types"
import Fab from '@material-ui/core/Fab';
import Slider from '@material-ui/core/Slider';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import Container from '@material-ui/core/Container';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TableContainer from '@material-ui/core/TableContainer';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import ImageIcon from '@material-ui/icons/Image';
import TitleIcon from '@material-ui/icons/Title';
import AddIcon from '@material-ui/icons/Add';

import Button from '../../uiComponents/Button';

import "./Notes.css";
import {s} from "../../App";
import NoteRender from "../../uiComponents/NoteRender";

class Notes extends SessionFrame {

    constructor(props) {
        super(props, {
            didLoadMetaDataSubjects: false
        });
    }

    componentDidMount() {
        this.request(link("adminStudent.CommonREST","staticData"), {subjects: true, topics: true})
            .success(data => this.setState({subjects: data.subjects, topics: data.topics, didMetaDataSubjects: true})).performRequest();
        if (this.props.match.params.subjectID != null && this.props.match.params.year != null) {
            // this.load();
        }
        this.load();
    }

    load() {
        this.request(link("student.NotesREST","1/1"))
            .success(data => {
                this.setState({noteData: data.note, didMetaDataSubjects: true})
            }).performRequest();
    }

    render() {
        return (
            <div>
                {(this.state.didMetaDataSubjects) ? <NoteRender
                    /*
                    subjectID={this.props.match.params.subjectID}
                    topicID={this.props.match.params.subjectID}
                    */
                    subjectID={1}
                    topicID={1}

                    subjects={this.state.subjects}
                    topics={this.state.topics}


                    noteDataRequest={(subjectID, topicID, versionNumber, complete) => {
                        this.request(link("student.NotesREST","1/1"))
                            .success(data => {
                                complete(data.note);
                                this.setState({noteData: data.note})
                            }).performRequest();
                    }}
                /> : null}
            </div>
        )
    };

}
Notes.package = "student.Notes";

export default Notes;