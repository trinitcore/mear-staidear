import React from "react";
import SessionFrame from "trinreact/layouts/SessionFrame"
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {s} from "../../App"

import "./Dashboard.css";
import {Link} from "react-router-dom";

const styles = {
    resourceCard: {
        maxWidth: 345,
        textAlign: "center",
        margin: "auto"
    },
    actionArea: {
        paddingTop: 32
    },

    notificationCard: {
        paddingLeft: 16,
        paddingRight: 16
    },
    notificationCardTitle: {
        marginBottom: 2
    }
};

class Dashboard extends SessionFrame {

    constructor(props) {
        super(props, {});
    }

    resourceCard = (title, imageResource, url) => {
        const { classes } = this.props;
        return <Link to={url}><Card className={classes.resourceCard}>
            <CardActionArea className={classes.actionArea}>
                <img
                    height={64}
                    src={resource(imageResource)}
                />
                <CardContent>
                    <Typography gutterBottom variant="h6" component="h2">
                        {title}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card></Link>
    };

    render() {
        const { classes } = this.props;

        return (
            <div className={"dashboard-body"}>
                <div className={"container"}>
                    <div className={"row dashboard-intro"}>
                        <div className={"row"} style={{width: "95%", margin: "auto"}}>
                            <h1>{s().welcome} Chormaic</h1>
                            <h5 style={{marginBottom: 16}}>{s().welcomePrompt}</h5>
                        </div>
                        <div className={"row dashboard-notification"} style={{width: "100%", margin: "auto", marginBottom: 8}}>
                            <div className={"col-xs-12 col-md-4"}>
                                <Card className={classes.notificationCard}>
                                    <div className={"row"}>
                                        <div className={"col-md-3"} style={{position: "absolute", height: "100%"}}>
                                            <img
                                                style={{padding: "7%", height: "49px", top: "6px", position: "relative"}}
                                                src={resource("sample_logo.png")}
                                            />
                                        </div>
                                        <div className={"col-xs-9"} style={{marginLeft: "19%"}}>
                                            <h4 className={classes.notificationCardTitle}>Obair Bhaile</h4>
                                            <p>Fuair tú obair bhaile ó do mhúinteoir fisice</p>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                        </div>
                        <div className={"row dashboard-notification"} style={{width: "100%", margin: "auto"}}>
                            <div className={"col-xs-12 col-md-4"}>
                                <Card className={classes.notificationCard}>
                                    <div className={"row"}>
                                        <div className={"col-md-3"} style={{position: "absolute", height: "100%"}}>
                                            <img
                                                style={{padding: "7%", height: "49px", top: "6px", position: "relative"}}
                                                src={resource("sample_logo.png")}
                                            />
                                        </div>
                                        <div className={"col-xs-9"} style={{marginLeft: "19%"}}>
                                            <h4 className={classes.notificationCardTitle}>Cur i gCuimhne</h4>
                                            <p>Ceist amháin mata a dhéanamh</p>
                                        </div>
                                    </div>
                                </Card>
                            </div>
                        </div>
                    </div>

                    <div className={"row dashboard-options"}>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard(s().classes,"sample_logo.png","")}
                        </div>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard(s().practiceQuestions,"sample_logo.png","")}
                        </div>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard(s().examPapers,"sample_logo.png","examPapers")}
                        </div>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard(s().notes,"sample_logo.png","notes")}
                        </div>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard(s().discussions,"sample_logo.png","")}
                        </div>
                        <div className={"col-xs-6 col-md-2"}>
                            {this.resourceCard(s().settings,"sample_logo.png","")}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
Dashboard.package = "student.Dashboard";

export default withStyles(styles)(Dashboard);