import React, {Component} from "react";
import {s,setLanguage} from "../../App"
import {cookies,setCookie,removeCookie} from "trinreact"
import "./Index.css";
import Button from "../../uiComponents/Button";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { CSSTransition } from "react-transition-group";
import Fab from '@material-ui/core/Fab';
import ExpandMore from '@material-ui/icons/ExpandMore';

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            askForLanguageAndPerson: (cookies().get("lang") == null),
            askForLanguageAndPersonIsShown: (cookies().get("lang") == null),
            speakerType:null
        };
        if (this.state.askForLanguageAndPerson) {
            this.props.appController.navigationBarVisible(false);
        } else {

        }
    }

    componentDidMount() {
        $(function() {
            $.scrollify({
                section : ".section-wrapper",
                easing: "easeOutExpo",
                scrollSpeed: 1100,
                offset : 0,
                scrollbars: true,
                standardScrollElements: "",
                setHeights: false,
                overflowScroll: true,
                updateHash: true,
                touchScroll:true,
                before:function() {},
                after:function() {},
                afterResize:function() {},
                afterRender:function() {}
            });
            $.scrollify.enable();
        })
    }

    componentWillUnmount() {
        $(function() {
            $.scrollify().disable();
        });
    }

    handleChange = (event) => {
        this.setState({speakerType: event.target.value});
    };

    setupIndexPage = (lang) => {
        setTimeout(() => {
            this.setState({askForLanguageAndPersonIsShown: false})
        }, 200);
        setLanguage(lang); this.setState({askForLanguageAndPerson:false}, () => {
            this.props.appController.navigationBarVisible(true);
        });
    };

    render() {
        const section1ButtonStyle = {
            marginRight: 8
        };

        const sectionSiteTitle = {
            fontWeight: 400
        };

        return (
            <div>
                {this.state.askForLanguageAndPersonIsShown ? <CSSTransition
                    classNames="fade-out"
                    in={!this.state.askForLanguageAndPerson}
                    timeout={500}
                >
                     <div className={"index-question-body"}>
                        <div className={"row"} style={{minHeight: 358}}>
                            <div>
                                <img className={"index-question-logo"} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                            </div>
                            <h1 style={{fontWeight: "600"}}>Cé thú féin?</h1>
                            <h3>Who are you?</h3>

                            <div className={"row index-question-buttons-wrapper"}>
                                <FormControl component="fieldset">
                                    <RadioGroup
                                        onChange={this.handleChange}
                                        aria-label="Gender"
                                        name="gender1">
                                        <div className={"row"}>
                                            <div className={"col-md-12"}><FormControlLabel style={{fontSize:"12px"}} value="ga" control={<Radio />} label="Is Gaeilgeoir mé" /></div>
                                            <div className={"col-md-12"}><FormControlLabel value="en" control={<Radio />} label="I'm an English Speaker" /></div>
                                        </div>
                                    </RadioGroup>
                                </FormControl>
                            </div>

                            {(this.state.speakerType == null) ? null :
                                <div className={"row index-question-buttons-wrapper"}>
                                    <div className={"col-md-6"}><Button onClick={() => {this.setupIndexPage(this.state.speakerType)}} label={(this.state.speakerType == "ga") ? "Atá i mo mhúinteoir" : "Whom is a teacher"}/></div>
                                    <div className={"col-md-6"}><Button onClick={() => {this.setupIndexPage(this.state.speakerType)}} label={(this.state.speakerType == "ga") ? "Atá i mo mhac léinn" : "Whom is a teacher"} color={"secondary"}/>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </CSSTransition> : null}
                <div className={"index-main-body"}>
                        <section className={"section-wrapper section index-main-section-1"}>
                            <div className={"container"}>
                                <div className={"row"}>
                                    <div className={"col-xs-12 col-md-5 index-main-section-1-logoHolder"}>
                                        <img className={"index-main-section-1-logo"} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                    </div>
                                    <div className={"col-xs-12 col-md-7"}>
                                        <h1><span className={"standardColor"} style={sectionSiteTitle}>{s().firstSection.title.s1}</span><br/>{s().firstSection.title.s2}</h1>
                                        <div className={"row index-main-section-1-buttons"}>
                                            <Button style={section1ButtonStyle} color="primary" label={s().firstSection.signIn}/><Button color="secondary" label={s().firstSection.register}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Fab style={{position: "absolute", bottom: 25}} variant="extended" aria-label="Delete" color={"primary"}>
                                <ExpandMore />
                                <div style={{marginLeft:8}}>{s().firstSection.findOutMore}</div>
                            </Fab>
                        </section>

                    <section className={"section-wrapper index-main-section-2 index-main-section-colored"}>
                        <div className={"section standardBackgroundColor"}>
                            <div className={"container"}>
                                <div className={"row"}>
                                    <div className={"col-md-6"}>
                                        <h1 style={sectionSiteTitle}>{s().secondSection.title.s1}</h1>
                                        <h3>{s().secondSection.title.s2}<br/>{s().secondSection.title.s3}<br/>{s().secondSection.title.s4}</h3>
                                        <p>{s().secondSection.description}</p>
                                    </div>
                                    <div className={"col-xs-12 col-md-5 index-main-section-2-logoHolder"}>
                                        <img className={"index-main-section-2-logo"} src={resource("sample_logo_inverse.png")} style={{marginBottom:8}}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                        <section className={"section-wrapper section index-main-section-3"}>
                            <div className={"container"}>
                                <div className={"row"}>

                                </div>
                                <div className={"row"}>
                                        <div className={"row"} style={{margin: "auto", width: "85%"}}>
                                            <div className={"col-xs-6 col-md-2"}>
                                                <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                                <h4>{s().thirdSection.subjects.appliedMaths}</h4>
                                            </div>
                                            <div className={"col-xs-6 col-md-2"} style={{height:150}}>
                                                <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                                <h4>{s().thirdSection.subjects.maths}</h4>
                                            </div>
                                            <div className={"col-xs-6 col-md-2"}>
                                                <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                                <h4>{s().thirdSection.subjects.irish}</h4>
                                            </div>
                                            <div className={"col-xs-6 col-md-2"}>
                                                <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                                <h4>{s().thirdSection.subjects.physics}</h4>
                                            </div>
                                            <div className={"col-xs-6 col-md-2"}>
                                                <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                                <h4>{s().thirdSection.subjects.physics}</h4>
                                            </div>
                                            <div className={"col-xs-6 col-md-2"}>
                                                <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                                <h4>{s().thirdSection.subjects.physics}</h4>
                                            </div>
                                        </div>
                                        <h1><span className={"standardColor"} style={sectionSiteTitle}>{s().thirdSection.title.s1}</span><br/></h1>
                                        <h4>{s().thirdSection.title.s2}</h4>
                                        <p>
                                            {s().thirdSection.description.s1}<br/>
                                            {s().thirdSection.description.s2}
                                        </p>
                                    <div className={"col-md-5"}></div>
                                </div>
                            </div>
                        </section>

                    <section className={"section-wrapper index-main-section-4 index-main-section-colored"}>
                        <div className={"section standardBackgroundColor"}>
                            <div className={"container"}>
                                <div className={"row"}>
                                    <div className={"col-xs-12 col-md-5 index-main-section-4-logoHolder"}>
                                        <img className={"index-main-section-4-logo"} src={resource("sample_logo_inverse.png")} style={{marginBottom:8}}/>
                                    </div>
                                    <div className={"col-md-6"}>
                                        <h1 style={sectionSiteTitle}>{s().fourthSection.title}</h1>
                                        <p>{s().fourthSection.description}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section className={"section-wrapper index-main-section-5"}>
                        <div className={"section"}>
                            <div className={"container"}>
                                <div className={"index-main-section-5-subtitles"}>
                                    <div className={"row"} style={{marginBottom: 36}}>
                                        <img className={"index-main-section-1-logo"} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                        <h1>{s().fifthSection.title}</h1>
                                    </div>
                                    <div className={"row"}>
                                        <div className={"col-md-6"}>
                                            <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                            <h3>{s().fifthSection.discussionPossibilities.students.title}</h3>
                                            <p>{s().fifthSection.discussionPossibilities.students.description}</p>
                                        </div>
                                        <div className={"col-md-6"}>
                                            <img width={82} src={resource("sample_logo.png")} style={{marginBottom:8}}/>
                                            <h3>{s().fifthSection.discussionPossibilities.teachers.title}</h3>
                                            <p>{s().fifthSection.discussionPossibilities.teachers.description}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p style={{fontSize: 10, marginLeft:8, marginRight: 8}}><span style={{fontWeight:800}}>{s().fifthSection.notice.s1}:</span> {s().fifthSection.notice.s2}</p>
                    </section>
                </div>
            </div>
        )
    }
}
Index.package = "public.Index";

export default (Index);