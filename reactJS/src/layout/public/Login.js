import React from "react";
import {LoginFrame} from "trinreact"
import {Form} from "trinreact"
import TextInput from "../../uiComponents/TextInput";
import FormInterface from "../../uiComponents/FormInterface";
import Button from "../../uiComponents/Button";

import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles, useTheme} from '@material-ui/styles';
import Container from '@material-ui/core/Container';
import {s} from "../../App";

import "./Login.css";

const styles = {
    '@global': {
        body: {
            backgroundColor: "white",
        },
    },
    paper: {
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: 1,
        backgroundColor: "pink",
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: 1,
    },
    submit: {
        margin: "3px,0px,2px,0px"
    },
};

class Login extends LoginFrame {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        super.componentDidMount();
        $.scrollify.disable();
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={"login-body section"}>
                <Container component="main" maxWidth="xs">
                    <CssBaseline />
                    <div className={classes.paper}>
                        <h1>
                            {s().signIn}
                        </h1>
                        <Form onSubmit={this.formSubmission}>
                            <TextInput
                                label={s().username}
                                name="email"
                                autoComplete="email"
                                autoFocus={true}
                                required={true}
                                fullWidth={true}
                                type="email"
                            />
                            <TextInput
                                required
                                fullWidth
                                name="password"
                                label={s().password}
                                type="password"
                                autoComplete="current-password"
                            />
                            {/*
                                <FormControlLabel
                                    control={<Checkbox value="remember" color="primary"/>}
                                    label={s().rememberMe}
                                />
                            */}
                            <div style={{height:10}}></div>
                            <FormInterface>
                                <Button
                                    submit={true}
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                    label={s().signIn}
                                />
                                <div style={{marginTop:16, textAlign: "center"}}>
                                    <div className={"row"}>
                                            <Link href="#" variant="body2">
                                                {s().forgotPassword}
                                            </Link>
                                    </div>

                                    <div className={"row"}>
                                            <Link href="#" variant="body2">
                                                {s().signUp}
                                            </Link>
                                    </div>
                                </div>
                            </FormInterface>
                        </Form>
                    </div>
                    <Box mt={5}>
                    </Box>
                </Container>
            </div>
        );

        /*
        return (
            <div>
                <Form onSubmit={this.formSubmission}>
                    <TextInput label={"Email"} name={"email"}/>
                    <TextInput label={"Password"} name={"password"}/>
                    <FormInterface>
                        <Button label={"Login"} submit={true}/>
                    </FormInterface>
                </Form>
            </div>
        )
        */
    }

}
Login.package = "public.Login";

export default withStyles(styles)(Login);