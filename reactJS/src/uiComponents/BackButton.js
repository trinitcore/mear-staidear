import React, {Component} from "react";
import Link from "react-router-dom/es/Link";

class BackButton extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <Link to={window.location.href.endsWith("/") ? "../" : "./"}>Back</Link>
            </div>
        )
    }

}

export default BackButton;