import React from "react";
import {TrinButton} from "trinreact"
import PropTypes from "prop-types"
import MaterialButton from '@material-ui/core/Button';

class Button extends TrinButton {

    constructor(props) {
        super(props)
    }

    render() {
        return (
                <MaterialButton fullWidth={this.props.fullWidth} variant={this.props.variant} color={this.props.color} style={Object.assign({fontSize:12},this.props.style)} onClick={this.onClick}>
                    {this.props.label}
                </MaterialButton>
        )
    }

}

Button.defaultProps = {
    variant: "contained",
    fullWidth: false,
    color: "primary",

    additionalParameters: {},
    submit: false,
    clearAll: false,
    onClick: () => {}
};

Button.propTypes = {
    variant: PropTypes.string,
    fullWidth: PropTypes.bool,

    label: PropTypes.string.isRequired,
    additionalParameters: PropTypes.object,
    alternativeURL: PropTypes.string,
    submit: PropTypes.bool,
    clearAll: PropTypes.bool,
    onClick: PropTypes.func
};

export default Button;