import React from "react";
import {TrinTextInput} from "trinreact";
import PropTypes from "prop-types";
import {s} from "../App";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

class SelectField extends TrinTextInput {

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentDidMount() {

    }

    render() {
        return (
            <FormControl fullWidth={true}>
                <InputLabel htmlFor="age-simple">{this.props.label}</InputLabel>
                <Select
                    value={this.state.value}
                    onChange={(e) => {
                        let ID = e.target.value;
                        let value = this.props.values.find(x => x.ID == ID);
                        // this.setValue(value.ID);
                        if (this.props.valueSelected != null) {
                            this.props.valueSelected(value)
                        }
                        this.setState({value: ID});
                    }}>
                    {this.props.values.map((value) => {
                        return <MenuItem value={value.ID}>{value.label}</MenuItem>
                        // return <option value={value.ID}>{value.label}</option>
                    })}
                </Select>
            </FormControl>
            /*
            <label>
                {this.props.label}:
                <select ref={"selectBox"} onChange={(e) => {
                    let ID = e.target.value;
                    let value = this.props.values.find(x => x.ID == ID);
                    this.setValue(value.ID);
                    if (this.props.valueSelected != null) {
                        this.props.valueSelected(value)
                    }
                }}>
                    {this.props.values.map((value) => {
                        return <option value={value.ID}>{value.label}</option>
                    })}
                </select>
            </label>
            */
        )
    }

}

SelectField.defaultProps = {
    name: null,
    valueSelected: null,
    value: null,
    optional: false,
    numeric: true
};
SelectField.propTypes = {
    values: PropTypes.any.isRequired,
    name: PropTypes.string,
    label: PropTypes.string.isRequired,
    valueSelected: PropTypes.func,
    value: PropTypes.any,
    optional: PropTypes.boolean
};
export default SelectField;