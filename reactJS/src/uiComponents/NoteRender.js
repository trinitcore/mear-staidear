import React, {Component} from "react";
import PropTypes from "prop-types";
import FormInterface from "./FormInterface";
import Typography from "@material-ui/core/Typography";
import {componentS} from "../App";
import Slider from "@material-ui/core/Slider";
import Fab from "@material-ui/core/Fab";
import DeleteIcon from "@material-ui/icons/Delete";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import TextFieldsIcon from "@material-ui/icons/TextFields";
import ImageIcon from "@material-ui/icons/Image";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import TitleIcon from "@material-ui/icons/Title";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import AddIcon from "@material-ui/icons/Add";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Button from "./Button";
import Box from "@material-ui/core/Box";
import {withStyles} from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import "./NoteRender.css";

function s() {
    return componentS("uiComponents.NoteRender")
}

const styles = {
    formControl: {
        width: "100%",
    },
    tabWidth: {
        minWidth: "33%"
    }
};

class TabPanel extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {children, value, index, ...other} = this.props;

        return (
            <Typography
                component="div"
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}>
                <Box p={3} style={{padding: 0}}>{children}</Box>
            </Typography>
        );
    }
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

class NoteRender extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tabPosition: 0,

            subjectID: props.subjectID,
            topicID: props.topicID,
            year: props.year,

            noteData: null,
            versionSelectedNoteData: null,

            versionNumber: null,

            widthBeingChanged: false,
            elementIndexOfWidthBeingChanged: null
        };

        /* Séard atá ann
        {
            elementID: **,
        }
         */
        this.deletedElements = [];

        /* Séard atá ann
        {
            order: **,
            width: **,

            paragraphText: {
                title: **,
                data: **
            },
            image: {
                URL: **,
                subtitle: **
            },

            // Modified table
            table: {
                addition: true, OR modification: true,

                metaColumns: [
                    // Addition meta column
                    {
                        modification: true,

                        title: **,
                        order: **,

                        elementID: **
                    }

                    // Modified meta column
                    {
                        modification: true,

                        title: **,
                        order: **,

                        elementID: **
                    }

                    // Deleted meta column
                    {
                        deletion: true,

                        elementID: **
                    }
                ],
                rows: [
                    // Modified row
                    {
                        modification: true

                        order: **,
                        columns: [
                            // Modified column
                            {
                                modification: true,

                                paragraphText: {
                                    title: **,
                                    data: **
                                },
                                image: {
                                    URL: **,
                                    subtitle: **
                                },

                                elementID: **
                            }

                            // Deleted column
                            {
                                deletion: true,

                                elementID: **
                            }
                        ]
                    }

                    // Addition row
                    {
                        addition: true

                        order: **,
                        columns: [
                            {
                                addition: true,

                                paragraphText: {
                                    title: **,
                                    data: **
                                },
                                image: {
                                    URL: **,
                                    subtitle: **
                                }
                            }
                        ]
                    }

                    // Deleted meta column
                    {
                        deletion: true,

                        elementID: **
                    }
                ]
            }
        } */
        this.modifiedElements = [];

        /* Séard atá ann
        {
            order: **,
            width: **,

            paragraphText: {
                title: **,
                data: **
            },
            image: {
                URL: **,
                subtitle: **
            },

            // New table
            table: {
                metaColumns: [
                    {
                        addition: true,

                        title: **,
                        order: **
                    }
                ],
                rows: [
                    {
                        addition: true,

                        order: **,
                        columns: [
                            {
                                addition: true,

                                paragraphText: {
                                    title: **,
                                    data: **
                                },
                                image: {
                                    URL: **,
                                    subtitle: **
                                }
                            }
                        ]
                    }
               ],
                title: **,
                caption: **
            }
        } */
        this.newElements = [];
    }

    componentDidMount() {
        if (this.props.subjectID != null && this.props.topicID != null != null) {
            this.props.noteDataRequest(this.props.subjectID, this.props.topicID, null,
                (noteData) => {
                    this.originalNoteData = JSON.parse(JSON.stringify(noteData));
                    this.setState({noteData: noteData})
                });
        }
    }

    findElementNotation = (array, element) => {
        return array.find(e => e.uniqueUIKey === ((element.newElementKey != null) ? element.newElementKey : element.modifiedElementKey) && e.deletion !== true);
    };

    // Iondúil
    deleteNotation = (elementID) => {
        return {
            deletion: true,

            elementID: elementID
        }
    };

    paragraphTextNotation = (title, data) => {
        let notation = {};
        if (title != null) notation.title = title;
        if (data != null) notation.data = data;

        return notation;
    };

    imageNotation = (URL, subtitle) => {
        let notation = {};
        if (URL != null) notation.URL = URL;
        if (subtitle != null) notation.subtitle = subtitle;

        return notation;
    };

    registeredUIKeys = [];
    generateUniqueUIKey = () => {
        const randomKey = Math.random();
        return this.registeredUIKeys.includes(randomKey) ? this.generateUniqueUIKey() : randomKey;
    };

    // Table
    // Table -> Meta Columns
    metaColumnNotation = (uniqueUIKey, title, order) => {
        let notation = {};
        if (title != null) notation.title = title;
        if (order != null) notation.order = order;
        notation.uniqueUIKey = uniqueUIKey;

        return notation;
    };

    newMetaColumnNotation = (uniqueUIKey, title, order) => {
        let notation = this.metaColumnNotation(uniqueUIKey, title, order);
        notation.addition = true;

        return notation;
    };

    modifiedMetaColumnNotation = (uniqueUIKey, elementID, title, order) => {
        let notation = this.metaColumnNotation(uniqueUIKey, title, order);
        notation.modification = true;
        notation.elementID = elementID;

        return notation;
    };

    // Table -> Row Notation
    rowNotation = (uniqueUIKey, columns, order) => {
        let notation = {};
        if (columns != null) notation.columns = columns;
        if (order != null) notation.order = order;
        notation.uniqueUIKey = uniqueUIKey;

        return notation;
    };

    newRowNotation = (uniqueUIKey, columns, order) => {
        let notation = this.rowNotation(uniqueUIKey, columns, order);
        notation.addition = true;

        return notation;
    };

    modifiedRowNotation = (uniqueUIKey, elementID, columns, order) => {
        let notation = this.rowNotation(uniqueUIKey, columns, order);
        notation.modification = true;
        notation.elementID = elementID;

        return notation;
    };

    // Table -> Column Notation
    columnNotation = (uniqueUIKey, paragraphText, image, order) => {
        let notation = {};
        if (paragraphText != null) notation.paragraphText = paragraphText;
        if (image != null) notation.image = image;
        if (order != null) notation.order = order;
        notation.uniqueUIKey = uniqueUIKey;

        return notation;
    };

    deleteElementColumnNotation = (uniqueUIKey, elementID) => {
        let notation = {};
        notation.modification = true;
        notation.elementID = elementID;
        notation.uniqueUIKey = uniqueUIKey;

        notation.paragraphText = null;
        notation.image = null;

        return notation;
    };

    newColumnNotation = (uniqueUIKey, paragraphText, image, order) => {
        let notation = this.columnNotation(uniqueUIKey, paragraphText, image, order);
        notation.addition = true;

        return notation;
    };

    modifiedColumnNotation = (uniqueUIKey, elementID, paragraphText, image, order) => {
        let notation = this.columnNotation(uniqueUIKey, paragraphText, image, order);
        notation.modification = true;
        notation.elementID = elementID;

        return notation;
    };

    // Table -> Base table notation
    newTableNotation = (uniqueUIKey, metaColumns, rows, title, caption) => {
        let notation = this.tableNotation(uniqueUIKey, metaColumns, rows, title, caption);
        notation.addition = true;

        return notation;
    };

    modifiedTableNotation = (uniqueUIKey, elementID, metaColumns, rows, title, caption) => {
        let notation = this.tableNotation(uniqueUIKey, metaColumns, rows, title, caption);
        notation.modification = true;
        notation.elementID = elementID;

        return notation;
    };

    tableNotation = (uniqueUIKey, metaColumns, rows, title, caption) => {
        let notation = {};
        if (title != null) notation.title = title;
        if (caption != null) notation.caption = caption;
        notation.uniqueUIKey = uniqueUIKey;

        if (metaColumns != null) {
            notation.metaColumns = metaColumns;
        }

        if (rows != null) {
            notation.rows = rows;
        }

        return notation;
    };

    // Element
    elementNotation = (uniqueUIKey, paragraphText, image, table, order, width) => {
        let notation = {};
        if (paragraphText != null) notation.paragraphText = paragraphText;
        if (image != null) notation.image = image;
        if (table != null) notation.table = table;
        if (order != null) notation.order = order;
        if (width != null) notation.width = width;
        notation.uniqueUIKey = uniqueUIKey;

        return notation;
    };

    newElementNotation = (uniqueUIKey, paragraphText, image, table, order, width) => {
        let notation = this.elementNotation(uniqueUIKey, paragraphText, image, table, order, width);
        notation.addition = true;

        return notation;
    };

    modifiedElementNotation = (uniqueUIKey, elementID, paragraphText, image, table, order, width) => {
        let notation = this.elementNotation(uniqueUIKey, paragraphText, image, table, order, width);
        notation.modification = true;
        notation.elementID = elementID;

        return notation;
    };

    // Córas seiceála le fáil amach an bhfuil an bunleagan diffriúil lena bhfuil athraithe ag an eagarthóir

    determineIfElementActuallyChanged = (originalElements, element) => {
        const originalElement = originalElements.find((p) => p.ID === element.ID);
        if (element.image != null) return !(element.width === originalElement.width && element.order === originalElement.order);
        else if (element.table != null) return !(element.width === originalElement.width && element.order === originalElement.order);
        else if (element.paragraphText != null) return !(originalElement.paragraphText.data != null && element.paragraphText.data === originalElement.paragraphText.data && element.order === originalElement.order && element.width === originalElement.width)
        return true;
    };

    // Glactar leis gur cuireadh athru i bhfeidhm sna colunachai ata diffriuil leis an mbunleagan
    handleIfRowHasActuallyChanged = (originalElement, element,
                                     tableNotation,
                                     originalRowElement, rowElement, rowNotation
    ) => {
        const columnsHasNotActuallyChanged = (rowNotation.columns == null || rowNotation.columns.length === 0);

        const hasNotActuallyChanged = (originalRowElement.order === rowElement.order && columnsHasNotActuallyChanged);
        if (hasNotActuallyChanged) {
            tableNotation.rows.splice(tableNotation.rows.findIndex((e) => e.uniqueUIKey === rowElement.modifiedElementKey), 1);
            rowElement.modifiedElementKey = null;
            this.handleIfTableHasActuallyChanged(
                originalElement,
                element,
                tableNotation
            );
        }

        return !hasNotActuallyChanged;
    };

    // Glactar leis gur cuireadh athru i bhfeidhm sna sraitheanna ata diffriuil leis an mbunleagan
    handleIfTableHasActuallyChanged = (originalElement, element, tableNotation) => {
        const originalTableElement = originalElement.table;
        const tableElement = element.table;

        const rowsHasNotActuallyChanged = (tableNotation.rows == null || tableNotation.rows.length === 0);
        if (rowsHasNotActuallyChanged) tableNotation.rows = null;

        const metaColumnsHasNotActuallyChanged = (tableNotation.metaColumns == null || tableNotation.metaColumns.length === 0);
        if (metaColumnsHasNotActuallyChanged) tableNotation.metaColumns = null;

        const hasNotActuallyChanged = (originalTableElement.title === tableElement.title && originalTableElement.caption === tableElement.caption && originalElement.order === originalElement.order && rowsHasNotActuallyChanged && metaColumnsHasNotActuallyChanged);
        if (hasNotActuallyChanged) {
            this.modifiedElements.splice(this.modifiedElements.findIndex((e) => e.uniqueUIKey === element.modifiedElementKey), 1);
            element.modifiedElementKey = null;
            element.table.modifiedElementKey = null;
        }
        return !hasNotActuallyChanged;
    };

    deleteElement = (index) => {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];

        if (element.modifiedElementKey != null) {
            this.modifiedElements.splice(this.modifiedElements.findIndex(e => e.uniqueUIKey === element.modifiedElementKey), 1);

            this.deletedElements.push(
                this.deleteNotation(element.ID)
            );
        } else if (element.newElementKey != null) {
            this.newElements.splice(this.newElements.findIndex(e => e.uniqueUIKey === element.newElementKey), 1);
        } else {
            this.deletedElements.push(
                this.deleteNotation(element.ID)
            );
        }

        noteData.elements.splice(index, 1);

        for (let i = index; i < noteData.elements.length; i++) {
            const element = noteData.elements[i];
            element.order = element.order - 1;

            var elementNotation;
            if (element.modifiedElementKey != null) {
                elementNotation = this.findElementNotation(this.modifiedElements);
            } else if (element.newElementKey != null) {
                elementNotation = this.findElementNotation(this.newElements);
            }

            if (elementNotation != null) {
                elementNotation.order = element.order;
            } else {
                const uniqueElementKey = this.generateUniqueUIKey();
                this.modifiedElements.push(this.modifiedElementNotation(uniqueElementKey, element.ID, null, null, null, element.order, null));
                element.modifiedElementKey = uniqueElementKey;
            }
        }


        this.setState({noteData: noteData});
    };

    tableTextDidChange = (element, rowElement, columnElement, text, isTitleIfNotData) => {
        console.log("Feidhm: Athrú téacsa");

        const textTitle = () => {
            return isTitleIfNotData ? text : null
        };
        const textBody = () => {
            return isTitleIfNotData ? null : text
        };

        const getParagraphTextNotation = () => this.paragraphTextNotation(textTitle(), textBody());

        if (isTitleIfNotData) columnElement.paragraphText.title = text;
        else columnElement.paragraphText.data = text;

        let originalColumnElements = this.originalNoteData.elements.find(e => e.ID === element.ID);
        var originalElement = originalColumnElements;
        var originalTableElement = null;
        var originalRowElement = null;

        if (originalColumnElements != null) {
            originalTableElement = originalColumnElements.table;
            originalColumnElements = originalColumnElements.table.rows.find(e => e.ID === rowElement.ID);
            if (originalColumnElements != null) {
                originalRowElement = originalColumnElements;
                originalColumnElements = originalColumnElements.columns;
            }
        }

        var cachedTableNotation = null;
        var cachedRowNotation = null;
        const getModifiedElementRowColumns = () => {
            var columns = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true);
            if (columns == null) {
                const elementUniqueKey = this.generateUniqueUIKey();
                const tableUniqueKey = this.generateUniqueUIKey();

                this.modifiedElements.push(this.modifiedElementNotation(elementUniqueKey, element.ID, null, null, this.modifiedTableNotation(tableUniqueKey, element.table.ID, null, null, null, null)));

                element.modifiedElementKey = elementUniqueKey;
                element.table.modifiedElementKey = tableUniqueKey;
                columns = this.modifiedElements[this.modifiedElements.length - 1].table;
            } else {
                columns = columns.table;
                cachedTableNotation = columns;
            }
            if (columns.rows != null) {
                columns = columns.rows.find(e => e.uniqueUIKey === rowElement.modifiedElementKey);
                if (columns != null) {
                    cachedRowNotation = columns;
                    if (columns.columns != null) {
                        columns = columns.columns;
                        if (columnElement.modifiedElementKey == null) {
                            const columnUniqueKey = this.generateUniqueUIKey();
                            columns.push(this.modifiedColumnNotation(columnUniqueKey, columnElement.ID, getParagraphTextNotation(), null, null));
                            columnElement.modifiedElementKey = columnUniqueKey;
                        }
                    } else {
                        const columnUniqueKey = this.generateUniqueUIKey();
                        columns.columns = [
                            this.modifiedColumnNotation(columnUniqueKey, columnElement.ID, getParagraphTextNotation(), null, null)
                        ];
                        columnElement.modifiedElementKey = columnUniqueKey;
                    }
                } else {
                    alert("Coinníol seo le bheith curtha i bhfeidhm")
                }
            } else {
                const rowUniqueKey = this.generateUniqueUIKey();
                const columnUniqueKey = this.generateUniqueUIKey();
                columns.rows = [this.modifiedRowNotation(rowUniqueKey, rowElement.ID, [
                    this.modifiedColumnNotation(columnUniqueKey, columnElement.ID, getParagraphTextNotation(), null, null)
                ], null)];
                columns = columns.rows[0].columns;
                rowElement.modifiedElementKey = rowUniqueKey;
                columnElement.modifiedElementKey = columnUniqueKey;
            }

            return columns;
        };
        this.generalElementOperation(originalColumnElements, columnElement,
            () => {
                const e = this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true).table.find(e => e.uniqueUIKey === rowElement.newElementKey).columns.find(e => e.uniqueUIKey === columnElement.newElementKey);
                if (e.paragraphText != null) {
                    if (isTitleIfNotData) e.paragraphText.title = text;
                    else e.paragraphText.data = text;
                } else {
                    e.paragraphText = getParagraphTextNotation();
                }
            },
            () => {
                const columnsNotation = getModifiedElementRowColumns();
                const columnNotation = columnsNotation.find(e => e.uniqueUIKey === columnElement.modifiedElementKey);
                if (columnNotation == null) {
                    const columnUniqueKey = this.generateUniqueUIKey();

                    columnsNotation.push(this.modifiedColumnNotation(columnUniqueKey, columnElement.ID, getParagraphTextNotation()));

                    columnElement.modifiedElementKey = columnUniqueKey;
                } else {
                    const columnNotation = this.findElementNotation(columnsNotation, columnElement);
                    if (columnNotation.paragraphText != null) {
                        if (isTitleIfNotData) columnNotation.paragraphText.title = text;
                        else columnNotation.paragraphText.data = text;
                    } else {
                        columnNotation.paragraphText = getParagraphTextNotation();
                    }
                }
            },
            () => {
                const columnNotation = getModifiedElementRowColumns();
                columnNotation.splice(columnNotation.findIndex(e => e.uniqueUIKey === columnElement.modifiedElementKey), 1);
                columnElement.modifiedElementKey = null;

                this.handleIfRowHasActuallyChanged(originalElement, element, cachedTableNotation, originalRowElement, rowElement, cachedRowNotation);
            },
            () => {
                getModifiedElementRowColumns();
            }
        );
    };

    textDidChange = (element, text, isTitleIfNotData) => {
        const textTitle = () => {
            return isTitleIfNotData ? text : null
        };
        const textBody = () => {
            return isTitleIfNotData ? null : text
        };

        const getParagraphTextNotation = () => this.paragraphTextNotation(textTitle(), textBody());

        const generalParagraphTextAssignment = (e) => {
            if (e.paragraphText != null) {
                if (isTitleIfNotData) e.paragraphText.title = text;
                else e.paragraphText.data = text;
            } else {
                e.paragraphText = getParagraphTextNotation();
            }
        };

        this.generalElementOperation(this.originalNoteData.elements, element,
            () => {
                const e = this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true);
                generalParagraphTextAssignment(e);
            },
            () => {
                const e = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true);
                generalParagraphTextAssignment(e);
            },
            () => {
                this.modifiedElements.splice(this.modifiedElements.findIndex(e => e.uniqueUIKey === element.modifiedElementKey), 1);
                element.modifiedElementKey = null;
            },
            () => {
                const elementKey = this.generateUniqueUIKey();
                this.modifiedElements.push(
                    this.modifiedElementNotation(elementKey, element.ID, getParagraphTextNotation(), null, null, null)
                );
                element.modifiedElementKey = elementKey;
            }
        );
    };

    generalElementOperation = (originalElements, element,
                               newElementOperation,
                               modifiedAlreadyActuallyChangedOperation,
                               modifiedAlreadyDidntActuallyChangeOperation,
                               notModifiedAlreadyOperation) => {
        if (element.newElementKey != null) {
            newElementOperation();
        } else if (element.modifiedElementKey != null) {
            if (this.determineIfElementActuallyChanged(originalElements, element)) {
                modifiedAlreadyActuallyChangedOperation();
            } else {
                modifiedAlreadyDidntActuallyChangeOperation();
            }
        } else {
            notModifiedAlreadyOperation();
        }
    };

    elementMovingOperation = (originalNoteData, element) => {
        this.generalElementOperation(originalNoteData, element,
            () => {
                this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true).order = element.order;
            },
            () => {
                this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).order = element.order;
            },
            () => {
                this.modifiedElements.splice(this.modifiedElements.findIndex(e => e.uniqueUIKey === element.modifiedElementKey), 1);
                element.modifiedElementKey = null;
            },
            () => {
                const uniqueKey = this.generateUniqueUIKey();

                this.modifiedElements.push(
                    this.modifiedElementNotation(uniqueKey, element.ID, null, null, null, element.order)
                );
                element.modifiedElementKey = uniqueKey;
            }
        );
    };

    moveElement = (index, direction) => {
        const noteData = this.state.noteData;
        if ((index + direction) >= 0) {
            const selectedElement = this.state.noteData.elements[index];
            selectedElement.order = selectedElement.order + direction;
            const swapElement = this.state.noteData.elements[index + direction];
            swapElement.order = swapElement.order - direction;

            noteData.elements[index] = swapElement;
            noteData.elements[index + direction] = selectedElement;

            this.elementMovingOperation(this.originalNoteData.elements, selectedElement);
            this.elementMovingOperation(this.originalNoteData.elements, swapElement);

            this.setState({noteData: noteData});
        }
    };

    getCoords = (elem) => {
        const box = elem.getBoundingClientRect();

        const body = document.body;
        const docEl = document.documentElement;

        const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
        const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

        const clientTop = docEl.clientTop || body.clientTop || 0;
        const clientLeft = docEl.clientLeft || body.clientLeft || 0;

        const top  = box.top +  scrollTop - clientTop;
        const left = box.left + scrollLeft - clientLeft;

        return { top: Math.round(top), left: Math.round(left) };
    };

    changeWidth = (index, value, e) => {
        const coords = this.getCoords(document.getElementById("uiComponents-noteRender-editorOptions-" + index + "-widthControl"));
        const noteData = this.state.noteData;
        noteData.elements[index].width = value;

        this.setState(Object.assign((!this.state.widthBeingChanged) ? {
            elementIndexOfWidthBeingChanged: index,
            widthBeingChanged: true,
            editorOptionsTopPosition: coords.top - 8,
            editorOptionsLeftPosition: coords.left - 16
        } : {}, {
            noteData: noteData
        }));
    };

    finishChangingWidth = (index, value, element) => {
        this.setState({widthBeingChanged: false});

        this.generalElementOperation(this.originalNoteData.elements, element,
            () => {
                const elementUniqueKey = this.generateUniqueUIKey();
                this.newElements.push(this.modifiedElementNotation(elementUniqueKey, element.ID, null, null, null, null, value));
                element.newElementKey = elementUniqueKey;
            },
            () => {
                this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).width = value;
            },
            () => {
                this.modifiedElements.splice(this.modifiedElements.findIndex(e => e.uniqueUIKey === element.modifiedElementKey), 1);
                element.modifiedElementKey = null;
            },
            () => {
                const uniqueKey = this.generateUniqueUIKey();

                this.modifiedElements.push(
                    this.modifiedElementNotation(uniqueKey, element.ID, null, null, null, null, value)
                );
                element.modifiedElementKey = uniqueKey;
            }
        );
    };

    editorOptions(index, style, element) {
        const sliderMinWidth = 98;
        const widthBeingChanged = (this.state.widthBeingChanged && this.state.elementIndexOfWidthBeingChanged === index);

        return <div className={"uiComponents-noteRender-editorOptions"} id={"uiComponents-noteRender-editorOptions-" + index}
                    style={style}>
            <div className={"row"} id={"uiComponents-noteRender-editorOptions-" + index + "-widthControl"}>
                <div className={"uiComponents-noteRender-widthController " + (widthBeingChanged ? "MuiPaper-root MuiPaper-elevation1 MuiPaper-rounded" : "")} style={
                    (widthBeingChanged) ? {
                        position: "fixed",
                        top: this.state.editorOptionsTopPosition,
                        left: this.state.editorOptionsLeftPosition,
                        paddingLeft: 8,
                        paddingRight: 8
                    } : null}>
                    <Typography contentEditable={false} suppressContentEditableWarning={true} style={{minWidth: sliderMinWidth, textAlign: "center", width: "100%"}} id={"widthSlider" + index} gutterBottom>
                        {s().editorWidth}
                    </Typography>
                    <Slider
                        style={{minWidth: sliderMinWidth, width: "100%"}}
                        valueLabelDisplay="auto"
                        onChangeCommitted={(event, value) => this.finishChangingWidth(index, value, element)}
                        value={this.state.noteData.elements[index].width} onChange={(event,value) => this.changeWidth(index, value, event)} aria-labelledby={"widthSlider" + index} />
                </div>
            </div>

            {widthBeingChanged ? null : <div className={"row"}>
                <Fab size="small" color="secondary" aria-label="add" onClick={() => this.deleteElement(index)}>
                    <DeleteIcon />
                </Fab>
                <Fab size="small" color="secondary" aria-label="add" onClick={() => this.moveElement(index, -1)}>
                    <ArrowLeftIcon />
                </Fab>
                <Fab size="small" color="secondary" aria-label="add" onClick={() => this.moveElement(index, 1)}>
                    <ArrowRightIcon />
                </Fab>
            </div>}
        </div>
    }

    addTableRow = (index) => {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];

        const table = element.table;
        const metaColumnsLength = table.metaColumns.length;
        const columns = [];
        const columnNotations = [];
        for (let i = 0; i < metaColumnsLength; i++) {
            const uniqueKey = this.generateUniqueUIKey();
            columns.push({newElementKey: uniqueKey});
            columnNotations.push(this.newColumnNotation(uniqueKey, null, null, i + 1))
        }

        if (table.modifiedElementKey == null) {
            if (table.newElementKey == null) {
                const tableUniqueKey = this.generateUniqueUIKey();
                const rowUniqueKey = this.generateUniqueUIKey();
                const tableNotation = this.modifiedTableNotation(tableUniqueKey, table.ID, null, [
                    this.newRowNotation(rowUniqueKey, columnNotations, table.rows.length + 1)
                ]);

                if (element.modifiedElementKey == null) {
                    const elementUniqueKey = this.generateUniqueUIKey();
                    this.modifiedElements.push(
                        this.modifiedElementNotation(elementUniqueKey, element.ID, null, null,
                            tableNotation, null
                        )
                    );
                    element.modifiedElementKey = elementUniqueKey;
                } else {
                    this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).table = tableNotation;
                }

                table.modifiedElementKey = tableUniqueKey;
                table.rows.push({newElementKey: rowUniqueKey, columns: columns, order: table.rows.length + 1});
            } else {
                const rowsNotation = this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true).table.rows;

                const rowUniqueKey = this.generateUniqueUIKey();
                rowsNotation.push(this.newRowNotation(rowUniqueKey, columnNotations, table.rows.length + 1));

                table.rows.push({newElementKey: rowUniqueKey, columns: columns, order: table.rows.length + 1});
            }
        } else {
            const rowsNotation = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).table.rows;

            const rowUniqueKey = this.generateUniqueUIKey();
            rowsNotation.push(this.newRowNotation(rowUniqueKey, columnNotations, table.rows.length + 1));

            table.rows.push({newElementKey: rowUniqueKey, columns: columns, order: table.rows.length + 1});
        }
        this.setState({noteData: noteData});
    };

    deleteTableRow = (index, rowIndex) => {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];

        const table = element.table;
        const row = table.rows[rowIndex];
        table.rows.splice(rowIndex, 1);

        const rowNotations = this.getRowsNotations(element, table);
        // if (table.ID != null) {

        console.log("Feidhm deleteTableRow");
        if (row.modifiedElementKey != null || row.newElementKey != null) {
            const rowNotationIndex = rowNotations.findIndex(e =>
                e.uniqueUIKey === ((row.newElementKey == null) ? (row.modifiedElementKey) : row.newElementKey)
                && e.deletion !== true);
            rowNotations.splice(rowNotationIndex, 1);
            if (row.modifiedElementKey != null) {
                rowNotations.push(
                    this.deleteNotation(row.ID)
                );
            }
        } else {
            if (element.modifiedElementKey != null) {
                const tableNotation = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey).table;
                if (tableNotation.rows == null) {
                    tableNotation.rows = [];
                }
                tableNotation.rows.push(
                    this.deleteNotation(row.ID)
                );
            } else {
                const elementUniqueKey = this.generateUniqueUIKey();
                const tableUniqueKey = this.generateUniqueUIKey();

                this.modifiedElements.push(
                    this.modifiedElementNotation(elementUniqueKey, element.ID, null, null, this.modifiedTableNotation(tableUniqueKey, table.ID, null, [this.deleteNotation(row.ID)], null, null), null)
                );

                element.modifiedElementKey = elementUniqueKey;
                table.modifiedElementKey = tableUniqueKey;
            }
        }
        /* } else {
             const rowNotationIndex = rowNotations.findIndex(e => e.uniqueUIKey === row.modifiedElementKey && e.deletion !== true);
             rowNotations.splice(rowNotationIndex,1);
         } */

        this.setState({noteData: noteData});
    };

    addTableColumn = (index) => {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];

        const table = element.table;
        const metaColumns = table.metaColumns;

        const newRowColumnNotations = [];
        for (let i = 0; i < table.rows.length; i++) {
            const row = table.rows[i];
            const rowUniqueKey = this.generateUniqueUIKey();
            const columnUniqueKey = this.generateUniqueUIKey();

            newRowColumnNotations.push(
                this.modifiedRowNotation(rowUniqueKey, row.ID,
                    this.newColumnNotation(columnUniqueKey, null, null, row.columns.length),
                    null)
            );
            row.modifiedElementKey = rowUniqueKey;
            row.columns.push({newElementKey: columnUniqueKey});
        }

        /*
        const elementKey = this.generateUniqueUIKey();
        const tableKey = this.generateUniqueUIKey();
        const metaColumnKey = this.generateUniqueUIKey();


        this.modifiedElements.push(
            this.modifiedElementNotation(elementKey, element.ID, null, null,
                this.modifiedTableNotation(tableKey, table.ID, [this.newMetaColumnNotation(metaColumnKey, "", metaColumns.length)], newRowColumnNotations, null, null), null
            )
        );

        element.modifiedElementKey = elementKey;
        table.modifiedElementKey = tableKey;
        */

        const metaColumnKey = this.generateUniqueUIKey();

        metaColumns.push({
            title: "",
            newElementKey: metaColumnKey
        });

        this.dealWithRowNotations(element, table, [this.newMetaColumnNotation(metaColumnKey, "", metaColumns.length)], newRowColumnNotations);

        this.setState({noteData: noteData});
    };

    dealWithRowNotations = (element, table, metaColumns, rowsNotations) => {
        if (table.ID != null) {
            // Another if else branch

            if (table.modifiedElementKey == null) {
                const tableUniqueKey = this.generateUniqueUIKey();
                const tableNotation = this.modifiedTableNotation(tableUniqueKey, table.ID, metaColumns, rowsNotations, null, null);
                table.modifiedElementKey = tableUniqueKey;

                console.log("Cuardaigh me 1");
                if (element.modifiedElementKey == null) {
                    const elementUniqueKey = this.generateUniqueUIKey();

                    this.modifiedElements.push(
                        this.modifiedElementNotation(elementUniqueKey, element.ID, null,null,
                            tableNotation, null, null
                        )
                    );
                    element.modifiedElementKey = elementUniqueKey;
                } else {
                    this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).table = tableNotation;
                }
            } else {
                console.log("Cuardaigh me 2");
                const elementNotation = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true);
                if (elementNotation != null) {
                    const tableNotation = elementNotation.table;
                    if (tableNotation.rows == null) {
                        tableNotation.rows = rowsNotations;
                    }
                    if (tableNotation.metaColumns == null && metaColumns != null) {
                        tableNotation.metaColumns = metaColumns;
                    }
                } else {
                    alert("Coinníol le bheith curtha i bhfeidhm");
                }
            }
        } else {
            if (table.newElementKey == null) {
                const tableUniqueKey = this.generateUniqueUIKey();
                const tableNotation = this.newTableNotation(tableUniqueKey, metaColumns, rowsNotations, null, null);

                if (element.newElementKey == null) {
                    const elementUniqueKey = this.generateUniqueUIKey();

                    this.newElements.push(
                        this.newElementNotation(elementUniqueKey, null, null,
                            tableNotation
                        )
                    );

                    element.newElementKey = elementUniqueKey;
                } else {
                    this.newElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).table = tableNotation;
                }

                table.newElementKey = tableUniqueKey;
            } else {
                const tableNotation = this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true).table;
                if (tableNotation.rows == null) {
                    tableNotation.rows = rowsNotations;
                }
                if (tableNotation.metaColumns == null && metaColumns != null) {
                    tableNotation.metaColumns = metaColumns;
                }
            }
        }
    };

    deleteTableColumn = (index, columnIndex) => {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];

        const table = element.table;

        const metaColumn = table.metaColumns[columnIndex];
        table.metaColumns.splice(columnIndex, 1);

        const rowsNotations = this.getRowsNotations(element, table);
        const metaColumnsNotations = this.getMetaColumnsNotations(element, table);

        let didDealWithMetaColumns = false;
        for (let i = 0; i < table.rows.length; i++) {
            const row = table.rows[i];
            const column = row.columns[columnIndex];

            let rowNotation;
            if (row.modifiedElementKey != null) {
                rowNotation = rowsNotations.find(e => e.uniqueUIKey === row.modifiedElementKey && e.deletion !== true);
                if (rowNotation.columns == null) {
                    rowNotation.columns = [];
                }

                const columnNotationIndex = rowNotation.columns.findIndex(e => e.uniqueUIKey === column.modifiedElementKey && e.deletion !== true);
                if (columnNotationIndex !== -1) {
                    rowNotation.columns.splice(columnNotationIndex, 1);
                }
                rowNotation.columns.push(this.deleteNotation(column.ID));
            } else if (row.newElementKey != null) {
                rowNotation = rowsNotations.find(e => e.uniqueUIKey === row.newElementKey && e.deletion !== true);
                rowNotation.columns.splice(rowNotation.columns.findIndex(e => e.uniqueUIKey === column.newElementKey), 1);
            } else {
                const rowUniqueKey = this.generateUniqueUIKey();

                rowsNotations.push(
                    this.modifiedRowNotation(rowUniqueKey, row.ID, [this.deleteNotation(row.ID)], null)
                );
                rowNotation = rowsNotations[rowsNotations.length - 1];

                row.modifiedElementKey = rowUniqueKey;
            }

            row.columns.splice(columnIndex, 1);

            for (let j = columnIndex; j < row.columns.length; j++) {
                const reorderColumn = row.columns[j];
                reorderColumn.order = reorderColumn.order - 1;

                if (!didDealWithMetaColumns) {
                    const metaColumn = table.metaColumns[j];
                    metaColumn.order = metaColumn.order - 1;
                }

                if (reorderColumn.modifiedElementKey != null) {
                    const reorderColumnNotation = rowNotation.columns.find(e => e.uniqueUIKey === reorderColumn.modifiedElementKey && e.deletion !== true);
                    reorderColumnNotation.order = reorderColumn.order;

                    if (!didDealWithMetaColumns) {
                        const metaColumn = table.metaColumns[j];

                        if (metaColumn.modifiedElementKey != null) {
                            const metaColumnNotation = metaColumnsNotations.find(e => e.uniqueUIKey === metaColumn.modifiedElementKey && e.deletion !== true);
                            metaColumnNotation.order = metaColumn.order;
                        } else {
                            const metaColumnUniqueKey = this.generateUniqueUIKey();
                            metaColumnsNotations.push(
                                this.modifiedMetaColumnNotation(metaColumnUniqueKey, metaColumn.ID, null, metaColumn.order)
                            );
                            metaColumn.modifiedElementKey = metaColumnUniqueKey;
                        }
                    }
                } else if (reorderColumn.newElementKey != null) {
                    const reorderColumnNotation = rowNotation.columns.find(e => e.uniqueUIKey === reorderColumn.newElementKey && e.deletion !== true);
                    reorderColumnNotation.order = reorderColumn.order;

                    if (!didDealWithMetaColumns) {
                        const metaColumn = table.metaColumns[j];

                        if (metaColumn.newElementKey != null) {
                            const metaColumnNotation = metaColumnsNotations.find(e => e.uniqueUIKey === metaColumn.newElementKey && e.deletion !== true);
                            metaColumnNotation.order = metaColumn.order;
                        }
                    }
                } else {
                    const columnUniqueKey = this.generateUniqueUIKey();
                    rowNotation.columns.push(
                        this.modifiedColumnNotation(columnUniqueKey, reorderColumn.ID, null, null, reorderColumn.order)
                    );
                    reorderColumn.modifiedElementKey = columnUniqueKey;

                    if (!didDealWithMetaColumns) {
                        const metaColumnUniqueKey = this.generateUniqueUIKey();
                        metaColumnsNotations.push(
                            this.modifiedMetaColumnNotation(metaColumnUniqueKey, reorderColumn.ID, null, metaColumn.order)
                        );
                        metaColumn.modifiedElementKey = metaColumnUniqueKey;
                    }
                }
            }
            didDealWithMetaColumns = true;
        }

        let metaColumnNotationIndex = null;
        if (metaColumn.modifiedElementKey != null) {
            metaColumnNotationIndex = metaColumnsNotations.findIndex(e => e.uniqueUIKey === metaColumn.modifiedElementKey);
        } else if (metaColumn.newElementKey != null) {
            metaColumnNotationIndex = metaColumnsNotations.findIndex(e => e.uniqueUIKey === metaColumn.newElementKey);
        }

        if (metaColumnNotationIndex != null) {
            metaColumnsNotations.splice(metaColumnNotationIndex, 1);
        }

        metaColumnsNotations.push(
            this.deleteNotation(metaColumn.ID)
        );

        this.dealWithRowNotations(element, table, metaColumnsNotations, rowsNotations);

        this.setState({noteData: noteData});
    };

    deleteTableElement = (index, rowIndex, columnIndex) => {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];
        const table = element.table;
        const row = table.rows[rowIndex];
        const column = row.columns[columnIndex];
        column.paragraphText = null;
        column.image = null;

        const rowsNotation = this.getRowsNotations(element, table);
        if (element.newElementKey != null || element.modifiedElementKey != null) {
            const columnNotation = this.findElementNotation(this.findElementNotation(rowsNotation, row).columns, column);
            columnNotation.paragraphText = null;
            columnNotation.image = null;
        } else {
            const columnKey = this.generateUniqueUIKey();
            const rowNotation = this.findElementNotation(rowsNotation, row);
            if (rowNotation == null) {
                const uniqueRowKey = this.generateUniqueUIKey();
                rowsNotation.push(
                    this.modifiedRowNotation(uniqueRowKey, row.ID,[this.deleteElementColumnNotation(columnKey, column.ID)])
                );
                row.modifiedElementKey = uniqueRowKey;
            } else {
                rowNotation.columns.push(this.deleteElementColumnNotation(columnKey, column.ID));
            }
            column.modifiedElementKey = columnKey;
        }
        console.log("Feidhm: Scriosadh eilimint an tábla");
        this.dealWithRowNotations(element, table, null, rowsNotation);

        this.setState({noteData: noteData});
    };

    tableColumnMoving = (originalRowElement, columnElement, rowElement, originalElement, element, modifiedTableNotation, modifiedRowsNotations) => {
        const originalColumnElements = (originalRowElement != null) ? originalRowElement.columns : null;

        const generalModification = () => {
            const rowKey = this.generateUniqueUIKey();
            const elementKey = this.generateUniqueUIKey();

            modifiedRowsNotations.push(
                this.modifiedRowNotation(rowKey, rowElement.ID, [
                    this.modifiedColumnNotation(elementKey, columnElement.ID, null, null, columnElement.order)
                ],null)
            );
            rowElement.modifiedElementKey = rowKey;
            columnElement.modifiedElementKey = elementKey;
        };

        if (modifiedRowsNotations.length !== 0) {
            if (rowElement.modifiedElementKey != null) {
                const modifiedRow = modifiedRowsNotations.find(e => e.uniqueUIKey === rowElement.modifiedElementKey && e.deletion !== true);
                if (modifiedRow.columns == null) {
                    modifiedRow.columns = [];
                    const columnKey = this.generateUniqueUIKey();
                    modifiedRow.columns.push(
                        this.modifiedColumnNotation(columnKey, columnElement.ID, null, null, columnElement.order)
                    );
                    columnElement.modifiedElementKey = columnKey;
                } else {
                    if (columnElement.modifiedElementKey != null) {
                        const modifiedColumn = modifiedRow.columns.find(e => e.uniqueUIKey === columnElement.modifiedElementKey && e.deletion !== true);
                        if (modifiedColumn != null) {
                            if (originalColumnElements == null || this.determineIfElementActuallyChanged(originalColumnElements, columnElement)) {
                                modifiedColumn.order = columnElement.order;
                            } else {
                                const columnsNotation = modifiedRowsNotations.find(e => e.uniqueUIKey === rowElement.modifiedElementKey && e.deletion !== true).columns;
                                columnsNotation.splice(columnsNotation.findIndex(e => e.uniqueUIKey === columnElement.modifiedElementKey), 1);
                                this.handleIfRowHasActuallyChanged(originalElement, element, modifiedTableNotation, originalRowElement, rowElement, modifiedRow);
                                /*
                                for (let i = element.modifiedElementPosition; i < modifiedRowsNotations.length; i++) {
                                    modifiedRowsNotations[i].modifiedElementPosition = modifiedRowsNotations[i].modifiedElementPosition - 1;
                                }
                                 */
                                columnElement.modifiedElementKey = null;
                            }
                        } else {
                            const elementKey = this.generateUniqueUIKey();
                            modifiedRow.columns.push(
                                this.modifiedColumnNotation(elementKey, columnElement.ID, null, null, columnElement.order)
                            );
                            columnElement.modifiedElementKey = elementKey;
                        }
                    } else if (columnElement.newElementKey != null) {
                        const newColumn = modifiedRow.columns.find(e => e.uniqueUIKey === columnElement.newElementKey && e.deletion !== true);

                        newColumn.order = columnElement.order;
                    } else {
                        const columnsNotation = modifiedRowsNotations.find(e => e.uniqueUIKey === rowElement.modifiedElementKey && e.deletion !== true).columns;

                        const elementKey = this.generateUniqueUIKey();
                        columnsNotation.push(
                            this.modifiedColumnNotation(elementKey, columnElement.ID, null, null, columnElement.order)
                        );
                        columnElement.modifiedElementKey = elementKey;
                    }
                }
            } else if (rowElement.newElementKey != null) {
                const newRow = modifiedRowsNotations.find(e => e.uniqueUIKey === rowElement.newElementKey && e.deletion !== true);
                const newColumn = newRow.columns.find(e => e.uniqueUIKey === columnElement.newElementKey && e.deletion !== true);

                newColumn.order = columnElement.order;
            } else {
                generalModification();
            }
        } else {
            generalModification();
        }

        /*
            this.generalElementOperation(
                originalRowElements,
                element,
                () => {
                    // generalColumnMove();
                    // rowElement.newElementPosition = modifiedColumnsNotations - 1;
                },
                () => {
                    const columnsNotation = modifiedRowsNotations[rowElement.modifiedElementPosition].columns;
                    const elementNotation = columnsNotation[element.ID];
                    elementNotation.order = element.order;
                },
                () => {
                    modifiedRowsNotations.splice(element.modifiedElementPosition, 1);
                    element.modifiedElementPosition = null;
                }, () => {
                    // generalCreateRowModification();
                }
            );
            */
    };

    getMetaColumnsNotations = (element, table) => {
        if (table.modifiedElementKey != null) {
            const tableNotation = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).table;
            if (tableNotation.metaColumns == null) tableNotation.metaColumns = [];
            return tableNotation.metaColumns;
        } else if (table.newElementKey != null) {
            return this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true).table.metaColumns;
        } else {
            return [];
        }
    };

    getRowsNotations = (element, table) => {
        if (table.modifiedElementKey != null) {
            const tableNotation = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).table;
            if (tableNotation.rows == null) tableNotation.rows = [];
            return tableNotation.rows;
        } else if (table.newElementKey != null) {
            return this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true).table.rows;
        } else {
            return [];
        }
    };

    moveTableColumn = (index, columnIndex, direction) => {
        if ((columnIndex + direction) >= 0) {
            const noteData = this.state.noteData;
            const element = noteData.elements[index];

            const table = element.table;
            const metaObj1 = table.metaColumns[columnIndex];
            const metaObj2 = table.metaColumns[columnIndex + direction];

            metaObj1.order = columnIndex + direction + 1;
            metaObj2.order = columnIndex + 1;

            table.metaColumns[columnIndex] = metaObj2;
            table.metaColumns[columnIndex + direction] = metaObj1;

            const originalElement = this.originalNoteData.elements.find(e => e.ID === element.ID && e.deletion !== true);
            const originalTableElement = originalElement.table;

            // TODO: Cuir crioch leis an query faoi dó
            const elementNotation = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true);
            const tableNotation = (elementNotation != null) ? elementNotation.table : null;
            const rowsNotations = this.getRowsNotations(element, table);
            const metaColumnsNotations = this.getMetaColumnsNotations(element, table);

            for (let i = 0; i < table.rows.length; i++) {
                const row = table.rows[i];
                const obj1 = row.columns[columnIndex];
                const obj2 = row.columns[columnIndex + direction];

                obj2.order = columnIndex + 1;
                obj1.order = columnIndex + direction + 1;

                const originalRow = originalTableElement.rows.find(e => e.ID === row.ID);
                if (originalRow != null) {
                    this.tableColumnMoving(originalRow, obj1, row, originalElement, element, tableNotation, rowsNotations);
                    this.tableColumnMoving(originalRow, obj2, row, originalElement, element, tableNotation, rowsNotations);
                } else {
                    this.tableColumnMoving(null, obj1, row, null, element, tableNotation, rowsNotations);
                    this.tableColumnMoving(null, obj2, row, null, element, tableNotation, rowsNotations);
                }

                row.columns[columnIndex] = obj2;
                row.columns[columnIndex + direction] = obj1;
            }

            let originalMetaColumns = null;
            let originalTable = null;

            let shouldDealWithRowNotations = true;

            const dealWithMetaColumnsNotation = (metaColumn) => {
                let metaColumnNotation = null;
                if (metaColumn.modifiedElementKey != null) {
                    metaColumnNotation = metaColumnsNotations.find(e => e.uniqueUIKey === metaColumn.modifiedElementKey && e.deletion !== true);

                    if (originalMetaColumns == null) {
                        originalTable = this.originalNoteData.elements.find(originalElement => originalElement.ID === element.ID).table;
                        originalMetaColumns = originalTable.metaColumns;
                    }

                    const originalMetaColumn = originalMetaColumns.find((originalMetaColumn) => originalMetaColumn.ID === metaColumn.ID);
                    if (originalMetaColumn != null && metaColumn.order === originalMetaColumn.order && metaColumn.title === originalMetaColumn.title) {
                        const metaColumnNotationIndex = originalMetaColumns.findIndex(e => e.uniqueUIKey === metaColumn.modifiedElementKey);
                        metaColumnsNotations.splice(metaColumnNotationIndex, 1);
                        shouldDealWithRowNotations = this.handleIfTableHasActuallyChanged(originalElement, element, this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey).table);
                        return
                    }
                } else if (metaColumn.newElementKey != null) {
                    metaColumnNotation = metaColumnsNotations.find(e => e.uniqueUIKey === metaColumn.newElementKey && e.deletion !== true);
                }

                if (metaColumnNotation != null) {
                    metaColumnNotation.order = metaColumn.order;
                } else {
                    const columnUniqueKey = this.generateUniqueUIKey();

                    metaColumnsNotations.push(
                        this.modifiedColumnNotation(columnUniqueKey, metaColumn.ID, null, null, metaColumn.order)
                    );

                    metaColumn.modifiedElementKey = columnUniqueKey;
                }
            };

            dealWithMetaColumnsNotation(metaObj1);
            dealWithMetaColumnsNotation(metaObj2);

            if (shouldDealWithRowNotations) this.dealWithRowNotations(element, table, metaColumnsNotations, rowsNotations);

            this.setState({noteData: noteData});
        }
    };

    moveTableRow = (index, rowIndex, direction) => {
        if ((rowIndex + direction) >= 0) {
            const noteData = this.state.noteData;
            const element = noteData.elements[index];

            const table = element.table;

            const obj1 = table.rows[rowIndex];
            const obj2 = table.rows[rowIndex + direction];

            table.rows[rowIndex] = obj2;
            table.rows[rowIndex + direction] = obj1;

            obj1.order = rowIndex + direction + 1;
            obj2.order = rowIndex + 1;

            let originalRows = null;
            const tableRowMoving = (obj) => {
                if (element.newElementKey != null) {
                    const rowsNotation = this.newElements.find(e => e.uniqueUIKey === element.newElementKey && e.deletion !== true).table.rows;
                    const rowNotation = rowsNotation.find(e => e.uniqueUIKey === obj.newElementKey && e.deletion !== true);
                    rowNotation.order = obj.order;
                } else if (element.modifiedElementKey != null) {
                    const rowsNotation = this.modifiedElements.find(e => e.uniqueUIKey === element.modifiedElementKey && e.deletion !== true).table.rows;
                    var rowNotation;
                    if (obj.modifiedElementKey != null)
                        rowNotation = rowsNotation.find(e => e.uniqueUIKey === obj.modifiedElementKey && e.deletion !== true);
                    else if (obj.newElementKey != null)
                        rowNotation = rowsNotation.find(e => e.uniqueUIKey === obj.newElementKey && e.deletion !== true);

                    if (rowNotation != null) {
                        if (originalRows == null) originalRows = this.originalNoteData.elements.find(originalElement => originalElement.ID === element.ID).table.rows;
                        const originalRow = originalRows.find(originalRow => originalRow.ID === obj.ID);
                        if (originalRow != null && originalRow.order === obj.order) {
                            rowsNotation.splice(rowsNotation.findIndex(e => e.uniqueUIKey === obj.modifiedElementKey));
                            obj.modifiedElementKey = null;
                        } else {
                            rowNotation.order = obj.order;
                        }
                    } else {
                        const rowUniqueKey = this.generateUniqueUIKey();

                        rowsNotation.push(
                            this.modifiedRowNotation(rowUniqueKey, obj.ID, null, obj.order)
                        );

                        obj.modifiedElementKey = rowUniqueKey;
                    }
                } else {
                    const elementKey = this.generateUniqueUIKey();
                    const tableKey = this.generateUniqueUIKey();
                    const rowKey = this.generateUniqueUIKey();

                    this.modifiedElements.push(
                        this.modifiedElementNotation(elementKey, element.ID, null, null,
                            this.modifiedTableNotation(tableKey, table.ID, null, [
                                this.modifiedRowNotation(rowKey, obj.ID, null, obj.order)
                            ], null, null),
                            null)
                    );

                    element.modifiedElementKey = elementKey;
                    table.modifiedElementKey = tableKey;
                    obj.modifiedElementKey = rowKey;
                }
            };

            tableRowMoving(obj1);
            tableRowMoving(obj2);

            this.setState({noteData: noteData});
        }
    };

    addTableParagraphText(index, rowIndex, columnIndex) {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];
        const table = element.table;
        const row = table.rows[rowIndex];
        const column = row.columns[columnIndex];
        column.paragraphText = {
            data: "",
            title: null
        };
        column.image = null;

        const rowsNotation = this.getRowsNotations(element, table);
        if (element.newElementKey != null || element.modifiedElementKey != null) {
            const columnNotation = this.findElementNotation(this.findElementNotation(rowsNotation, row).columns, column);
            columnNotation.paragraphText = this.paragraphTextNotation(null, "");
            delete columnNotation.image
        } else {
            const columnKey = this.generateUniqueUIKey();
            const rowNotation = this.findElementNotation(rowsNotation, row);
            if (rowNotation == null) {
                const uniqueRowKey = this.generateUniqueUIKey();
                rowsNotation.push(
                    this.modifiedRowNotation(uniqueRowKey, row.ID,[this.modifiedColumnNotation(columnKey, column.ID, this.paragraphTextNotation(null, ""))])
                );
                row.modifiedElementKey = uniqueRowKey;
            } else {
                rowNotation.columns.push(this.modifiedRowNotation(columnKey, column.ID));
            }
            column.modifiedElementKey = columnKey;
        }
        console.log("Feidhm: Cuir alt le heilimint an tábla");
        this.dealWithRowNotations(element, table, null, rowsNotation);

        /*
        const elementKey = this.generateUniqueUIKey();
        const tableKey = this.generateUniqueUIKey();
        const rowKey = this.generateUniqueUIKey();
        const columnKey = this.generateUniqueUIKey();

        this.modifiedElements.push(
            this.modifiedElementNotation(elementKey, element.ID, null, null,
                this.modifiedTableNotation(tableKey, table.ID, null, [
                    this.modifiedRowNotation(rowKey, row.ID, [
                        this.modifiedColumnNotation(columnKey, column.ID,
                            this.paragraphTextNotation(null, null),
                            null,
                            null
                        )], null)
                ], null, null), null
            )
        );

        element.modifiedElementKey = elementKey;
        table.modifiedElementKey = tableKey;
        row.modifiedElementKey = rowKey;
        column.modifiedElementKey = columnKey;
        */

        this.setState({noteData: noteData});
    }

    enableParagraphTextTitle(index, rowIndex, columnIndex) {
        const noteData = this.state.noteData;
        const element = noteData.elements[index];
        const table = element.table;
        const row = table.rows[rowIndex];
        const column = row.columns[columnIndex];
        column.paragraphText.title = "";
        this.setState({noteData: noteData});
    }

    tableCellEditorOption(index, rowIndex, columnIndex, showDeleteButton, additionalButtons) {
        return (
            <div className={"uiComponents-noteRender-editorOptions"}>
                <div className={"row"}>
                    {showDeleteButton ?
                        <Fab onClick={() => this.deleteTableElement(index, rowIndex, columnIndex)} size="small" color="secondary" aria-label="add">
                            <DeleteIcon />
                        </Fab> : null}
                    <Fab onClick={() => this.addTableParagraphText(index, rowIndex, columnIndex)} size="small" color="secondary" aria-label="add">
                        <TextFieldsIcon />
                    </Fab>
                    <Fab size="small" color="secondary" aria-label="add">
                        <ImageIcon />
                    </Fab>
                    {additionalButtons.map(additionalButton =>
                        <Fab onClick={() => additionalButton.action()} size="small" color="secondary" aria-label="add">
                            {additionalButton.icon}
                        </Fab>
                    )}
                </div>
            </div>
        )
    }

    tableEditorOption(horizontalControls, leftAction, rightAction, deleteAction) {
        return (
            [
                <Fab style={{marginRight: 4}} size="small" color="secondary" aria-label="add" onClick={leftAction}>
                    {horizontalControls ? <ArrowLeftIcon /> : <ArrowDropUpIcon />}
                </Fab>,
                <Fab onClick={deleteAction} size="small" color="secondary" aria-label="add">
                    <DeleteIcon />
                </Fab>,
                <Fab style={{marginLeft: 4}} size="small" color="secondary" aria-label="add" onClick={rightAction}>
                    {horizontalControls ? <ArrowRightIcon /> : <ArrowDropDownIcon />}
                </Fab>
            ]
        )
    }

    tableElementRenderer(index, rowIndex, columnIndex, element, editable) {
        if (element.paragraphText != null) return this.constructTableElementBody(element, null,
            <div style={{minHeight: 20}}>
                <div>
                    <div>{(element.paragraphText.title !== null) ? this.defaultTitleRenderer(element.paragraphText.title, editable, (p1) => {this.tableTextDidChange(this.state.noteData.elements[index], this.state.noteData.elements[index].table.rows[rowIndex], element,p1.currentTarget.innerText, true)}) : null }</div>
                    <div style={{width: "100%", minHeight: 20}} contentEditable={editable} suppressContentEditableWarning={true} onInput={(p1) => {this.tableTextDidChange(this.state.noteData.elements[index], this.state.noteData.elements[index].table.rows[rowIndex], element,p1.currentTarget.innerText, false)}}>
                        {element.paragraphText.data}
                    </div>
                    {editable ? this.tableCellEditorOption(index, rowIndex, columnIndex, true, [
                        {
                            icon: <TitleIcon/>,
                            action: () => this.enableParagraphTextTitle(index, rowIndex, columnIndex)
                        }
                    ]) : null}
                </div>
            </div>);
        else if (element.image != null) return this.constructTableElementBody(element, "uiComponents-noteRender-portraitImageElement",
            <div>
                <div className={"uiComponents-noteRender-portraitImage"}
                     style={{backgroundImage: "url('" + element.image.URL + "')"}}>
                    <img alt="" src={element.image.URL} style={{visibility: "hidden"}}/>
                </div>
                {editable ? this.tableCellEditorOption(index, rowIndex, columnIndex, true, [
                    {}
                ]) : null}
            </div>);
        return editable ? this.tableCellEditorOption(index, rowIndex, columnIndex, false, []) : null;
    }

    defaultTitleRenderer(text, editable, onInput) {
        return <h3 contentEditable={editable} suppressContentEditableWarning={true} style={{"fontWeight": 500, minHeight: 26}} onInput={onInput}>{text}</h3>
    }

    elementRenderer(index, element, editable) {
        if (element.paragraphText != null) return this.constructElementBody(element, null,
            <div>
                <div>
                    {(element.paragraphText.title !== null) ? this.defaultTitleRenderer(element.paragraphText.title, editable, (p1) => {this.textDidChange(element, p1.currentTarget.innerText, true)}) : null }
                </div>
                <div style={{minHeight: 20}} contentEditable={editable} suppressContentEditableWarning={true} onInput={(p1) => {this.textDidChange(element, p1.currentTarget.innerText, false)}}>
                    {element.paragraphText.data}
                </div>
                {editable ? this.editorOptions(index, {}, element) : null}
            </div>);
        else if (element.image != null) return this.constructElementBody(element, "uiComponents-noteRender-portraitImageElement",
            <div>
                <div className={"uiComponents-noteRender-portraitImage"}
                     style={{backgroundImage: "url('" + element.image.URL + "')"}}>
                    <img alt="" src={element.image.URL} style={{visibility: "hidden"}} />
                </div>
                {editable ? this.editorOptions(index, {}, element) : null}
            </div>);
        else if (element.table != null) return this.constructElementBody(element, null,
            <div>
                {(element.table.title !== null) ? this.defaultTitleRenderer(element.table.title, editable, (p1) => {this.textDidChange(element, p1.currentTarget.innerText, true)}) : null }
                <TableContainer component={Paper}>
                    <Table aria-label="caption table">
                        {(element.table.caption !== null) ? <caption>
                            {editable ? <div className={"uiComponents-noteRender-newRow"}>
                                <Fab onClick={() => this.addTableRow(index)} size="small" color="secondary" aria-label="add">
                                    <AddIcon />
                                </Fab>
                            </div> : null}
                            {element.table.caption}
                        </caption> : null}
                        <TableHead>
                            <TableRow>
                                {element.table.metaColumns.map((metaColumn, metaColumnIndex) =>
                                    <TableCell>
                                        {editable ?
                                            <div>
                                                <div style={{height: 24}} contentEditable={editable} suppressContentEditableWarning={true}>
                                                    {metaColumn.title}
                                                </div>
                                                <div className={"uiComponents-noteRender-columnEditor"}>{
                                                    this.tableEditorOption(true,
                                                        () => this.moveTableColumn(index, metaColumnIndex, -1),
                                                        () => this.moveTableColumn(index, metaColumnIndex, 1),
                                                        () => {this.deleteTableColumn(index, metaColumnIndex)}) }</div>
                                            </div> : metaColumn.title}
                                    </TableCell>
                                )}
                                {editable ?
                                    <TableCell style={{textAlign: "center"}}>
                                        <div><Fab onClick={() => this.addTableColumn(index)} size="small" color="secondary" aria-label="add">
                                            <AddIcon />
                                        </Fab></div>
                                    </TableCell> : null}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {element.table.rows.map((row, rowIndex) =>
                                <TableRow>
                                    {row.columns.map((element, columnIndex) =>
                                        <TableCell style={{width: (1/this.state.noteData.elements[index].table.metaColumns.length + (editable ? 1 : 0)).toString() + "%"}}>
                                            {this.tableElementRenderer( index, rowIndex, columnIndex, element, editable)}
                                        </TableCell>
                                    )}
                                    {editable ?
                                        <TableCell style={{textAlign: "center", width: (1/this.state.noteData.elements[index].table.metaColumns.length + (editable ? 1 : 0)).toString() + "%"}}>
                                            { this.tableEditorOption(false,
                                                () => this.moveTableRow(index, rowIndex, -1),
                                                () => this.moveTableRow(index, rowIndex, 1),
                                                () => this.deleteTableRow(index, rowIndex)) }
                                        </TableCell> : null}
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                {editable ? this.editorOptions(index, {}, element) : null}
            </div>);
        return null;
    };

    addParagraphText() {
        const elementKey = this.generateUniqueUIKey();
        this.newElements.push(
            this.newElementNotation(elementKey,
                this.paragraphTextNotation(null, ""), null, null, null
            )
        );

        const noteDate = this.state.noteData;
        noteDate.elements.push({
            paragraphText: {
                title: null,
                data: null
            },
            width: 100,
            order: noteDate.elements[noteDate.elements.length - 1].order,

            newElementKey: elementKey
        });
        this.setState({noteDate: noteDate});
    }

    constructTableElementBody(element, className, child) {
        return <div className={((className == null) ? "" : "uiComponents-noteRender-portraitImageElement") + " uiComponents-noteRender-element"} style={{width: "100%"}}>
            {child}
        </div>
    }

    constructElementBody(element, className, child) {
        const width  = (window.innerWidth >= 1024) ? element.width : 100;
        return <div className={((className == null) ? "" : "uiComponents-noteRender-portraitImageElement") + " uiComponents-noteRender-element"} style={{width: width + "%"}}>
            {child}
        </div>
    }

    save = () => {
        console.log("Deleted elements: ", this.deletedElements);
        console.log("Modified elements: ", this.modifiedElements);
        console.log("New elements: ", this.newElements);

        console.log("Elements: ", this.state.noteData.elements);
        console.log("Original Elements: ", this.originalNoteData.elements);

        this.request(link("student.NotesREST", "1/submitProposal"),
            {
                modifiedElements: this.modifiedElements,
                newElements: this.newElements,
                deletedElements: this.deletedElements
            }
        ).performRequest();
    };

    handleSubjectChange = (event) => {
        this.setState({subjectID: event.target.value}, () => {

        });
    };

    handleTopicChange = (event) => {
        this.setState({topicID: event.target.value}, () => {

        });
    };

    handleVersionNumberChange = (event) => {
        this.setState({versionNumber: event.target.value}, () => {

        });
    };

    render() {
        const {classes} = this.props;

        return (

            <div className={"uiComponents-noteRender-body"}>
                <div className={"container-fluid"}>
                    <div className={"row"}>
                        <div className={"col-md-2 uiComponents-noteRender-selectors"}>
                            <div className={"row uiComponents-noteRender-contributionOptions"}>
                                <Tabs
                                    value={this.state.tabPosition}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    variant="fullWidth"
                                    onChange={(value, position) => this.setState({tabPosition: position})}
                                    aria-label="disabled tabs example">
                                    <Tab className={classes.tabWidth} label={s().readTitle} />
                                    <Tab className={classes.tabWidth} label={s().editorTitle} />
                                    <Tab className={classes.tabWidth} label={s().historyTitle} />
                                    {this.props.adminMode ? [
                                            <Tab className={classes.tabWidth} label={s().proposalsTitle} />
                                        ] : []}
                                </Tabs>
                            </div>
                            <div className={"row"}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>{s().readSubject}</InputLabel>
                                    <Select
                                        value={this.state.subjectID}
                                        onChange={this.handleSubjectChange}>
                                        {this.props.subjects.map(subject => <MenuItem value={subject.ID}>{subject.titleGA}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            </div>
                            <div className={"row"}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>{s().readTopics}</InputLabel>
                                    <Select
                                        value={this.state.subjectID}
                                        onChange={this.handleTopicChange}>
                                        {this.props.topics.map(subject => <MenuItem value={subject.ID}>{subject.titleGA}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            </div>
                            {/* Painéal Rialacháin an Eagarthóra */}
                            <div style={{display: (this.state.tabPosition === 1) ? "block" : "none"}} className={"row uiComponents-noteRender-editorElement"}>
                                <div className={"row"}>
                                    <h4>{s().editorImages}</h4>
                                </div>
                                <div className={"row"}>
                                    <div className={"col-xs-4"}>
                                        <Button variant={"text"} label={s().editorPortrait}/>
                                    </div>
                                    <div className={"col-xs-4"}>
                                        <Button variant={"text"} label={s().editorDiagram}/>
                                    </div>
                                </div>

                                <div className={"row"}>
                                    <h4>Téacs</h4>
                                </div>
                                <div className={"row"}>
                                    <div className={"col-xs-4"}>
                                        <Button variant={"text"} label={s().editorParagraph} onClick={() => this.addParagraphText()}/>
                                    </div>
                                </div>

                                <div className={"row"}>
                                    <h4>Structúr</h4>
                                </div>
                                <div className={"row"}>
                                    <div className={"col-xs-4"}>
                                        <Button variant={"text"} label={s().editorTable}/>
                                    </div>
                                </div>

                                <div style={{paddingTop: 8}} className={"row"} onClick={() => this.save()}>
                                    <Button label={s().editorSave}/>
                                </div>
                            </div>

                            {/* Painéal rialacháin leaganachaí staire */}
                            <div style={{display: (this.state.tabPosition === 2) ? "block" : "none"}} className={"row uiComponents-noteRender-editorElement"}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel>{s().versionNumber}</InputLabel>
                                    <Select
                                        value={this.state.versionNumber}
                                        onChange={this.handleVersionNumberChange}>
                                        {this.props.subjects.map(subject => <MenuItem value={subject.ID}>{subject.titleGA}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            </div>
                        </div>
                        <div className={"col-md-10"} style={{paddingLeft: 0, paddingRight: 0}}>
                            <TabPanel value={this.state.tabPosition} index={0}>
                                {(this.state.noteData != null) ? this.state.noteData.elements.map((element, index) => {
                                    return this.elementRenderer(index, element, false);
                                }) : null}
                            </TabPanel>
                            <TabPanel value={this.state.tabPosition} index={1}>
                                {(this.state.noteData != null) ? this.state.noteData.elements.map((element, index) => {
                                    return this.elementRenderer(index, element, true);
                                }) : null}
                            </TabPanel>
                            <TabPanel value={this.state.tabPosition} index={2}>
                                {(this.state.versionSelectedNoteData != null) ? this.state.noteData.elements.map((element, index) => {
                                    return this.elementRenderer(index, element, false);
                                }) : null}
                            </TabPanel>
                            {this.props.adminMode ? [
                                <TabPanel index={this.state.tabPosition} value={3}>
                                    Tograi
                                </TabPanel>
                            ] :
                                [

                                ]}
                        </div>
                    </div>
                </div>
            </div>
        )
    };

}
NoteRender.package = "uiComponents.NoteRender";

NoteRender.defaultProps = {
    adminMode: false
};

NoteRender.propTypes = {
    adminMode: PropTypes.bool,
    subjects: PropTypes.array.isRequired,
    topics: PropTypes.array.isRequired,

    /*
    Definition:
    Let noteDataRequest = (subjectID, topicID, versionNumber, completion) => { ... }
    Define subjectID, topicID and year to be non-nullable parameters that associate to a note from the database,
    versionNumber to be a nullable parameter to choose a version of an approved note from the present/past and
    completion = (noteData) => { ... } and non-nullable.
    If noteDataRequest is called, it will call completion and noteData will contain the note for the given parameters from the database.
    If versionNumber is null, the latest version of the note will be given.
     */
    noteDataRequest: PropTypes.func.isRequired,

    subjectID: PropTypes.number,
    topicID: PropTypes.number
};

export default withStyles(styles)(NoteRender)