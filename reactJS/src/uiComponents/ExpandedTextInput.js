import React, {Component} from "react";
import {TrinTextInput} from "trinreact"
import PropTypes from "prop-types"

class ExpandedTextInput extends TrinTextInput {

    constructor(props) {
        super(props);
        this.state.value = props.value;
        if (props.valueChanged != null) {
            props.valueChanged(props.value);
        }
    }

    render() {
        return (
            <textarea placeholder={this.props.label} onChange={this.setInputValue}>{this.state.value}</textarea>
        )
    }

}

ExpandedTextInput.defaultProps = {
    value: null,
    name: null,
    numeric: false,
    optional: false
};
ExpandedTextInput.propTypes = {
    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    numeric: PropTypes.boolean,
    optional: PropTypes.boolean
};
export default ExpandedTextInput;