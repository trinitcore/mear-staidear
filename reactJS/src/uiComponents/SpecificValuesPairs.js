import React from "react";
import TrinPairs from "../../trinreact/uiComponents/TrinPairs";
import PropTypes from "prop-types"
import TextInput from "./TextInput";
import SelectField from "./SelectField";
import Button from "./Button";

class SpecificValuesPairs extends TrinPairs {

    constructor(props) {
        super(props)
    }

    registeredPairRender(values) {
        return values.map((pair,index) => {
                return <div>
                    <span style={{width:"45%",paddingRight:8}}>
                        <SelectField label={"Specific Field"} value={pair.specificFieldID} values={this.props.specificFields}/>
                    </span>
                    <span style={{width:"45%"}}>
                        <TextInput label={"Value"} value={pair.value} />
                    </span>
                    <span style={{width:"10%"}}>
                        <Button label={"Remove"} onClick={() => {
                            this.removePair(index)
                        }}/>
                    </span>
                </div>})
    }

    newPairRender() {
        return (
            <div>
                <span style={{width:"50%",paddingRight:8}}>
                    <SelectField label={"Specific Field"} values={this.props.specificFields} name={"specificFieldID"} />
                </span>
                <span style={{width:"50%"}}>
                    <TextInput label={"Value"} name={"value"}/>
                </span>
                <div>

                </div>
                <div>
                    <Button label={"Add"} onClick={this.addPair} />
                </div>
            </div>
        )
    }
}

SpecificValuesPairs.defaultProps = {
    values: []
};

SpecificValuesPairs.propTypes = {
    values: PropTypes.array,
    specificFields: PropTypes.array.isRequired
};

export default SpecificValuesPairs;