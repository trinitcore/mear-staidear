import React from "react";
import {TrinTextInput} from "trinreact"
import PropTypes from "prop-types"
import {s} from "../App";
import TextField from '@material-ui/core/TextField';

class TextInput extends TrinTextInput {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TextField
                variant="outlined"
                margin="normal"
                required={this.props.required}
                autoFocus={this.props.autoFocus}
                fullWidth={this.props.fullWidth}
                name={this.props.name}
                label={this.props.label}
                type={this.props.type}
                autoComplete={this.props.autoComplete}
                onChange={this.setInputValue}
                value={this.state.value}
            />
        )
    }

}

TextInput.defaultProps = {
    autoFocus: false,
    fullWidth: false,

    value: null,
    name: null,
    numeric: false,
    double: false,
    optional: false
};
TextInput.propTypes = {
    autoFocus: PropTypes.boolean,
    autoComplete: PropTypes.string,
    type: PropTypes.string,
    required: PropTypes.boolean,
    fullWidth: PropTypes.boolean,

    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string.isRequired,
    numeric: PropTypes.boolean,
    double: PropTypes.boolean,
    optional: PropTypes.boolean
};
export default TextInput;