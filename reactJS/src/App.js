import React, {Component} from 'react';
import {AppController,CurrentRoutedComponent} from "trinreact"
import {Route} from "trinreact"
import {RouteBundle} from "trinreact"
import {SessionRouteBundle, SessionRoute} from "trinreact"
import {ComponentRoute} from "trinreact"
import {Theme} from "trinreact";
import Login from "./layout/public/Login";

import AdminDashboard from "./layout/admin/Dashboard";
import AdminExamResources from "./layout/admin/ExamResources";
import AdminExamEditor from "./layout/admin/examResources/ExamEditor";
import AdminNotes from "./layout/admin/Notes";

import StudentDashboard from "./layout/student/Dashboard";
import StudentExamPapers from "./layout/student/ExamPapers";
import StudentNotes from "./layout/student/Notes";

import Index from "./layout/public";

import {Link} from "react-router-dom";
import {cookies,setCookie,removeCookie,userAttribute,doSignOut} from "trinreact"

import translationEN from "./en.app.json";
import translationGA from "./ga.app.json";

import AppBar from '@material-ui/core/AppBar';
import IconArrowBack from "@material-ui/icons/ArrowBack";
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider, WithTheme } from '@material-ui/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Collapse from '@material-ui/core/Collapse';

import "./App.css";
import Drawer from "@material-ui/core/es/Drawer/Drawer";
import ListItemText from "@material-ui/core/es/ListItemText/ListItemText";
import { withStyles } from '@material-ui/core/styles';
import BackButton from "./uiComponents/BackButton";

export var currentInstance = null;
export var currentInstanceName = null;

export function s() {
    if (cookies().get("lang") === "en") {
        return translationEN[currentInstanceName];
    } else {
        return translationGA[currentInstanceName];
    }
}

export function getAllAvailableYears() {
    const startYear = 2019;
    const endYear = 2003;

    const allTheYears = [];

    for (var i = 0; i <= (startYear-endYear); i++) {
        allTheYears.push(endYear+i);
    }

    return allTheYears;
}

export function setLanguage(langCode) {
    setCookie("lang",langCode);
}
function controllerS() {
    if (cookies().get("lang") === "en") {
        return translationEN["AppController"];
    } else {
        return translationGA["AppController"];
    }
}

export function componentS(componentPackage) {
    if (cookies().get("lang") === "en") {
        return translationEN[componentPackage];
    } else {
        return translationGA[componentPackage];
    }
}

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#009688'
        }
    },
    typography: {
        // Tell Material-UI what the font-size on the html element is.
        fontFamily: ["Source Sans Pro"],
    },
});

const styles = {
    active: {
        backgroundColor: "red"
    }
};

const defaultNavigationMargin = 12;

class App extends AppController {

    render() {
        return <ThemeProvider theme={theme}>
            {super.render()}
        </ThemeProvider>
    }

    defineComponent(context) {
        currentInstanceName = context.package;
        currentInstance = context;
    }

    constructor(props) {
        super(props);
    }

    defineTheme() {
        return new Theme()
            .defineProgressIndicator(<div style={{textAlign: "center"}}><CircularProgress/></div>)
    }

    defineLogin() {
        return {
            component: Login
        }
    }

    createNavigationBar = (title, buttons) => <AppBar position="relative">
        <Toolbar variant="dense" style={{backgroundColor:"white", color:"#009688"}}>
            <IconButton
                size={"small"} edge="start" color="inherit" aria-label="Menu" style={{marginRight:14}} onClick={() => {
                this.sideDrawerVisible(true)
            }}>
                <MenuIcon />
            </IconButton>
            <Typography variant="h6" style={{flexGrow:1}} noWrap>
                {title}
            </Typography>
            {buttons}
            {/* <Button color="inherit" onClick={() => {removeCookie("lang")}}>Ruaig na fianáin theanga</Button> */}
        </Toolbar>
    </AppBar>;

    defineNavigationBars() {
        return [
            new ComponentRoute("/", (title) => this.createNavigationBar(title, [
                <Link to={"/login"}><Button color="inherit">{controllerS().signIn}</Button></Link>
            ]), true),
            new ComponentRoute("/", (title) => this.createNavigationBar(title, []), false)
        ]
    }

    defineLanguages() {
        return {
            "en" : translationEN,
            "ga" : translationGA
        };
    }

    defineNavigationSideDrawer() {
        const isLoggedIn = cookies().get("isLoggedIn") === "true";
        return <Drawer open={this.state.navigationSideDrawerVisible} onClose={() => this.sideDrawerVisible(false)}>
            <div
                onClick={() => this.sideDrawerVisible(false)}
                onKeyDown={() => this.sideDrawerVisible(false)}
                role="presentation">
                <List>
                {this.state.navigationSideDrawerComponents}
                    {isLoggedIn ?
                        <ListItem style={{paddingLeft: defaultNavigationMargin}} button onClick={() => {
                            doSignOut(this.currentContext);
                        }}>
                            <ListItemText primary={controllerS().signOut}/>
                        </ListItem> : null
                    }
                </List>
            </div>
        </Drawer>;
    }

    defineNavigationSideDrawerLink(label, path, selected, indentPosition, subNavigationSideDrawerComponents) {
        const {classes} = this.props;
        const styles = {
            backgroundColor: "rgba(0, 0, 0, " + (0.14 - 0.04*indentPosition) + ")"
        };
        return (
            <Link to={path}>
                <ListItem button key={label} selected={selected} style={Object.assign({paddingLeft: defaultNavigationMargin + indentPosition*defaultNavigationMargin}, selected ? styles : null)}>
                    <ListItemText primary={label} />
                </ListItem>
            </Link>
        )
    }

    defineNavigationChildren(children) {
        return (<List component="div" disablePadding>
                    {children}
                </List>
        )
    }

    defineRoutes() {
        return [
            new SessionRouteBundle("student",null,[
                new Route("dashboard", StudentDashboard),
                new Route("notes", StudentNotes),
                new Route("examPapers/:subjectID/:year", StudentExamPapers),
                new Route("examPapers/:subjectID", StudentExamPapers),
                new Route("examPapers", StudentExamPapers)
            ]),
            new SessionRouteBundle("admin",null,[
                new Route("dashboard", AdminDashboard),
                new RouteBundle("examResources",AdminExamResources,[
                    new Route("examEditor/:ID",AdminExamEditor)
                ]),
                new Route("notes", AdminNotes)
            ]),
            new RouteBundle("", Index, [
                new Route("gallery", Index)
            ])
        ]
    }

}

export default withStyles(styles)(App);